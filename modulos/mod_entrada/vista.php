<?php
class Vista extends MView {

	function Ver_Modelo($vista){		
		global $diccionario;
		/********************** INDICADOR DE MODO (PRODUCCION, DESARROLLO)****************************/
		if(APLICATION_MODE==2 && $vista==MODULO){
			echo '<div style="background-color:#FFFF00; height:4px;"></div>';
		}elseif(APLICATION_MODE==0 && $vista==MODULO){
			echo '<div style="background-color:#0000FF; height:4px;"></div>';
		}	
		/********************************************************************************************/
		$html = $this->get_template($vista);
		$html = $this->render_dinamic_data($html, $diccionario['formulario']);
		print $html;
	
	}
}

	
	