<?php
class Model extends ABModel{
		/*****************************************************************************************************************
														INSERCION DE  REGISTROS
		*****************************************************************************************************************/
		public function addItem($bindvars) {
			$sql = "INSERT INTO L_LISTAS_PARENT
					VALUES (SQ_L_LISTAS_PARENT.NEXTVAL,:ID_LISTA,:PARENT,:NOMBRE,'1')";
			return $this->single_query($sql,$bindvars);
		}
		public function editItem($bindvars) {
			$sql = "UPDATE L_LISTAS_PARENT  SET NOMBRE = :NOMBRE WHERE P_KEY=:COD ";
			return $this->single_query($sql,$bindvars);
		}		
		/*****************************************************************************************************************
														OBTENCION DE REGISTROS
		*****************************************************************************************************************/
		public function getMenu($bindvars) {
			$sql = "SELECT NOMBRE, CARPETA, SITIO, ID_PERMISO, U_MODULOS.P_KEY, U_MODULOS.PARENT, U_MODULOS.ICON
					FROM U_PERMISOS
					LEFT JOIN U_MODULOS ON U_MODULOS.P_KEY = U_PERMISOS.ID_MODULO
					WHERE U_MODULOS.ESTADO='1' AND ID_USUARIO = :COD AND SITIO = '1'
					ORDER BY PARENT ASC, ORDEN ASC";
			return $this->query_fetch($sql,$bindvars);
		}
		
		public function obtenListas($bindvars) {
			$sql = "SELECT P_KEY, PARENT, NOMBRE 
					FROM L_LISTAS_PARENT
					WHERE ESTADO='1' AND ID_LISTA = :COD
					ORDER BY PARENT ASC";
			return $this->query_fetch($sql,$bindvars);
		}
		
		
	}
	




