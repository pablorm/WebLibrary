<?php
class Vista extends MView {
	public $combo1;
	public $_menu;
    // visualiza la pagina principal
	function Ver_Modelo($vista){		
		global $diccionario;
		/********************** INDICADOR DE MODO (PRODUCCION, DESARROLLO)****************************/
		if(APLICATION_MODE==2){
			//echo '<div style="background-color:#FFFF00; height:4px; index:0;"></div>';
		}elseif(APLICATION_MODE==0){
			//echo '<div style="background-color:#0000FF; height:4px; index:0;"></div>';
		}	
		/********************************************************************************************/
		$html = $this->get_template($vista);
		$html = $this->render_dinamic_data($html, $diccionario['formulario']);
		$html = str_replace('{USUARIO}',Session::get('usuario'),$html);		
		$html = str_replace('{NOMBRE_EMPRESA}',NOMBRE_EMPRESA,$html);
		
		$menu = '';
		
		$menu = $this->orderMenu('0','');
		
		//$html = str_replace('{MENU_LIST}',$menu,$html);
		print $html;	
	}
	
	
	function orderMenu($parent,$crumb){
		if(is_array($this->_menu)){
			foreach($this->_menu as $key => $val){
				if($val['PARENT']==$parent){
					$menu .= '<li><a href="'.$val['CARPETA'].'"  data-breadcrumb="'.$crumb.$val['NOMBRE'].'" title="'.$val['NOMBRE'].'"><i class="'.$val['ICON'].'"></i> <span class="menu-item-parent">'.$val['NOMBRE'].'</span></a>';
					$smenu = $this->orderMenu($val['P_KEY'],$val['NOMBRE'].' \ ');
					if($smenu){
						$menu .= '<ul>'.$smenu.'</ul>';
					}
					$menu .= '</li>';
				}
			}			
		}		
		return $menu;
	}
	
}

	
	