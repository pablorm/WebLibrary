<?php
	//const MODULO = 'inspecciones';
	const FIN_X_CRITICIDAD = '5,3';
	const FOLDER_FILES = 'files_b';
	const FOLDER_IMAGES = 'files';
	const ZIP_FOLDER = '/TempZip/';
	
	
		

	$diccionario = array(
		MODULO=>array('result_a'=>'Cod.',
							'result_b'=>'Nombre',
							'result_c'=>'Costo'
		),
		
		'nuevo'=>array('TITULO_NUEVO'=>'INGRESAR',
								'FECHA_VAL'=>'',
								'FECHA_AVISO_VAL'=>'',
								'AVISO_VAL'=>'',
								'OT_VAL'=>'',
								'DESCRIPCION_VAL'=>'',
								'TRATAMIENTO_VAL'=>'',
								'CAUSA_VAL'=>'',								
								'ACCION'=>'NUEVO',
								'form_action'=>'index.php?'.MODULO.'_guardar'
		),
		
		'modificar'=>array('TITULO_NUEVO'=>'MODIFICAR',
									'ACCION'=>'MODIFICAR',
									'form_action'=>'index.php?'.MODULO.'_guardar'
		),
		
		'eliminar'=>array('TITULO_NUEVO'=>'ELIMINAR',
									'ACCION'=>'ELIMINAR',
									'form_action'=>'index.php?'.MODULO.'_guardar'
		),
		
		'formulario'=>array('action_search'=>'index.php?'.MODULO.'_grid',
							'titulo'=>'Biblioteca - Grupos',
							'ruta_regreso'=>'index.php?'.MODULO,
							'modulo'=>MODULO,
							'ruta'=>SITE_ROOT,
							'permisos'=>'7'//Controller::getAccess(MODULO)

		)
	);

	$listado_inputs=array(	'bateriaexamen' =>array('COD'=>array(	'largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),

													'ID_BATERIA' =>	array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no'),

													'ID_EXAMEN' =>	array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no')),
																		
							'grupo' =>	array(	'GCOD'=>array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no'), 
																	
												'GNOMBRE'=>array(		'largo'=>45,
																		'tipo'=>'text',
																		'valido_nulo'=>'no')),
																	
							'nuevo' =>array(	'COD' => array(		'largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),

												'ID_USUARIO' =>	array(	'largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),

												'ID_GRUPO' =>array(	'largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),

												'PERMISO' =>array(	'largo'=>1,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no')),

							'getvars'=>array(	'COD' =>array(	'largo'=>6,
																'tipo'=>'integer',
																'valido_nulo'=>'no')),


							'buscador' =>	array(	'FOLIO' =>array('largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'si'),
													'NOMBRE' =>	array(	'largo'=>90,
																	'tipo'=>'text',
																	'valido_nulo'=>'si'))
							);


?>
