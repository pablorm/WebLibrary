<?php
	const MODULO = 'admbib';	
	include_once(SERVER_ROOT . MODULOS . '/mod_'. MODULO .'/constantes.php');
	include_once(SERVER_ROOT . MODULOS . '/mod_'. MODULO .'/modelo.php');
	include_once(SERVER_ROOT . MODULOS . '/mod_'. MODULO .'/vista.php');
	
	class Controller extends Controllermass
	{
		public $_archivoids;
		public $_response;
		private $zipfolder;
				
		public function main(array $getVars,$subpage){
			global $formatos_registrados;
			$Model = new Model;	
			$Vista = new Vista;
			$Vista->subpage = $subpage;
			$exclude=array();
			
			switch($subpage){
				case 'rmgrupo':
					$includeVars	= $this->include_vars(array('cod'=>$getVars['cod']), $exclude, 'getvars');
					if( $includeVars[0] != "ERROR" ){
						if( $Model->rmRegistro('BIB_GRUPOS','P_KEY',$includeVars) )
							$Vista->print_json(array('success'=>'1','case'=>$subpage));
					}
					$Vista->print_json(array('success'=>'0','case'=>$subpage));
					exit;
				case 'setgrupo':
					if(!$getVars['Gcod'])
						unset($getVars['Gcod']);
					$includeVars = $this->include_vars($getVars, $exclude, 'grupo');
					$includeVars = $this->array_utf8($includeVars,'DECODE');
					if( $includeVars[0] != "ERROR" ){
						if($getVars['Gcod']){
							if($Model->editGrupo( $includeVars ) )
								$Vista->print_json(array('success'=>'1','case'=>$subpage));							
						}else{
							if($Model->setGrupo( $includeVars ) )
								$Vista->print_json(array('success'=>'1','case'=>$subpage));
						}
					}
					$Vista->print_json(array('success'=>'0','case'=>$subpage));
					exit;
				case 'gridgrupos':
					$Vista -> _data  = $Model->getGrupos($includeVars);
					$Vista->target = $getVars['target'];
					break;
					
				case 'rmgroup':
					$cod = array('cod'=>$getVars['cod']);
					$includeVars = $this->include_vars($cod,$exclude,'getvars');
					if( $includeVars[0] != "ERROR" ){
						if($Model -> rmGrupo($includeVars)){
							$Vista->print_json(array('success'=>'1'));
						}
						$Vista->print_json(array('success'=>'0'));
					}					
					exit;
					
				case 'setgroup':
					$cod = array('id_usuario'=>$getVars['cod'],'id_grupo'=>$getVars['grupo'],'permiso'=>$getVars['permiso']);
					$includeVars = $this->include_vars($cod,$exclude,'nuevo');
					if( $includeVars[0] != "ERROR" ){
						if($Model -> setPermiso($includeVars)){
							$Vista->print_json(array('success'=>'1'));
						}
						$Vista->print_json(array('success'=>'0'));
					}				
					exit;
					
				case 'loadgroups':
					$cod=array('cod'=>$getVars['cod']);
					$includeVars = $this->include_vars($cod,$exclude,'getvars');
					if($includeVars[0] != "ERROR"){
						$_data = $Model -> loadGrupos($includeVars);
						$Vista -> _data = $_data;		
					}
					$Vista->target = $getVars['target'];
					break;
				
				case 'grid':
					$getVars['nombre'] = utf8_decode($getVars['nombre']);
					$includeVars = $this->include_vars($getVars,$exclude,'buscador');
					$includeVars_p = $this->paginar($includeVars,$getVars['iniciopagina'],$getVars['cantidad_filas']);
					$_data = $Model->obtain_results($includeVars_p,$getVars);
					
					foreach($_data as $key=>$val){
						if($_data[$key]['TIPO'] == '1')
							$_data[$key]['DEN_TIPO'] = 'Examen Adicional';
						elseif($_data[$key]['TIPO'] == '2')
							$_data[$key]['DEN_TIPO'] = 'Baterias';
							
						$_data[$key]['ADHERENTE'] = number_format($_data[$key]['ADHERENTE']);	
						$_data[$key]['NO_ADHERENTE'] = number_format($_data[$key]['NO_ADHERENTE']);	
					}
					$Vista -> _data = $_data;
					$Vista -> filas = $Model->obtain_number_rows($includeVars,$getVars);	
					break;
					
				default:
					Session::init();
					$Vista -> subpage = MODULO;
					//$includeVars = $this->include_vars(array('COD'=>Session::get(PRE_SESS.'id_usuario')),$exclude,'getvars');
					$Vista -> combo1 = $Model -> getGrupos();
			}
			$Vista -> Ver();
		}		
		
	}