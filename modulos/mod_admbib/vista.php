<?php
    /***************************************************************************************************
											CONTROL DE VISTAS
	****************************************************************************************************/
	include_once(SERVER_ROOT .'/includes/class.excel.php');

class Vista extends MView {
	public $subpage;
	public $_data;
	public $filas;
	public $combo1;
	public $combo2;
	public $combo3;
	public $clave;

	function Ver(){
		global $diccionario;
		$html = $this->get_template($this->subpage);
		$html = $this->render_dinamic_data($html, $diccionario['formulario']);
		if(isset($diccionario[$this->subpage]))
			$html = $this->render_dinamic_data($html, $diccionario[$this->subpage]);

		switch($this->subpage){
			case 'gridgrupos':
				$html = $this->renderGrid($html, $this->_data, 'LISTADO');
				$html = base64_encode($html);
				$this->print_json(array('target'=>$this->target,'append'=>$html));
				break;
			case 'loadgroups':
				$html = $this->renderGrid($html, $this->_data, 'DOCUMENTACION','');
				$html = base64_encode($html);
				$this->print_json(array('target'=>$this->target,'append'=>$html));
				break;

			case 'exportar':
				$html='';
				$col_inicio		=	'a';
				$fila_inicio 	= 	1;
				$titulos		=	1;
				$nombre_archivo = 	"REM_INY_".date('d-m-Y');
				$_label 		= 	array('FOLIO','FECHA','CUMPLIMIENTO','INSPECTOR','SUPERVISOR','AREA','SECTOR','OT','MO_PRESENTE','HORA_INICIO','HORA_TERMINO','ACTIVIDAD','COMENTARIO','EMPRESA');
				$_titulos[]		= 	array('COL'=>'a','FILA'=>'2','DATO'=>'EMPRESA: BUREAU VERITAS');
				$_titulos[]		= 	array('COL'=>'a','FILA'=>'3','DATO'=>'FECHA: '.date('d-m-Y'));
				$this->creaXLS($nombre_archivo,$_label,$this->_data,$_titulos,$fila_inicio,$col_inicio);	
				break;

			case 'grid':
				$html = $this->renderGrid($html, $this->_data, 'LISTADO','');	
				$html = str_replace('{filas}',$this->filas[0]['TOTAL'],$html);
				break;

			case MODULO:
				$html = $this->renderCombo($html, $this->combo1,"COMBO_EXAMENES",null,'E');
				break;
		}
		print $html;


	}

}