<?php
class Model extends ABModel{
		/*****************************************************************************************************************
														INSERCION DE  REGISTROS
		*****************************************************************************************************************/
		public function setGrupo($bindvars) {
			$sql = "INSERT INTO BIB_GRUPOS VALUES (SQ_BIB_GRUPOS.NEXTVAL, :GNOMBRE, '1')";
			return $this->single_query($sql,$bindvars);
		}
		public function setPermiso($bindvars) {
			$sql = "MERGE INTO BIB_GRUPOS_USUARIOS USING dual ON ( ID_USUARIO=:ID_USUARIO AND ID_GRUPO=:ID_GRUPO )
					WHEN NOT MATCHED THEN INSERT (P_KEY, ID_USUARIO, ID_GRUPO, PERMISO) 
					VALUES (SQ_BIB_GRUPOS_USUARIOS.NEXTVAL,:ID_USUARIO,:ID_GRUPO,:PERMISO)";
			return $this->single_query($sql,$bindvars);
		}
		/*****************************************************************************************************************
														SELECCION DE  REGISTROS
		*****************************************************************************************************************/
		public function obtain_results($bindvars,$getVars) {
			$where = $this->build_where($getVars);
			$sql = "SELECT * from (
						SELECT a.*, rownum rnum  FROM (							
							SELECT USER_LIST.ID AS COD, NOMBRES AS NOMBRE, AP_PATERNO, AP_MATERNO, USUARIO
							FROM USER_LIST
							LEFT JOIN U_PERFILES ON ID_USUARIO = ID
							LEFT JOIN U_SM_PERMISOS ON (U_SM_PERMISOS.ID_USUARIO = ID)
							WHERE ID_MODULO = 14 AND ".$where."
						) 
						a WHERE rownum < :FINALPAGINA   
					) 
					WHERE rnum >= :INICIOPAGINA";
			return $this->query_fetch($sql,$bindvars);
		}		
		public function obtain_number_rows($bindvars,$getVars) {
			$where = $this->build_where($getVars);
			$sql = "SELECT COUNT(*) AS TOTAL
					FROM(
						SELECT USER_LIST.ID AS COD, NOMBRES AS NOMBRE, AP_PATERNO, AP_MATERNO, USUARIO
						FROM USER_LIST
						LEFT JOIN U_PERFILES ON ID_USUARIO = ID
						LEFT JOIN U_SM_PERMISOS ON (U_SM_PERMISOS.ID_USUARIO = ID)
						WHERE ID_MODULO = 14 AND ".$where.")";
			return $this->query_fetch($sql,$bindvars);
		}
		/*
		public function obtain_results_by_id($bindvars) {
			$sql = "SELECT P_KEY,NOMBRE,ADHERENTE,NO_ADHERENTE,TIPO,PARENT,ESTADO
					FROM HSE_QH_EXAMENES
					WHERE HSE_QH_EXAMENES.P_KEY = :COD";
			return $this->query_fetch($sql,$bindvars);
		}*/
		public function loadGrupos($bindvars) {
			$sql = "SELECT T1.P_KEY, BIB_GRUPOS.NOMBRE 
					FROM BIB_GRUPOS
					LEFT JOIN BIB_GRUPOS_USUARIOS T1 ON T1.ID_GRUPO = BIB_GRUPOS.P_KEY
					WHERE BIB_GRUPOS.ESTADO='1' AND T1.ID_USUARIO = :COD";
			return $this->query_fetch($sql,$bindvars);
		}
		
		
		public function getGrupos() {
			$sql = "SELECT BIB_GRUPOS.P_KEY, BIB_GRUPOS.NOMBRE 
					FROM BIB_GRUPOS
					WHERE BIB_GRUPOS.ESTADO='1'";
			return $this->query_fetch($sql,$bindvars);
		}
		/*****************************************************************************************************************
														ACTUALIZAR REGISTROS
		*****************************************************************************************************************/
		public function editGrupo($bindvars){
			$sql="UPDATE BIB_GRUPOS SET NOMBRE=:GNOMBRE WHERE P_KEY=:GCOD";
			return $this->single_query($sql,$bindvars);			
		}
		/*****************************************************************************************************************
														ELIMINACION DE  REGISTROS
		*****************************************************************************************************************/
		public function rmGrupo($bindvars) {
			$sql = "DELETE FROM BIB_GRUPOS_USUARIOS WHERE P_KEY = :COD";
			return $this->single_query($sql,$bindvars);
		}
		public function rmRegistro($tabla,$clave,$bindvars){
			$sql="DELETE FROM ".$tabla." WHERE ".$clave."=:COD";
			return $this->single_query($sql,$bindvars);			
		}
		
		/*****************************************************************************************************************
														CONSTRUCCION DE CONSULTAS 
		*****************************************************************************************************************/
			
		public function build_where($getVars) {
			$where="1 = 1";
			/*****************************************************************************************************************/
			
			if ($getVars['nombre'] <> "") { $where.=" AND UPPER(SIST_USUARIOS.USUARIO) LIKE '%' || UPPER(:NOMBRE)  || '%' "; }
			if ($getVars['folio'] <> "") { $where.=" AND SIST_USUARIOS.ID_USUARIO=:FOLIO";}
			/*****************************************************************************************************************/
			//$where.=" ORDER BY SIST_USUARIOS.USUARIO DESC";		
			return $where;
		}
		
		
	}
	




