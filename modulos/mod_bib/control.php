<?php
	const MODULO = 'bib';	
	include_once(SERVER_ROOT . MODULOS . '/mod_'. MODULO .'/constantes.php');
	include_once(SERVER_ROOT . MODULOS . '/mod_'. MODULO .'/modelo.php');
	include_once(SERVER_ROOT . MODULOS . '/mod_'. MODULO .'/vista.php');	
	require(SERVER_ROOT .'/includes/formats.php');

	class Controller extends Controllermass{  // 475 -
/*
		function updateDirPerms($a=null){
			//if( !$a || (!Session::get('dir_perms') && $a ) ){
				if( Session::get('id_usuario') ){
					$Model = new Model;
					$includeVars = $this->include_vars(array('cod'=>Session::get('id_usuario')),null,'getvars');
					//print_r($Model->get_user_groups($includeVars));
					Session::set('dir_perms',$Model->get_user_groups($includeVars));
					Session::set('file_perms',$Model->file_perms($includeVars));
				}
			//}
		}
*/
		public function main(array $getVars,$subpage){
			global $formatos_registrados;
			$Model = new Model;	
			$Vista = new Vista;
			$Vista -> subpage = $subpage;

			$exclude=array();
			
			//if( !Session::get('TIPO_LIB') ) exit;
			Session::init();
			if( !Session::get('TIPO_LIB') ) Session::set('TIPO_LIB','1');
			
			switch($subpage){
				case 'cp':				
					$parent_type = substr($getVars['parent'],0,2);
					$file_type = substr($getVars['file'],0,2);
					$file = str_replace($file_type,'',$getVars['file']);					
					if( $getVars['parent']=='NULO' )
						$parent = Session::get('carpeta_actual');
					else
						$parent = str_replace($parent_type,'',$getVars['parent']);
					
					$includeVars = $this->include_vars(array('cod'=>$file,'parent'=>$parent), $exclude, 'getvars');
					if($includeVars[0] != "ERROR" && $getVars['file']!= null){					
						if( $file_type=='C_' ){								
							$check = $Model->checkParentDir($includeVars);
							if($check[0]['TOTAL'] == 0 ){
								if($parent==0)
									$set = ', LIB='.Session::get('TIPO_LIB');
								$Model->updParentDir($includeVars,$set);
								//echo 'Corto/pego carpeta:'.$file.', en carpeta:'.$parent;
								echo 'Carpeta Pegada Correctamente';
							}							
						}elseif( $file_type=='A_' ){	
							$Model->updParentFile($includeVars);
							//echo 'Corto/pego archivo:'.$file.', en carpeta:'.$parent;	
							echo 'Archivo Pegada Correctamente';
						}						
					}
					exit;
					break;
				case 'history':
					$includeVars = $this->include_vars(array('cod'=>$getVars['cod']), $exclude, 'getvars');
					if($includeVars[0] != "ERROR"){
						$_data = $Model -> get_history($includeVars);
						foreach( $_data as$key=>$val ){
							switch($val['ACCION']){
								case 'gdocview':
									$_data[$key]['ACCION'] = 'Visualizo';
									break;
								case 'delete':
									$_data[$key]['ACCION'] = 'Elimino Archivo';
									break;
								case 'download': 
									$_data[$key]['ACCION'] = 'Descargo archivo';
									break;
								case 'upload':
									$_data[$key]['ACCION'] = 'Subio Archivo';
									break;
								case 'mkdir': 
									$_data[$key]['ACCION'] = 'Creo Carpeta';
									break;
								case 'rm':
									$_data[$key]['ACCION'] = 'Elimino Carpeta';
									break;
								case 'parentdirs':
								case 'listfiles':
								case 'listdir':
									$_data[$key]['ACCION'] = 'Consulta';
									break;
								case 'mklink':
									$_data[$key]['ACCION'] = 'Creo Link';
									break;
								case 'shlink':
									$_data[$key]['ACCION'] = 'Visito Link';
									break;
								case 'getdirinfo':
									$_data[$key]['ACCION'] = 'Obten Informacion de la carpeta';
									break;
								case 'dirprop':
									$_data[$key]['ACCION'] = 'Propiedades de la carpeta';
									break;
								case 'quitperm':
									$_data[$key]['ACCION'] = 'Quito Permiso';
									break;
								case 'addperm': 
									$_data[$key]['ACCION'] = 'Agrego Permiso';
									break;
								case 'rename':
									$_data[$key]['ACCION'] = 'Renombro Carpeta';
									break;
								case 'history':
									$_data[$key]['ACCION'] = 'Historial';
									break;			
							}
						}
						$Vista->_data = $_data;
					}
					break;
				case 'upload':
					$error = null;
					if( is_array($_FILES) ){
						
						foreach( $_FILES as $key=>$val ){
							$file_id 		= 	$Model->giveMeID('SQ_BIB_ARCHIVOS');
							$parent			= 	Session::get('carpeta_actual');
							$filename 		= 	$val['name'];
							$tempfilename   = 	$val['tmp_name'];
							$tamanio 		= 	$val['size'];
							$exp_filename	= 	explode(".",$filename);
							$extencion		=	strtolower(array_pop($exp_filename));
							$ubicacion 		= 	LIB_FOLDER.CURRENT_DIR.'/';
							
							$cod = array(
								'cod'=>$file_id,
								'parent'=>$parent,
								'nombre'=>$filename,
								'ext'=>$extencion,
								'tamanio'=>$tamanio,
								'ubicacion'=>$ubicacion,
								'fecha_subida'=>date('d/m/Y'),
								'estado'=>'3'
							);
							$includeVars = $this->include_vars($cod,$exclude,'archivo');							
							$includeVars = $this->array_utf8($includeVars,'DECODE');							
							//print_r($cod);
							//print_r($includeVars);
							if(	in_array($extencion,$formatos_registrados['imagenes']) ||
								in_array($extencion,$formatos_registrados['documentos']) ||
								in_array($extencion,$formatos_registrados['video'])	||
								in_array($extencion,$formatos_registrados['comprimido'])){
								
								if($includeVars[0] != "ERROR"){
									$route = SERVER_ROOT.MASTER_FOLDER.$ubicacion.$file_id;
									if($Model->guardar_archivo($includeVars)){
										if(move_uploaded_file($tempfilename, $route)){
											$includeVars_2 = $this->include_vars(array('cod'=>$file_id,'estado'=>'1'), $exclude, 'archivo');
											$Model -> actualizar_archivo($includeVars_2);
										}else
											$error = 1;
									}else
										$error = 2;
								}else
									$error = 3;
							}else
								$error = 4;
						}
					}
					if( $error )
						$Vista->print_json(array('success'=>'0','case'=>'upload','error'=>$error));
					else
						$Vista->print_json(array('success'=>'1','case'=>'upload','nfiles'=>count($_FILES)));
					exit;
					break;
					
				case 'delete': // ok
					$includeVars = $this->include_vars(array('cod'=>$getVars['file'],'estado'=>'5'), $exclude, 'archivo');
					if($includeVars[0] != "ERROR"){
						if($Model -> actualizar_archivo($includeVars))
							$Vista->print_json(array('success'=>'1','case'=>'delete'));					
					}
					$Vista->print_json(array('success'=>'0','case'=>'delete',));
					exit;
					break;

				case 'mkdir':				
					$nombre = base64_decode($getVars['nombre']);
					if( $getVars['parent']=='NULO' )
						$parent = '0';//Session::get('carpeta_actual');
					else
						$parent = $getVars['parent'];
					
					$includeVars = $this->include_vars(array('parent'=>$parent,'nombre'=>$nombre,'lib'=>strval(Session::get('TIPO_LIB'))), $exclude, 'nuevo');
					if($includeVars[0] != "ERROR"){
						$Model -> make_dir($includeVars);
						$clave_v = $Model -> last_id('SQ_BIB_CARPETAS');
						$clave = $clave_v[0]['CURRVAL'];
						
						if( $parent=='0' ){
							$includeVars2 = $this->include_vars(array('id_usuario'=>Session::get('id_usuario'), 'id_carpeta'=>$clave),$exclude,'nuevo');
							if($includeVars2[0] != "ERROR")
								$Model->set_dir_user($includeVars2);
						}else{
							$includeVars2 = $this->include_vars(array('cod'=>$parent,'id_carpeta'=>$clave), $exclude, 'nuevo');
							if($includeVars2[0] != "ERROR")
								$Model -> set_dir_perms($includeVars2);
						}
						//$this->updateDirPerms();
						
						$Vista->print_json(array('success'=>'1','case'=>'mkdir','id'=>$clave,'nombre'=>$nombre,'parent'=>$parent));
					}
					$Vista->print_json(array('success'=>'0','case'=>'mkdir','id'=>$clave,'nombre'=>$nombre,'parent'=>$parent));
					exit;

				case 'rm': //OK
					$includeVars	= $this->include_vars(array('cod'=>$getVars['file']), $exclude, 'archivo');
					if( $includeVars[0] != "ERROR" ){
						$files	= $Model->count_files( $includeVars );
						$dirs 	= $Model->count_dirs( $includeVars );
						if( $files[0]['TOTAL'] > 0 || $dirs[0]['TOTAL'] > 0 )
							$msg = ERR104;
						else{
							$b1 = $Model -> rm_carpeta($includeVars);
							$b2 = $Model -> rm_dir_perms($includeVars);
						}
					}else
						$msg = ERR103;
					if( $b1 && $b2 ){
						$Vista->print_json(array('success'=>'1','case'=>'rm'));
					}else
						$Vista->print_json(array('success'=>'0','case'=>'rm'));
					exit;
					break;
					
				case 'download':  // ok
					$type = '';
					$includeVars = $this->include_vars( array('cod'=>$getVars['file']), $exclude, 'getvars' );
					if($includeVars[0] != "ERROR"){
						$_data		= $Model->getFileInfo($includeVars);
						$file_id 	= $_data[0]['P_KEY'];
						$name 		= $_data[0]['NOMBRE'];
						$ubicacion 	= $_data[0]['UBICACION'];
						$path 		= SERVER_ROOT.MASTER_FOLDER.$ubicacion.$file_id;
						if(!$this -> download_file($name,$path,$type))
							$Vista -> alert(ERR126,"",NULL);
					}
					exit;
					break;
					
				case 'filesearch':
					global $exts;
					$cod=array('nombre'=>base64_decode($getVars['nombre']));
					$includeVars = $this->include_vars($cod,$exclude,'nuevo');
					//print_r($includeVars);
					if($includeVars[0] != "ERROR"){
						$_data = $Model->getFileSearch($includeVars);
						if( $_data ){
							foreach( $_data as $key=>$val ){
								$ext = strtolower($_data[$key]['EXT']);
								$_data[$key]['TIPO'] 	= (array_key_exists($ext, $exts)) ? $exts[$ext] : 'Desconocido';
								$_data[$key]['TAMANIO'] = $this->humansize($val['TAMANIO']);
							}
						}
					}
					$Vista -> _data 	= $_data;
					$Vista->target = $getVars['target'];
					break;

				case 'listfiles':
					global $exts;
					$getVars['shlink'] = !isset($getVars['shlink']) ? 0 : $getVars['shlink'];
					
					if( $getVars['cadena'] || $getVars['cadena']=='0' ) 
						$parent = $getVars['cadena'];
					else
						$parent = Session::get('carpeta_actual');
					
					if( $parent ){
						$includeVars = $this->include_vars( array('parent'=>$parent), $exclude, 'getvars' );
						$cod=array('parent'=>$parent,'usuario'=>Session::get('id_usuario'),'lib'=>strval(Session::get('TIPO_LIB')));
						$includeVars2 = $this->include_vars($cod,$exclude,'getdirs');
						if($includeVars[0] != "ERROR"){
							//echo 'test';
							Session::set('carpeta_actual', $parent);
							if( $getVars['shlink'] ){
								$_dirs  = $Model->get_folders_link($includeVars);
							}else{
								$_dirs  = $Model->get_folders($includeVars2);
							}							
							$_data 	= $Model->get_files($includeVars);
							if( $_data ){
								foreach( $_data as $key=>$val ){
									$ext = strtolower($_data[$key]['EXT']);
									$_data[$key]['TIPO'] 	= (array_key_exists($ext, $exts)) ? $exts[$ext] : 'Desconocido';
									$_data[$key]['TAMANIO'] = $this->humansize($val['TAMANIO']);
								}
							}
							if( $_dirs ){
								foreach( $_dirs as $key=>$val ){
									$_dirs[$key]['NOMBRE64'] = base64_encode($val['NOMBRE']);
								}
							}
							$Vista -> address 	= Session::get('carpeta_actual');
							$Vista -> permisos 	= $this->get_perms(Session::get('carpeta_actual'));
							$Vista -> _data 	= $_data;
							$Vista -> _data2 	= $_dirs;
						}
					}
					$Vista->target = $getVars['target'];
					break;
				
				case 'listdir': // ok
					$cod=array('parent'=>$getVars['dir'],'usuario'=>Session::get('id_usuario'),'lib'=>strval(Session::get('TIPO_LIB')));
					$includeVars = $this->include_vars($cod,$exclude,'getdirs');
					if(!$getVars['dir']) $includeVars[0][1]='0';
					if($includeVars[0] != "ERROR"){
						$parent = ( $getVars['dir'] ? $getVars['dir'] : '0');
						$files = $Model->get_folders($includeVars);
						if(is_array($files)){
							foreach( $files as $key=>$val ) {
								if( APLICATION_MODE==0 || APLICATION_MODE==2 )
									$files[$key]['NOMBRE'] = $val['P_KEY'].'-'.$val['NOMBRE'];
								$files[$key]['NOMBRE64'] = base64_encode(utf8_decode($val['NOMBRE']));
							}
						}
						$Vista->data	= $parent;
						$Vista->_data 	= $files;
					}
					break;

				case 'mklink':
					$fecha  = new DateTime(date('Y/m/d'));
					$fecha->add(new DateInterval('P'.$getVars['expira'].'D'));
					$f_cr			= date('d/m/Y');
					
					$f_exp 			= (( $getVars['expira'] )? $fecha->format('d/m/Y'):'');
					
					//$f_exp 			= $fecha->format('d/m/Y');
					$round_code 	= $this->get_rand();
					$tiempo_actual 	= time();
					$hash 			= md5($round_code.$getVars['shlink_dir'].$tiempo_actual);
					
					$cod = array(	'ID_CARPETA'=>$getVars['shlink_dir'],'FECHA_CREACION'=>$f_cr,'FECHA_EXPIRACION'=>$f_exp,
									'CLAVE'=>$getVars['login'],'PERMISOS'=>$getVars['permisos'],'HASH'=>$hash);
					$includeVars = $this->include_vars($cod, $exclude, 'shlink');
					
					if($includeVars[0] != "ERROR"){	
						$Model->set_link($includeVars);
						$_data = array(	'success'=>'1',
										'link'=>base64_encode(SITE_ROOT.'index.php?biblioteca_shlink&id='.$hash));
						$Vista->print_json($_data);
					}else
						$Vista->print_json($_data['success']='0');
					exit;
					break;

				case 'shlink':					
					$includeVars = $this->include_vars( array('HASH'=>$getVars['id']), $exclude, 'shlink' );
					if($includeVars[0] != "ERROR"){	
						$_data = $Model->get_linkdata($includeVars);
						if( $_data[0]['FECHA_EXPIRACION'] ){
							if( $_data[0]['FECHA_EXPIRACION']>date('d/m/Y') ){
								if( $_data[0]['CLAVE'] ){
									if( Session::get('autenticado') ){
										Session::set('pre_aut',true);
									}else
										$this->redireccionar();
								}else
									Session::set('pre_aut',true);
							}else{
								echo 'link vencido';
								exit;
							}
						}else
							Session::set('pre_aut',true);
					}
					$Vista->data = $getVars['id'];
					$Vista->_data = $_data;
					break;

				case 'gdocview':
					$round_code 	= $this->get_rand();
					$tiempo_actual 	= time();
					$hash 			= md5($round_code.$getVars['cod'].$tiempo_actual);
					$cod 	= array('codigo'	=> $round_code);
					$cod2 	= array('codigo'	=> $round_code,
									'hash'		=> $hash,
									'id_archivo'=> $getVars['cod'],
									'tiempo'	=> $tiempo_actual);

					$includeVars = $this->include_vars($cod,$exclude,'otpfile');
					$includeVars2 = $this->include_vars($cod2,$exclude,'otpfile');
					if($includeVars[0] != "ERROR" && $includeVars2[0] != "ERROR"){
						$_data = $Model->count_otpinfo($includeVars);
						if( is_array($_data[0]) ){
							$time = time()-$_data[0]['TIEMPO'];
							$time = $time/60;
							if( $time >= '1' ){
								//borrar
							}
							exit;
						}else{
							$Model->guardar_otp($includeVars2);
							echo $round_code;
						}
					}
					exit;
					break;

				case 'getdirinfo':
					$includeVars = $this->include_vars( array('cod'=>$getVars['cod']), $exclude, 'getvars' );
					if($includeVars[0] != "ERROR"){	
						$_data = $Model->get_dirinfo($includeVars);
						$_data = $this->data2b64($_data);
						$Vista->print_json(array('target'=>$getVars['target'],'data'=>$_data[0]));
						/*
						foreach($_data as $key=>$val){
							foreach($val as $x=>$y){
								$_data[$key][$x] = base64_encode($y); 
							}
						}
						//$Vista->print_json($_data);*/
					}
					//$Vista->_data = $_data;
					//$Vista->target = $getVars['target'];
					break;
					
				case 'getdirgroups':
					$includeVars = $this->include_vars( array('cod'=>$getVars['cod']), $exclude, 'getvars' );
					if($includeVars[0] != "ERROR"){	
						$_data = $Model->get_dirgroups($includeVars);
						/*
						foreach($_data as $key=>$val){
							foreach($val as $x=>$y){
								$_data[$key][$x] = base64_encode($y); 
							}
						}
						//$Vista->print_json($_data);*/
					}
					$Vista->_data = $_data;
					$Vista->target = $getVars['target'];
					break;
/*
				case 'dirprop':
					$includeVars = $this->include_vars( array('cod'=>$getVars['cod']), $exclude, 'getvars' );
					if($includeVars[0] != "ERROR"){	
						$filas = $Model->get_perms($includeVars);
						foreach( $filas as $key=>$val ){
							if( $val['PERMISOS']=='4' )
								$filas[$key]['PERMISOS'] = "Lectura";
							elseif( $val['PERMISOS']=='6' )
								$filas[$key]['PERMISOS'] = "Modificar";
							elseif( $val['PERMISOS']=='7' )
								$filas[$key]['PERMISOS'] = "Descargar";
						}
						$Vista -> _data = $filas;
					}else{
						echo 'Error';
						exit;
					}
					break;*/

				case 'quitperm':
					$cod = array('id_grupo'=>$getVars['grupo'],'id_carpeta'=>$getVars['cod']);
					$includeVars = $this->include_vars($cod, $exclude, 'nuevo');
					if( $includeVars[0] != "ERROR" ){
						$b1 = $Model->rm_perms_childrens($includeVars);					
					}
					if( $b1 ){
						$Vista->print_json(array('success'=>'1','case'=>'rmperm'));
					}else
						$Vista->print_json(array('success'=>'0','case'=>'rmperm'));
					exit;

				case 'addperm':
					$includeVars 	= $this->include_vars(array('id_carpeta'=>$getVars['cod'],'id_grupo'=>$getVars['grupo']), $exclude, 'nuevo');
					if( $includeVars[0] != "ERROR" ){
						$b1 = $Model->rm_perms_parent_childrens($includeVars);					
						$b2 = $Model->set_perms($includeVars);
					}
					if( $b1 && $b2 ){
						$Vista->print_json(array('success'=>'1','case'=>'setperm'));
					}else
						$Vista->print_json(array('success'=>'0','case'=>'setperm'));
					exit;
					
				case 'parentdirs':
					$includeVars = $this->include_vars(array('parent'=>$getVars['parent']),$exclude,'getdirs');
					if($includeVars[0] != "ERROR"){
						$files = $Model->get_parents_dirs($includeVars);
						$Vista->print_json($files);
					}
					break;

				case 'rename': // ok
					$nombre = base64_decode($getVars['nombre']);
					$includeVars = $this->include_vars(array('id_carpeta'=>$getVars['cod'],'nombre'=>$nombre), $exclude, 'nuevo');
					if($includeVars[0] != "ERROR"){
						if($Model -> change_name_dir($includeVars)){						
							$_data = $this->data2b64(array('nombre'=>$nombre,'nombre_original'=>$nombre));
							$Vista->print_json(array('success'=>'1','case'=>'rename','target'=>$getVars['target'],'data'=>$_data));
						}
					}
					$Vista->print_json(array('success'=>'0'));
					break;			

				default:
					if( !Session::get('carpeta_actual') ) Session::set('carpeta_actual', '0');
					$Vista ->_combo = $Model->get_groups();
					$Vista->subpage = MODULO;
			}
			$Vista -> Ver();
		}
		public function humansize($size) {
			$units = array ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
			$k = 0;
			while($size > 1024) {
				$size = $size / 1024;
				$k++;
			}
			if($k > 1)
				return round($size, 1).' '.$units[$k];
			else
				return round($size).' '.$units[$k];
		}
		private function get_rand(){
			$r1 = mt_rand(1000000,9999999);
			$r2 = mt_rand(1000000,9999999);
			$r3 = $this->myRand('9');
			$r4 = $this->myRand('9');
			$r5 = $this->myRand('9');
			$r6 = $this->myRand('9');

			$r10=str_replace($r3,$r4,$r1);
			$r20=str_replace($r5,$r6,$r2);
			$_r10 = str_split($r10);
			$_r20 = str_split($r20);

			$rt = null;
			foreach($_r10 as $key=>$val){
				if ($key % 2) { 
					$rt .= $val;
				} else { 
					$rt .= $_r20[$key];
				}
			}
			return $rt;
		}
		private function myRand($max){
			do{
				$result = floor($max*(hexdec(bin2hex(openssl_random_pseudo_bytes(4)))/0xffffffff));
			}while($result == $max);
			return $result;
		}
		private function get_perms($id){
			if( is_array( Session::get('dir_perms') ) ){
				foreach(Session::get('dir_perms') as $clave=>$valor){
					if($valor['ID_CARPETA'] == $id){
						return $valor['PERMISOS'];
					}
				}
			}
			return false;
		}
	}
?>