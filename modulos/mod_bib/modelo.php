<?php
class Model extends ABModel{
		/*****************************************************************************************************************
														INSERCION DE  REGISTROS
		*****************************************************************************************************************/		
		public function guardar_archivo($bindvars) {
			$sql = "INSERT INTO BIB_ARCHIVOS
					VALUES (:COD,:PARENT,:NOMBRE,:EXT,:TAMANIO,:UBICACION,TO_DATE(:FECHA_SUBIDA,'DD/MM/YYYY'),:ESTADO)";
			return $this->single_query($sql,$bindvars);		
		}
		public function set_dir_user($bindvars) {
			$sql = "INSERT INTO BIB_PERMISOS
					SELECT SQ_BIB_PERMISOS.NEXTVAL, :ID_CARPETA, ID_GRUPO
					FROM (
						SELECT ID_USUARIO, ID_GRUPO, RANK() OVER (PARTITION BY ID_USUARIO ORDER BY P_KEY ASC) ORDEN
						FROM BIB_GRUPOS_USUARIOS
						WHERE ID_USUARIO = :ID_USUARIO
					)
					WHERE ORDEN = 1";
			return $this->single_query($sql,$bindvars);
		}
		public function guardar_otp($bindvars) {
			$sql = "INSERT INTO A_OTPFILES  VALUES(SQ_A_OTPFILES.NEXTVAL, :CODIGO, :HASH, :ID_ARCHIVO, :TIEMPO, '1')";
			return $this->single_query($sql,$bindvars);
		}
		public function make_dir($bindvars) {
			$sql = "INSERT INTO BIB_CARPETAS VALUES(SQ_BIB_CARPETAS.NEXTVAL, :NOMBRE, :PARENT, :LIB, '1')";
			return $this->single_query($sql,$bindvars);
		}
		public function set_dir_perms($bindvars) {
			$sql = "INSERT INTO BIB_PERMISOS (P_KEY, ID_CARPETA, ID_GRUPO )
					SELECT SQ_BIB_PERMISOS.NEXTVAL, :ID_CARPETA, ID_GRUPO FROM BIB_PERMISOS WHERE ID_CARPETA = :COD";
			return $this->single_query($sql,$bindvars);
		}
		public function set_perms($bindvars) {
			$sql = "INSERT INTO BIB_PERMISOS (P_KEY, ID_CARPETA, ID_GRUPO)
					SELECT SQ_BIB_PERMISOS.NEXTVAL, P_KEY, :ID_GRUPO 
					FROM (
					SELECT P_KEY FROM BIB_CARPETAS
					START WITH PARENT = :ID_CARPETA
					CONNECT BY PRIOR P_KEY = PARENT
					UNION
					SELECT P_KEY FROM BIB_CARPETAS
					START WITH P_KEY = :ID_CARPETA
					CONNECT BY PRIOR  PARENT = P_KEY)";
			return $this->single_query($sql,$bindvars);
		}
		public function set_link($bindvars) {
			$sql = "INSERT INTO A_LINKS
					VALUES (SQ_A_LINKS.NEXTVAL,:ID_CARPETA,TO_DATE(:FECHA_CREACION,'DD/MM/YYYY'),TO_DATE(:FECHA_EXPIRACION,'DD/MM/YYYY'),:CLAVE,:PERMISOS,:HASH,'1')";
			return $this->single_query($sql,$bindvars);
		}
		/*
		public function set_history($bindvars) {
			$sql = "INSERT INTO A_HISTORIAL VALUES (SQ_A_HISTORIAL.NEXTVAL,:ID_USUARIO,:ID_ARCHIVO,:TIPO_CONSULTA,TO_DATE(:FECHA,'DD/MM/YYYY'),:ACCION,'1')";
			return $this->single_query($sql,$bindvars);
		}
		/*****************************************************************************************************************
														SELECCION DE  REGISTROS
		*****************************************************************************************************************/
		public function checkParentDir($bindvars) {
			$sql = "SELECT COUNT(*) AS TOTAL FROM BIB_CARPETAS WHERE P_KEY = :PARENT AND PARENT=:COD";
			return $this->query_fetch($sql,$bindvars);
		}
		public function get_history($bindvars) {
			$sql = "SELECT * FROM  (
						SELECT * FROM(
							SELECT A_HISTORIAL.P_KEY, USER_LIST.USUARIO, BIB_CARPETAS.NOMBRE AS ARCHIVO, TO_CHAR(FECHA,'DD/MM/YYYY') AS FECHA,ACCION, TIPO_CONSULTA
							FROM A_HISTORIAL
							LEFT JOIN USER_LIST ON USER_LIST.ID = A_HISTORIAL.ID_USUARIO
							LEFT JOIN BIB_CARPETAS ON BIB_CARPETAS.P_KEY = A_HISTORIAL.ID_ARCHIVO
							WHERE TIPO_CONSULTA=1 AND BIB_CARPETAS.P_KEY=:COD
							UNION
							SELECT A_HISTORIAL.P_KEY, USER_LIST.USUARIO, BIB_ARCHIVOS.NOMBRE AS ARCHIVO, TO_CHAR(FECHA,'DD/MM/YYYY') AS FECHA,ACCION, TIPO_CONSULTA
							FROM A_HISTORIAL
							LEFT JOIN USER_LIST ON USER_LIST.ID = A_HISTORIAL.ID_USUARIO
							LEFT JOIN BIB_ARCHIVOS ON BIB_ARCHIVOS.P_KEY = A_HISTORIAL.ID_ARCHIVO
							WHERE TIPO_CONSULTA=2 AND BIB_ARCHIVOS.PARENT=:COD) 
						ORDER BY P_KEY DESC ) 
					WHERE ROWNUM <= 30";
			return $this->query_fetch($sql,$bindvars);
		}
		public function get_linkdata($bindvars) {
			$sql = "SELECT ID_CARPETA,TO_CHAR(FECHA_CREACION,'DD/MM/YYYY') AS FECHA_CREACION,TO_CHAR(FECHA_EXPIRACION,'DD/MM/YYYY') AS FECHA_EXPIRACION,CLAVE,PERMISOS,HASH  
					FROM A_LINKS
					WHERE HASH = :HASH";
			return $this->query_fetch($sql,$bindvars);
		}
		public function count_otpinfo($bindvars) {
			$sql = "SELECT P_KEY, CODIGO, ID_ARCHIVO, HASH, TIEMPO, ESTADO FROM A_OTPFILES
					WHERE CODIGO = :CODIGO";
			return $this->query_fetch($sql,$bindvars);
		}
		/*
		public function get_user_groups($bindvars) {
			$sql = "SELECT BIB_PERMISOS.ID_CARPETA, MAX(BIB_PERMISOS.PERMISOS) AS PERMISOS
					FROM BIB_PERMISOS
					LEFT JOIN BIB_GRUPOS_USUARIOS ON BIB_GRUPOS_USUARIOS.ID_GRUPO = BIB_PERMISOS.ID_USUARIO
					WHERE BIB_GRUPOS_USUARIOS.ID_USUARIO = :COD
					GROUP BY(BIB_PERMISOS.ID_CARPETA)
					ORDER BY BIB_PERMISOS.ID_CARPETA DESC";
			return $this->query_fetch($sql,$bindvars);
		}
		public function file_perms($bindvars) {
			$sql = "SELECT BIB_ARCHIVOS.P_KEY, MAX(T.PERMISOS) AS PERMISOS
					FROM BIB_ARCHIVOS
					LEFT JOIN ( SELECT DISTINCT BIB_PERMISOS.ID_CARPETA, BIB_PERMISOS.PERMISOS, BIB_GRUPOS_USUARIOS.ID_USUARIO 
								FROM BIB_PERMISOS
								LEFT JOIN BIB_GRUPOS_USUARIOS ON BIB_GRUPOS_USUARIOS.ID_GRUPO = BIB_PERMISOS.ID_USUARIO
								WHERE BIB_GRUPOS_USUARIOS.ID_USUARIO = :COD) T ON T.ID_CARPETA = PARENT
					WHERE T.ID_USUARIO IS NOT NULL 
					AND BIB_ARCHIVOS.ESTADO = 1
					GROUP BY(BIB_ARCHIVOS.P_KEY)
					ORDER BY BIB_ARCHIVOS.P_KEY DESC";
			return $this->query_fetch($sql,$bindvars);
		}		
		public function verifica_existencia($bindvars) {
			$sql = "SELECT COUNT(*) AS TOTAL FROM BIB_PERMISOS WHERE ID_USUARIO=:ID_GRUPO AND ID_CARPETA=:ID_CARPETA";
			$filas = $this->query_fetch($sql,$bindvars);
			if($filas[0]['TOTAL'] > 0){
				return false;
			}else{
				return true;
			}
		}*/
		public function get_folders_link($bindvars) {
			$sql = "SELECT DISTINCT BIB_CARPETAS.P_KEY, BIB_CARPETAS.NOMBRE, BIB_CARPETAS.PARENT, BIB_CARPETAS.ESTADO
                    FROM BIB_CARPETAS
                    WHERE BIB_CARPETAS.PARENT = :PARENT 
                    AND BIB_CARPETAS.ESTADO = '1'
                    ORDER BY BIB_CARPETAS.NOMBRE";
			return $this->query_fetch($sql,$bindvars);
		}
		public function get_folders($bindvars) {
			$sql = "SELECT BIB_CARPETAS.P_KEY, BIB_CARPETAS.NOMBRE, BIB_CARPETAS.PARENT 
                    FROM BIB_CARPETAS
                    LEFT JOIN (
                        SELECT BIB_PERMISOS.P_KEY, BIB_PERMISOS.ID_CARPETA, BIB_GRUPOS_USUARIOS.PERMISO,
                            RANK() OVER (PARTITION BY BIB_PERMISOS.ID_CARPETA ORDER BY BIB_PERMISOS.P_KEY) ORDEN
                        FROM BIB_PERMISOS
                        LEFT JOIN BIB_GRUPOS_USUARIOS ON BIB_GRUPOS_USUARIOS.ID_GRUPO = BIB_PERMISOS.ID_GRUPO
                        WHERE BIB_GRUPOS_USUARIOS.ID_USUARIO = :USUARIO
                        ORDER BY ID_CARPETA ASC
                    ) T0 ON T0.ID_CARPETA = BIB_CARPETAS.P_KEY
                    WHERE BIB_CARPETAS.PARENT = :PARENT
                    AND BIB_CARPETAS.LIB IN(0,:LIB)
                    AND BIB_CARPETAS.ESTADO = '1'
                    AND ORDEN = 1
                    ORDER BY BIB_CARPETAS.NOMBRE";
			return $this->query_fetch($sql,$bindvars);
		}
		public function get_files($bindvars) {
			$sql = "SELECT P_KEY, NOMBRE, PARENT, TO_CHAR(FECHA_SUBIDA,'DD/MM/YYYY') AS FECHA_SUBIDA, TAMANIO, EXT
					FROM BIB_ARCHIVOS 
					WHERE PARENT = :PARENT AND ESTADO = '1' ORDER BY NOMBRE";
			return $this->query_fetch($sql,$bindvars);
		}
		public function getFileSearch($bindvars) {
			$sql = "SELECT P_KEY, NOMBRE, EXT, TAMANIO, TO_CHAR(FECHA_SUBIDA,'DD/MM/YYYY') AS FECHA_SUBIDA
					FROM BIB_ARCHIVOS
					WHERE UPPER(BIB_ARCHIVOS.NOMBRE) LIKE '%' || UPPER(:NOMBRE) || '%' AND ESTADO='1'";
			return $this->query_fetch($sql,$bindvars);
		}
		/*
		public function get_childrens_files($bindvars) {
			$sql = "SELECT P_KEY, NOMBRE 
					FROM BIB_ARCHIVOS
					WHERE PARENT IN (
						SELECT BIB_CARPETAS.P_KEY
						FROM BIB_CARPETAS
						START WITH BIB_CARPETAS.P_KEY = (SELECT BIB_CARPETAS.P_KEY
							FROM BIB_CARPETAS
							LEFT JOIN A_LINKS ON A_LINKS.ID_CARPETA = BIB_CARPETAS.P_KEY 
							WHERE A_LINKS.HASH = :HASH)
						CONNECT BY PRIOR BIB_CARPETAS.P_KEY = BIB_CARPETAS.PARENT)
					AND ESTADO = 1";
			return $this->query_fetch($sql,$bindvars);
		}*
		public function get_childrens($bindvars) {
			$sql = "SELECT BIB_CARPETAS.P_KEY, BIB_CARPETAS.NOMBRE 
					FROM BIB_CARPETAS
					START WITH BIB_CARPETAS.P_KEY = (SELECT BIB_CARPETAS.P_KEY
						FROM BIB_CARPETAS
						LEFT JOIN A_LINKS ON A_LINKS.ID_CARPETA = BIB_CARPETAS.P_KEY 
						WHERE A_LINKS.HASH = :HASH)
					CONNECT BY PRIOR BIB_CARPETAS.P_KEY = BIB_CARPETAS.PARENT";
			return $this->query_fetch($sql,$bindvars);
		}*/
		public function get_parents_dirs($bindvars) {
			$sql = "SELECT P_KEY, NOMBRE FROM BIB_CARPETAS
						START WITH P_KEY = :PARENT
						CONNECT BY PRIOR PARENT = P_KEY";
			return $this->query_fetch($sql,$bindvars);
		}
		/*
		public function get_all_folders() {
			$sql = "SELECT P_KEY, NOMBRE, PARENT, ESTADO
					FROM BIB_CARPETAS
					WHERE ESTADO = '1'  ORDER BY NOMBRE";
			return $this->query_fetch($sql,null);
		}*/
		public function get_groups() {
			$sql = "SELECT P_KEY, NOMBRE
					FROM BIB_GRUPOS
					WHERE ESTADO = '1' ORDER BY NOMBRE";
			return $this->query_fetch($sql,null);
		}
		public function get_perms($bindvars) {
			$sql = "SELECT BIB_PERMISOS.P_KEY, BIB_GRUPOS.NOMBRE AS GRUPO, PERMISOS , BIB_GRUPOS.P_KEY AS ID_GRUPO
					FROM BIB_PERMISOS 
					LEFT JOIN BIB_GRUPOS ON BIB_GRUPOS.P_KEY = BIB_PERMISOS.ID_USUARIO
					WHERE ID_CARPETA = :COD";
			return $this->query_fetch($sql,$bindvars);
		}
		public function get_dirinfo($bindvars) {
			$sql = "SELECT P_KEY AS COD, NOMBRE, NOMBRE AS NOMBRE_ORIGINAL
					FROM BIB_CARPETAS 
					WHERE P_KEY=:COD";
			return $this->query_fetch($sql,$bindvars);
		}
		public function get_dirgroups($bindvars) {
			$sql = "SELECT BIB_GRUPOS.P_KEY, BIB_GRUPOS.NOMBRE
					FROM BIB_PERMISOS
					LEFT JOIN BIB_GRUPOS ON BIB_GRUPOS.P_KEY=BIB_PERMISOS.ID_GRUPO
					WHERE BIB_PERMISOS.ID_CARPETA = :COD";
			return $this->query_fetch($sql,$bindvars);
		}	
		public function count_files($bindvars) {
			$sql = "SELECT COUNT(*) AS TOTAL FROM BIB_ARCHIVOS
					WHERE PARENT = :COD AND ESTADO='1'";
			return $this->query_fetch($sql,$bindvars);
		}
		public function count_dirs($bindvars) {
			$sql = "SELECT COUNT(*) AS TOTAL FROM BIB_CARPETAS
					WHERE PARENT = :COD AND ESTADO='1'";
			return $this->query_fetch($sql,$bindvars);
		}
		
		/*****************************************************************************************************************
														ACTUALIZACION DE  REGISTROS
		*****************************************************************************************************************/
		public function updParentFile($bindvars) {
			$sql = "UPDATE BIB_ARCHIVOS SET PARENT = :PARENT WHERE P_KEY=:COD";
			return $this->single_query($sql,$bindvars);
		}
		public function updParentDir($bindvars,$set=null) {
			$sql = "UPDATE BIB_CARPETAS SET PARENT = :PARENT ".$set." WHERE P_KEY=:COD";
			return $this->single_query($sql,$bindvars);
		}
		public function actualizar_archivo($bindvars) {
			$sql = "UPDATE BIB_ARCHIVOS  SET ESTADO = :ESTADO WHERE P_KEY = :COD";
			return $this->single_query($sql,$bindvars);
		}
		/*
		public function actualizar_carpeta($bindvars) {
			$sql = "UPDATE BIB_CARPETAS  SET ESTADO = :ESTADO WHERE P_KEY = :COD";
			return $this->single_query($sql,$bindvars);
		}*/
		public function change_name_dir($bindvars) {
			$sql = "UPDATE BIB_CARPETAS  SET NOMBRE = :NOMBRE WHERE P_KEY = :ID_CARPETA";
			return $this->single_query($sql,$bindvars);
		}
		/*****************************************************************************************************************
														ELIMINACION DE  REGISTROS
		*****************************************************************************************************************/
		public function rm_perms_childrens($bindvars) { 
			$sql = "DELETE FROM BIB_PERMISOS WHERE BIB_PERMISOS.ID_GRUPO = :ID_GRUPO AND ID_CARPETA IN(
					SELECT P_KEY FROM BIB_CARPETAS
					START WITH P_KEY = :ID_CARPETA
					CONNECT BY PRIOR P_KEY = PARENT)";
			return $this->single_query($sql,$bindvars);
		}
		public function rm_perms_parent_childrens($bindvars) { 
			$sql = "DELETE FROM BIB_PERMISOS WHERE BIB_PERMISOS.ID_GRUPO = :ID_GRUPO AND ID_CARPETA IN(
					SELECT P_KEY FROM BIB_CARPETAS
					START WITH PARENT = :ID_CARPETA
					CONNECT BY PRIOR P_KEY = PARENT
					UNION
					SELECT P_KEY FROM BIB_CARPETAS
					START WITH P_KEY = :ID_CARPETA
					CONNECT BY PRIOR  PARENT = P_KEY)";
			return $this->single_query($sql,$bindvars);
		}
		public function rm_dir_perms($bindvars) {
			$sql = "DELETE FROM BIB_PERMISOS WHERE ID_CARPETA = :COD";
			return $this->single_query($sql,$bindvars);
		}
		public function rm_carpeta($bindvars) {
			$sql = "DELETE FROM BIB_CARPETAS WHERE P_KEY = :COD";
			return $this->single_query($sql,$bindvars);
		}
	}
?>