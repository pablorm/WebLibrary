<?php
    /***************************************************************************************************
											CONTROL DE VISTAS
	****************************************************************************************************/
class Vista extends MView {
	public $subpage;
	public $data;
	public $_data;
	public $_data2;
	public $_combo;
	public $address;	
	public $permisos;
		
	function Ver(){		
		$html = $this->get_template($this->subpage);		
		switch($this->subpage){
			case 'getdirgroups':
				$html = $this->renderGrid($html, $this->_data, 'LISTADO');
				$html = base64_encode($html);
				$this->print_json(array('target'=>$this->target,'append'=>$html));
				break;
			case 'history':
				$html = $this->renderGrid($html, $this->_data, 'LISTADO');
				break;
			case 'shlink':
				$html = $this->renderDetail($html, $this->_data);
				$html = str_replace('{SITE_ROOT}',SITE_ROOT,$html);
				break;
			case 'dirprop':
				$html = $this->renderGrid($html, $this->_data, 'LISTADO');
				break;
				
			case 'filesearch':
				$html = $this->renderGrid($html, $this->_data, 'LISTADO');
				$html = base64_encode($html);
				$this->print_json(array('target'=>$this->target,'append'=>$html));
				break;
						
			case 'listfiles':			
				if( !$this->address )	$this->address  = '0';
				if( !$this->permisos ) 	$this->permisos = 6;
				$html = str_replace('{ruta}',$this->address,$html);
				$html = str_replace('{permisos}',$this->permisos,$html);
				$html = $this->renderGrid($html, $this->_data, 'LISTADO'); // ARCHIVOS
				$html = $this->renderGrid($html, $this->_data2, 'LISTADO2',''); // CARPETAS
				
				$html = base64_encode($html);
				$this->print_json(array('target'=>$this->target,'append'=>$html));
				break;
			case 'listdir':
				$html = str_replace('{PARENT}', $this->data,$html);
				$html = $this->renderGrid($html, $this->_data, 'LISTADO','E');
				break;
			case MODULO:
				$html = str_replace('{SITE_ROOT}',SITE_ROOT,$html);
				$html = str_replace('{modulo}',MODULO,$html);
				$html = $this->render_combo($html, $this->_combo,"COMBO_GRUPO","P_KEY","NOMBRE");
				break;				
		}
		print $html;
	}
}
?>