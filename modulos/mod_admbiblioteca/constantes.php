<?php

	$diccionario = array(
		'formulario'=>array('action_search'=>'index.php?'.MODULO.'_grid',
							'titulo'=>'Accesos Biblioteca',
							'ruta_regreso'=>'index.php?'.MODULO,
							'modulo'=>MODULO,
							'ruta'=>SITE_ROOT,
							'permisos'=>Controller::get_Permission(MODULO)
							
		)
	);
	
	$listado_inputs=array(	'permisos' =>	array(	'PCOD'=>array(	'largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'), 
																	
												'PID_USUARIO'=>array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no'), 
																	
												'PID_GRUPO'=>array(	'largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'), 
																	
												'PPERMISO'=>array(	'largo'=>1,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no')),
																		
							'grupo' =>	array(	'GCOD'=>array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no'), 
																	
												'GNOMBRE'=>array(		'largo'=>45,
																		'tipo'=>'text',
																		'valido_nulo'=>'no')),
																																						
							'nuevo' =>	array(	'NCOD'=>array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no'), 
																	
												'NID_CARGO'=>array(		'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no'),
																	
												'NID_CC' => array(		'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no'),
																		
												'NN_PASE' => array(		'largo'=>45,
																		'tipo'=>'text',
																		'valido_nulo'=>'no'),
																	
												'NF_INICIO_CONTRATO' =>array('largo'=>10,
																		'tipo'=>'fecha',
																		'valido_nulo'=>'si'),
																	
												'NID_CONTRATO' =>array('largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'si')),
																																						
																								
							'getvars'=>array(	'COD' =>	array(	'largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no')),
							
							
							'buscador' =>	array(	'FOLIO' =>array('largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'si'),																															
																		
													'NOMBRES' => array( 'largo'=>90,
																		'tipo'=>'text',
																		'valido_nulo'=>'si'),
																		
													'ID_CARGO'=>array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'si'),
																	
													'ID_CC' => array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'si'),
																			
													'N_PASE' => array(	'largo'=>45,
																		'tipo'=>'text',
																		'valido_nulo'=>'si'),
																		
													'F_INICIO_CONTRATO' =>array(	'largo'=>10,
																					'tipo'=>'fecha',
																					'valido_nulo'=>'si'),
																		
													'ID_CONTRATO' =>array(	'largo'=>6,
																			'tipo'=>'integer',
																			'valido_nulo'=>'si')
													
												)
							);


?>
