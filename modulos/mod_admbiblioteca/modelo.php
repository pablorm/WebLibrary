<?php
class Model extends ABModel{
		/*****************************************************************************************************************
														ACTUALIZAR REGISTROS
		*****************************************************************************************************************/
		public function setGrupo($bindvars) {
			return $this->execProcedure("INS_LIBRARY.IGROUP(:GNOMBRE)",$bindvars, false);
		}
		public function insertPermiso($bindvars) {
			return $this->execProcedure("INS_LIBRARY.IPERMISO(:PCOD,:PID_GRUPO,:PPERMISO)",$bindvars, false);
		}
		/*****************************************************************************************************************
														OBTENCION DE REGISTROS
		*****************************************************************************************************************/
		
		public function getGruposPermisos($bindvars) {			
			$this->execProcedure("SEL_LIBRARY.GROUPS_PERMS(:COD, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		
		public function getGrupos() {			
			$this->execProcedure("SEL_LIBRARY.SGROUPS(:CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function obtain_results($bindvars,$getVars) {
			$where = $this->build_where($getVars);
			$sql = "SELECT * from (
						SELECT a.*, rownum rnum  FROM (
							SELECT ID AS COD, USUARIO AS NOMBRE
							FROM U_LISTADO
							WHERE ".$where."
						)
						a WHERE rownum < :FINALPAGINA
					)
					WHERE rnum >= :INICIOPAGINA";
			return $this->query_fetch($sql,$bindvars);
		}		
		public function obtain_number_rows($bindvars,$getVars) {
			$where = $this->build_where($getVars);
			$sql = "SELECT COUNT(*) AS TOTAL					
					FROM U_LISTADO
					WHERE  ".$where;
			return $this->query_fetch($sql,$bindvars);
		}
		/*****************************************************************************************************************
														CONSTRUCCION DE CONSULTAS 
		*****************************************************************************************************************/
		public function editGrupo($bindvars){
			return $this->execProcedure("UPD_LIBRARY.UGROUP(:GNOMBRE,:GCOD)",$bindvars, false);
		}/*****************************************************************************************************************
														CONSTRUCCION DE CONSULTAS 
		*****************************************************************************************************************/
		public function rmRegistro($tabla,$clave,$bindvars){
			$sql="DELETE FROM ".$tabla." WHERE ".$clave."=:COD";
			return $this->single_query($sql,$bindvars);			
		}
		/*****************************************************************************************************************
														CONSTRUCCION DE CONSULTAS 
		*****************************************************************************************************************/
		
		
		public function build_where($getVars) {
			$where="1 = 1";
			/*****************************************************************************************************************/
			
			if ($getVars['folio'] <> "") { $where.=" AND RH_PERSONAL.P_KEY=:FOLIO";}
			if ($getVars['rut'] <> "") { $where.=" AND RH_PERSONAL.RUT=:RUT";}
			if ($getVars['nombres'] <> "") { $where.=" AND UPPER(RH_PERSONAL.NOMBRES) LIKE '%' || UPPER(:NOMBRES) || '%' ";}
			
			if (array_key_exists('id_cc',$getVars) && $getVars['id_cc'] <> "0") { $where.=" AND ID_CC=:ID_CC "; }
			if (array_key_exists('id_cargo',$getVars) && $getVars['id_cargo'] <> "0") { $where.=" AND ID_CARGO=:ID_CARGO "; }
			if (array_key_exists('id_contrato',$getVars) && $getVars['id_contrato'] <> "0") { $where.=" AND ID_CONTRATO=:ID_CONTRATO "; }
			
			/*****************************************************************************************************************/
			$where.=" AND U_LISTADO.ESTADO='1' ORDER BY U_LISTADO.ID DESC";		
			return $where;
		}
		
		
	}
	




