<?php
	const MODULO = 'admbiblioteca';	
	include_once(SERVER_ROOT . MODULOS . '/mod_'. MODULO .'/constantes.php');
	include_once(SERVER_ROOT . MODULOS . '/mod_'. MODULO .'/modelo.php');
	include_once(SERVER_ROOT . MODULOS . '/mod_'. MODULO .'/vista.php');
	
	class Controller extends Controllermass
	{	
		
		
		public function main(array $getVars,$subpage){
			global $formatos_registrados;
			$Model = new Model;	
			$Vista = new Vista;
			$Vista->subpage = $subpage;
			$exclude=array();
			
			switch($subpage){
				case 'selectgrupos':
					$Vista -> _data  = $Model->getGrupos($includeVars);
					$Vista->target = $getVars['target'];
					break;
				case 'rmgrupo':
					$includeVars	= $this->include_vars(array('cod'=>$getVars['cod']), $exclude, 'getvars');
					if( $includeVars[0] != "ERROR" ){
						if( $Model->rmRegistro('A_GRUPOS','P_KEY',$includeVars) )
							$Vista->print_json(array('success'=>'1','case'=>$subpage));
					}
					$Vista->print_json(array('success'=>'0','case'=>$subpage));
					exit;
				case 'rmpermiso':				
					$includeVars	= $this->include_vars(array('cod'=>$getVars['cod']), $exclude, 'getvars');
					if( $includeVars[0] != "ERROR" ){
						if( $Model->rmRegistro('A_GRUPOS_USUARIOS','P_KEY',$includeVars) )
							$Vista->print_json(array('success'=>'1','case'=>$subpage));
					}
					$Vista->print_json(array('success'=>'0','case'=>$subpage));
					exit;
					
				case 'setpermiso':
					
					$includeVars = $this->include_vars($getVars, $exclude, 'permisos');
					$includeVars = $this->array_utf8($includeVars,'DECODE');
					if( $includeVars[0] != "ERROR" ){
						if($Model->insertPermiso( $includeVars ) )
							$Vista->print_json(array('success'=>'1','case'=>$subpage));	
						
					}
					$Vista->print_json(array('success'=>'0','case'=>$subpage));
					exit;	
				case 'setgrupo':
					if(!$getVars['Gcod'])
						unset($getVars['Gcod']);
					$includeVars = $this->include_vars($getVars, $exclude, 'grupo');
					$includeVars = $this->array_utf8($includeVars,'DECODE');
					if( $includeVars[0] != "ERROR" ){
						if($getVars['Gcod']){
							if($Model->editGrupo( $includeVars ) )
								$Vista->print_json(array('success'=>'1','case'=>$subpage));							
						}else{
							if($Model->setGrupo( $includeVars ) )
								$Vista->print_json(array('success'=>'1','case'=>$subpage));
						}
					}
					$Vista->print_json(array('success'=>'0','case'=>$subpage));
					exit;
					
				case 'gridpermisos':
					$includeVars = $this->include_vars(array('cod'=>$getVars['cod']),$exclude,'getvars');					
					if($includeVars[0] != "ERROR"){
						$_data  = $Model->getGruposPermisos($includeVars);
						
						foreach($_data as $key=>$val){
							$_data[$key]['DEN_PERMISO'] = $this->get_value_xmllist('permisos',$val['PERMISO']);
						}
						
						$Vista -> _data  = $_data;
						
						$Vista->target = $getVars['target'];
					}else
						exit;					
					break;
				case 'gridgrupos':
					$Vista -> _data  = $Model->getGrupos($includeVars);
					//print_r($Vista -> _data );
					$Vista->target = $getVars['target'];
					break;
					
				case 'gridpermisos':
					$Vista -> _data  = $Model->getGrupos($includeVars);
					$Vista->target = $getVars['target'];
					break;
					
				case 'detalle':
					$cod=array('cod'=>$getVars['cod']);
					$includeVars = $this->include_vars($cod,$exclude,'getvars');					
					if($includeVars[0] != "ERROR"){
						$_data = $Model -> obtain_results_by_id($includeVars);						
						$_data = $this->data2b64($_data);
						$Vista->print_json(array('target'=>$getVars['target'],'data'=>$_data[0]));					
					}
					exit;					
					break;
					
				case 'guardar':
					if( $getVars['accion']=="MODIFICAR" ){						
						$includeVars = $this->include_vars($getVars,$exclude,'nuevo');
						$includeVars = $this->array_utf8($includeVars,'DECODE');
						if($Model -> update_data($includeVars)){
							$Vista->print_json(array('success'=>'1'));
						}else
							$Vista->print_json(array('success'=>'0'));
							
					}
					
					
					exit;
					break;
					
				case 'grid':
				/*
					$includeVars = $this->include_vars(array('cod'=>'4'),$exclude,'getvars');					
					if($includeVars[0] != "ERROR"){
						$_data  = $Model->getGruposPermisos($includeVars,true);
					}
					echo 'test';
					print_r($_data);*/
					/*******/
					$includeVars = $this->include_vars($getVars,$exclude,'buscador');
					$includeVars_p = $this->paginar($includeVars,$getVars['iniciopagina'],$getVars['cantidad_filas']);
					$_data = $Model->obtain_results($includeVars_p,$getVars);
					$Vista -> _data = $_data;
					$Vista -> filas = $Model->obtain_number_rows($includeVars,$getVars);
					break;
					
				default:
					$Vista -> subpage = MODULO;
					$Vista -> combo1 = $this->XML2Select('permisos','1');
			}
			$Vista -> Ver();
		}
		

	}