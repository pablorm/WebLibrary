<?php
    /***************************************************************************************************
											CONTROL DE VISTAS
	****************************************************************************************************/
	include_once(SERVER_ROOT .'/includes/class.excel.php');
	
class Vista extends MView {
	public $subpage;
	public $tipo;	
	public $_data;
	public $data;
	public $array_data;
	public $filas;
	public $combo1;
	public $combo2;
	
	public $combo_responsable;
	public $combo_fallas;
	public $clave;
	public $site;
	public $combo_tinspeccion;
	
	function Ver(){
		global $diccionario;			
		$html = $this->get_template($this->subpage);
		$html = $this->render_dinamic_data($html, $diccionario['formulario']);
		$html = $this->mensaje_error($html,$this->array_data,$this->clave);
		
		switch($this->subpage){
			case 'selectgrupos':
				$html='';
				if(is_array($this->_data)){
					foreach($this->_data as $key=>$val){
						$html .= "<option value=".$val['P_KEY'].">".$val['NOMBRE']."</option>"; 
						
					}
				}
				$html = base64_encode($html);
				$this->print_json(array('target'=>$this->target,'append'=>$html));
				break;
			
			case 'gridpermisos':
			case 'gridgrupos':
				$html = $this->renderGrid($html, $this->_data, 'LISTADO');
				$html = base64_encode($html);
				$this->print_json(array('target'=>$this->target,'append'=>$html));
				break;
				
			case 'grid':
				$html = $this->renderGrid($html, $this->_data, 'LISTADO','E');	
				$html = str_replace('{filas}',$this->filas[0]['TOTAL'],$html);
				break;
				
			case MODULO:
				$html = $this->renderCombo($html, $this->combo1,"COMBO_PERMISOS");
				$html = $this->render_dinamic_data($html, $diccionario['resultados']);
				break;				
		}			
		print $html;
			
		
	}
}
	
	