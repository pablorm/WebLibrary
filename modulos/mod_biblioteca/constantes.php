<?php
	const MODULO = 'biblioteca';
	//const Nivel_Modulo= NIVEL_biblioteca;
	
	/********************************* CONTROL DE ERRORES ********************************************/
	
	const ERR101 = 'Error: No fue posible eliminar archivo | Cod: ERR101';
	const ERR103 = 'Error: No se pudo eliminar carpeta | Cod: ERR103';
	const ERR104 = 'Error: Esta carpeta contiene archivos, no se puede eliminar | Cod: ERR104';
	const ERR105 = 'Error: Carpeta no existe | Cod: ERR105';
	const ERR107 = 'Error: No se pudo renombrar la carpeta | Cod: ERR107';
	const ERR109 = 'Error: No se pudo crear la carpeta | Cod: ERR109';
	const ERR111 = 'Error: No se pudo copiar el archivo | Cod: ERR111';
	const ERR112 = 'Error: Archivo no existe | Cod: ERR112';
	const ERR114 = 'Error: no se pudo renombrar | Cod: ERR114';
	const ERR115 = 'Error: Archivo no existe | Cod: ERR115';
	const ERR117 = 'Error: Desconocido | Cod: ERR117';
	const ERR118 = 'Error: Existe un archivo con el mismo nombre | Cod: ERR118';
	const ERR119 = 'Error: No existe archivo de origen | Cod: ERR119';
	const ERR120 = 'Error: Desconocido 2  | Cod: ERR120';
	const ERR122 = 'Error: Archivo Copiado No Cortado | Cod: ERR122';
	const ERR123 = 'Error: Desconocido | Cod: ERR123';
	const ERR124 = 'Error: Existe un archivo con el mismo nombre | Cod: ERR124';
	const ERR125 = 'Error: No existe archivo de origen | Cod: ERR125';
	const ERR126 = 'Error al descargar archivo';
	const MES100 = 'Sin Mensajes';
	const MES101 = 'Archivo Cortado Correctamente';
	const MES102 = 'Archivo Copiado Correctamente';
	const MES103 = 'Archivo eliminado Correctamente';
	const MES105 = 'Carpeta creada correctamente';
	const MES106 = 'La carpeta se ha renombrado correctamente';
	const MES107 = 'Se ha renombrado correctamente';
	const MES108 = 'Carpeta Eliminada';

	
	/***********************************************************************************************/
	
	$diccionario = array(
		'rename'=>array(	'action'=>'index.php?'.MODULO.'_guardar',
							'titulo'=>'CAMBIAR NOMBRE'
		),
		
		
		
		'formulario'=>array('action'=>'index.php?'.MODULO.'_guardar',
							'titulo'=>'Personal',
							'ruta_regreso'=>'index.php?'.MODULO,
							'modulo'=>MODULO
							
		)
	);
	
	
	$_permisos = array(	'LECTURA'=>array('history','download','listfiles','listdir','gdocview','getdirinfo','parentdirs'),
						'MODIFICAR'=>array('history','gdocview','delete','download','upload','mkdir','rm','listfiles','parentdirs','listdir','mklink','shlink','getdirinfo','dirprop','quitperm','addperm','rename'),
						'DESCARGAR'=>array('history','gdocview','delete','download','upload','mkdir','rm','listfiles','parentdirs','listdir','mklink','shlink','getdirinfo','dirprop','quitperm','addperm','rename'));
	
	$_permisos_shlink = array(	'LECTURA'=>array('history','parentdirs','listfiles','download'),
								'MODIFICAR'=>array('history','parentdirs','listfiles','download','delete','mkdir','rm','upload'));
	
	$listado_inputs=array(	'historial'=>array(	'ID_USUARIO' =>	array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'si'),
												'ID_ARCHIVO' =>	array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'si'),
												'TIPO_CONSULTA'=>array(	'largo'=>1,
																		'tipo'=>'integer',
																		'valido_nulo'=>'si'),
												'FECHA'=>array(	'largo'=>10,
																'tipo'=>'fecha',
																'valido_nulo'=>'no'),
												'ACCION' => array(	'largo'=>45,
																	'tipo'=>'text',
																	'valido_nulo'=>'si')),
																	
							'shlink'=>array(	'ID_CARPETA' =>	array(	'largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),
												'HASH' =>	array(	'largo'=>35,
																	'tipo'=>'varchar',
																	'valido_nulo'=>'si'),
												'FECHA_CREACION'=>array('largo'=>12,
																	'tipo'=>'fecha',
																	'valido_nulo'=>'no'),
												'FECHA_EXPIRACION'=>array('largo'=>12,
																	'tipo'=>'fecha',
																	'valido_nulo'=>'si'),
												'CLAVE' => array(	'largo'=>1,
																	'tipo'=>'integer',
																	'valido_nulo'=>'si',
																	'zero'=>'si'),
												'PERMISOS' => array('largo'=>1,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no')),
							
							'otpfile'=>array(	'CODIGO' =>	array(	'largo'=>9,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),
												'HASH' =>	array(	'largo'=>35,
																	'tipo'=>'varchar',
																	'valido_nulo'=>'si'),
												'ID_ARCHIVO'=>array('largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),
												'TIEMPO' => array(	'largo'=>10,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no')),
							
							'nuevo' =>	array(	'COD' =>array(	'largo'=>6,
																'tipo'=>'integer',
																'valido_nulo'=>'no'), 																	
												'NOMBRE'=>array('largo'=>257,
																'tipo'=>'text',
																'valido_nulo'=>'no'),
												'PARENT' => array(	'largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no',
																	'zero'=>'si'),
												'ID_CARPETA' => array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no'),
												'ID_USUARIO' => array(	'largo'=>6,
																		'tipo'=>'integer',
																		'valido_nulo'=>'no'),
												'ID_GRUPO' => array('largo'=>6,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),
												'PERMISOS' =>array(	'largo'=>2,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),
												'LIB' =>array(	'largo'=>6,
																'tipo'=>'integer',
																'valido_nulo'=>'si')),							
																
							'getvars'=>array('COD' =>array(	'largo'=>6,
															'tipo'=>'integer',
															'valido_nulo'=>'no'),
											'PARENT' =>	array(	'largo'=>6,
																'tipo'=>'integer',
																'valido_nulo'=>'si',
																'zero'=>'si'),
											'SHLINK' =>	array(	'largo'=>35,
																	'tipo'=>'varchar',
																	'valido_nulo'=>'no')),
																	
							'getdirs'=>array('PARENT' =>array(	'largo'=>6,
																'tipo'=>'integer',
																'valido_nulo'=>'si'),
											'USUARIO' =>array(	'largo'=>6,
																'tipo'=>'integer',
																'valido_nulo'=>'no'),
											'LIB' =>array(	'largo'=>6,
															'tipo'=>'integer',
															'valido_nulo'=>'si')),
							
							'archivo'=>array(	'COD' =>array(	'largo'=>6,
															'tipo'=>'integer',
															'valido_nulo'=>'no'), 
												'PARENT'=>array('largo'=>6,
																'tipo'=>'integer',
																'valido_nulo'=>'si'),			
															
												'NOMBRE'=>array('largo'=>257,
																'tipo'=>'text',
																'valido_nulo'=>'no'),
												'EXT'=>array('largo'=>45,
																'tipo'=>'text',
																'valido_nulo'=>'no'),
												'UBICACION'=>array('largo'=>250,
																'tipo'=>'text',
																'valido_nulo'=>'no'),
																
												'TAMANIO' =>array(	'largo'=>15,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no'),
												
												'FECHA_SUBIDA' =>array(	'largo'=>12,
																		'tipo'=>'fecha',
																		'valido_nulo'=>'no'),
												
												'ESTADO' =>	array(	'largo'=>1,
																	'tipo'=>'integer',
																	'valido_nulo'=>'no')),
													
							/*										
							'buscador' =>	array(	'NOMBRE' =>	array(	'largo'=>24,
																		'tipo'=>'alphanum',
																		'valido_nulo'=>'si')
													
																		
																		)*/
							);
	
	$exts = array(

	// Imagens
	'png' => 'Imagen',
	'gif' => 'Imagen',
	'jpeg' => 'Imagen',
	'jpg' => 'Imagen',
	'bmp' => 'Imagen',
	'tiff' => 'Imagen',
	'psd' => 'Imagen',
	'psp' => 'Imagen',
	'svg' => 'Imagen',
	'xcf' => 'Imagen',
	
	// Archivo de sonido & music
	'mp3' => 'Archivo de sonido',
	'ogg' => 'Archivo de sonido',
	'oga' => 'Archivo de sonido',
	'flac' => 'Archivo de sonido',
	'wav' => 'Archivo de sonido',
	'aif' => 'Archivo de sonido',
	'aiff' => 'Archivo de sonido',
	'aifc' => 'Archivo de sonido',
	'cda' => 'Archivo de sonido',
	'm3u' => 'Archivo de sonido',
	'mid' => 'Archivo de sonido',
	'mod' => 'Archivo de sonido',
	'mp2' => 'Archivo de sonido',
	'snd' => 'Archivo de sonido',
	'voc' => 'Archivo de sonido',
	'wma' => 'Archivo de sonido',
	'm4a' => 'Archivo de sonido',
	
	// Videos
	'avi' => 'Video',
	'wmv' => 'Video',
	'qt' => 'Video',
	'mkv' => 'Video',
	'ogv' => 'Video',
	'flv' => 'Video',
	'mpg' => 'Video',
	'ram' => 'Video',
	'm4v' => 'Video',
	
	// Web
	'htm' => 'Web',
	'html' => 'Web',
	'shtml' => 'Web',
	'dhtml' => 'Web',
	'mshtml' => 'Web',
	
	// Scripts
	'php' => 'Script',
	'rb' => 'Script',
	'py' => 'Script',
	'c' => 'Script',
	'h' => 'Script',
	'aspx' => 'Script',
	'asp' => 'Script',
	'jsp' => 'Script',
	'cgi' => 'Script',
	'cpp' => 'Script',
	'css' => 'Script',
	'pl' => 'Script',
	'pm' => 'Script',
	'sql' => 'Script',
	'js' => 'Script',
	'sh' => 'Script',
	'txt' => 'Script',
	'conf' => 'Script',

	// Binaries
	'exe' => 'Aplicacion',
	'jav' => 'Aplicacion',
	'java' => 'Aplicacion',
	
	// Archivo comprimidos
	'gz' => 'Archivo comprimido',
	'tar' => 'Archivo comprimido',
	'ar' => 'Archivo comprimido',
	'cbz' => 'Archivo comprimido',
	'jar' => 'Archivo comprimido',
	'tar.7z' => 'Archivo comprimido',
	'tar.bz2' => 'Archivo comprimido',
	'tar.gz' => 'Archivo comprimido',
	'tar.lzma' => 'Archivo comprimido',
	'tar.xz' => 'Archivo comprimido',
	'zip' => 'Archivo comprimido',
	'rar' => 'Archivo comprimido',
	'bz' => 'Archivo comprimido',
	'deb' => 'Archivo comprimido',
	'rpm' => 'Archivo comprimido',
	'7z' => 'Archivo comprimido',
	'ace' => 'Archivo comprimido',
	'cab' => 'Archivo comprimido',
	'arj' => 'Archivo comprimido',
	'msi' => 'Archivo comprimido',
	
	// Documentos
	'pdf' => 'Documento',
	'odt' => 'Documento',
	'ott' => 'Documento',
	'sxw' => 'Documento',
	'stw' => 'Documento',
	'ots' => 'Documento',
	'sxc' => 'Documento',
	'stc' => 'Documento',
	'sxi' => 'Documento',
	'sti' => 'Documento',
	'pot' => 'Documento',
	'odp' => 'Documento',
	'ods' => 'Documento',
	'doc' => 'Documento',
	'docx' => 'Documento',
	'xls' => 'Documento',
	'xlt' => 'Documento',
	'ppt' => 'Documento',
	'pps' => 'Documento',
	'odg' => 'Documento',
	'otp' => 'Documento',
	'sxd' => 'Documento',
	'std' => 'Documento',
	'pptx' => 'Documento',
	'docx' => 'Documento',
	'xlsx' => 'Documento',

	// Other
	'rtf' => 'Archivo de texto',
	'ttf' => 'Fuente',
	);/**/


?>
