<?php
class Model extends ABModel{
		/*****************************************************************************************************************
														INSERCION DE  REGISTROS
		*****************************************************************************************************************/		
		public function guardar_archivo($bindvars) {			
			return $this->execProcedure("INS_LIBRARY.SAVE_FILE(:COD,:PARENT,:NOMBRE,:EXT,:TAMANIO,:UBICACION,:FECHA_SUBIDA,:ESTADO)",$bindvars, false);
		}
		public function set_dir_user($bindvars) {
			return $this->execProcedure("INS_LIBRARY.SET_DIR_USER(:ID_CARPETA, :ID_USUARIO)",$bindvars, false);
		}
		public function guardar_otp($bindvars) {
			return $this->execProcedure("INS_LIBRARY.SAVE_OTP(:CODIGO, :HASH, :ID_ARCHIVO, :TIEMPO)",$bindvars, false);
		}
		public function make_dir($bindvars) {
			return $this->execProcedure("INS_LIBRARY.MAKE_DIR(:NOMBRE, :PARENT, :LIB)",$bindvars, false);
		}
		public function set_dir_perms($bindvars) {
			return $this->execProcedure("INS_LIBRARY.SET_DIR_PERMS(:COD, :ID_CARPETA)",$bindvars, false);
		}
		public function set_perms($bindvars) {
			return $this->execProcedure("INS_LIBRARY.SET_PERMS(:ID_GRUPO, :ID_CARPETA)",$bindvars, false);
		}
		public function set_link($bindvars) {
			return $this->execProcedure("INS_LIBRARY.SET_LINK(:ID_CARPETA,:FECHA_CREACION,:FECHA_EXPIRACION,:CLAVE,:PERMISOS,:HASH)",$bindvars, false);
		}
		public function set_history($bindvars) {
			return $this->execProcedure("INS_LIBRARY.SET_HISTORY(:ID_USUARIO,:ID_ARCHIVO,:TIPO_CONSULTA,:FECHA,:ACCION)",$bindvars, false);
		}
		/*****************************************************************************************************************
														SELECCION DE  REGISTROS
		*****************************************************************************************************************/
		public function shParentsDir($bindvars) {
			$this->execProcedure("SEL_LIBRARY.SHPARENTSDIR(:SHLINK, :PARENT, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function shgetFileInfo($bindvars) {			
			$this->execProcedure("SEL_LIBRARY.SHGETFILEINFO(:SHLINK, :COD, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function checkParentDir($bindvars) {						
			$this->execProcedure("SEL_LIBRARY.CHECKPARENTDIR(:PARENT, :COD, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_linkdata($bindvars) {					
			$this->execProcedure("SEL_LIBRARY.GETLINKDATA(:HASH, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function count_otpinfo($bindvars) {					
			$this->execProcedure("SEL_LIBRARY.COUNTOTPINFOR(:CODIGO, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}	
		public function verifica_existencia($bindvars) {				
			$this->execProcedure("SEL_LIBRARY.COUNTOTPINFOR(:ID_GRUPO, :ID_CARPETA, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_folders_link($bindvars) {			
			$this->execProcedure("SEL_LIBRARY.COUNTOTPINFOR(:PARENT, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_folders($bindvars) {			
			$this->execProcedure("SEL_LIBRARY.GETFOLDERS(:USUARIO, :PARENT, :LIB, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_files($bindvars) {			
			$this->execProcedure("SEL_LIBRARY.GETFILES(:PARENT, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function getFileSearch($bindvars) {			
			$this->execProcedure("SEL_LIBRARY.GETFILESEARCH(:NOMBRE, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_childrens_files($bindvars) {			
			$this->execProcedure("SEL_LIBRARY.GETFILESEARCH(:HASH, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_childrens($bindvars) {			
			$this->execProcedure("SEL_LIBRARY.GETCHILDRENS(:HASH, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_parents_dirs($bindvars) {	
			$this->execProcedure("SEL_LIBRARY.GETPARENTSDIR(:PARENT, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_all_folders() {
			$this->execProcedure("SEL_LIBRARY.GETALLFOLDERS( :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_groups() {	
			$this->execProcedure("SEL_LIBRARY.SGROUPS(:CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_dirinfo($bindvars) {	
			$this->execProcedure("SEL_LIBRARY.GETDIRINFO(:COD, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function get_dirgroups($bindvars) {	
			$this->execProcedure("SEL_LIBRARY.GETDIRGROUPS(:COD, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}	
		public function count_files($bindvars) {	
			$this->execProcedure("SEL_LIBRARY.COUNT_FILES(:COD, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		public function count_dirs($bindvars) {	
			$this->execProcedure("SEL_LIBRARY.COUNT_DIRS(:COD, :CURSOR)",$bindvars, true);
			return $this->getCursor();
		}
		
		/*****************************************************************************************************************
														ACTUALIZACION DE  REGISTROS
		*****************************************************************************************************************/
		public function updParentFile($bindvars) {
			return $this->execProcedure("UPD_LIBRARY.UPARENTFILE(:PARENT,:COD)",$bindvars, false);
		}
		public function actualizar_archivo($bindvars) {
			return $this->execProcedure("UPD_LIBRARY.UFILE(:ESTADO,:COD)",$bindvars, false);
		}
		public function reciclaArchivo($bindvars) {
			return $this->execProcedure("UPD_LIBRARY.URECYCLEFILE(:ESTADO,:COD)",$bindvars, false);
		}
		public function actualizar_carpeta($bindvars) {
			return $this->execProcedure("UPD_LIBRARY.UDIRSTATUS(:ESTADO,:COD)",$bindvars, false);
		}
		public function change_name_dir($bindvars) {
			return $this->execProcedure("UPD_LIBRARY.UDIRNAME(:NOMBRE,:ID_CARPETA)",$bindvars, false);
		}
		/*****************************************************************************************************************
														ELIMINACION DE  REGISTROS
		*****************************************************************************************************************/
		public function rm_perms_childrens($bindvars) { 
			return $this->execProcedure("DEL_LIBRARY.PERMSCHILDRENS(:ID_GRUPO,:ID_CARPETA)",$bindvars, false);
		}
		public function rm_perms_parent_childrens($bindvars) {  
			return $this->execProcedure("DEL_LIBRARY.PERMSPARENTCHILDRENS(:ID_GRUPO,:ID_CARPETA)",$bindvars, false);
		}
		public function rm_dir_perms($bindvars) { 
			return $this->execProcedure("DEL_LIBRARY.DIRPERMS(:COD)",$bindvars, false);
		}
		public function rm_carpeta($bindvars) { 
			return $this->execProcedure("DEL_LIBRARY.RMCARPETA(:COD)",$bindvars, false);
		}
	}
?>