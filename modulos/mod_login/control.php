<?php
	include_once(SERVER_ROOT . MODULOS . '/mod_login/constantes.php');
	include_once(SERVER_ROOT . MODULOS . '/mod_login/modelo.php');
	include_once(SERVER_ROOT . MODULOS . '/mod_login/vista.php');
	
class Controller extends Controllermass
{
	public function main(array $getVars,$subpage){
		ob_start();
		$Model = new Model;
		$Vista = new Vista;
		if($subpage=="salir"){
			$this->cerrar();
			$this->redireccionar('login');	
		}
		if( Session::get('autenticado') && $getVars['ajax']==1 ){
			$Vista->print_json(array('status'=>1,'msg'=>'Acceso Concedido.'));
		}elseif( Session::get('autenticado') ){
            $this->redireccionar('sitio_'.PAGINA_PRINCIPAL);
			exit;
        }
			
		$mensaje='';
		$status = 0;       
		
		$getVars['enviar'] = !isset($getVars['enviar']) ? 0 : $getVars['enviar'];
		$getVars['ajax'] = !isset($getVars['ajax']) ? 0 : $getVars['ajax'];
		
		/*
		if( isset($getVars['enviar']) )
			$getVars['enviar'] = $getVars['enviar'];
		else
			$getVars['enviar'] = 1;*/
		
		if($getVars['enviar']==1){
			
			if($getVars['login_usr']){		
				$user=array('login_usr'=>strtolower(trim($getVars['login_usr'])));
				$includeVars = $this->include_vars($user,'NULL','login');
				
				if( $includeVars[0] != "ERROR" ){
					$data_array = $Model->obtain_results_by_id($includeVars);
										
					if( $data_array[0]['USUARIO'] == strtolower(trim($getVars['login_usr'])) ){
						
						if( $data_array[0]['PASS'] == md5($getVars['pass']) ){	
						
							$cod=array('cod'=>$data_array[0]['ID']);
							$includeVars = $this->include_vars($cod,null,'login');
							if( $includeVars[0] != "ERROR" ){
								$modulos = $Model->listar_modulos($includeVars);
								$modulos[] = array('NOMBRE'=>'entrada',	'CARPETA'=>'entrada'	,'PERMISOS'=>'7');
								$modulos[] = array('NOMBRE'=>'sitio',	'CARPETA'=>'sitio'		,'PERMISOS'=>'7');
								$modulos[] = array('NOMBRE'=>'login',	'CARPETA'=>'login'		,'PERMISOS'=>'7');
								$modulos[] = array('NOMBRE'=>'sinc',	'CARPETA'=>'sinc'		,'PERMISOS'=>'7');
								//print_r($modulos);exit;
							}
							if(is_array($modulos)){
								Session::set('autenticado', true);
								//Session::set('level',		$data_array[0]['NIVEL']);
								Session::set('usuario', 	$data_array[0]['USUARIO']);
								Session::set('id_usuario', 	$data_array[0]['ID']);
								//Session::set('permisos',	$data_array[0]['PERMISOS']);
								//Session::set('servicio',	$data_array[0]['PROYECTO']);
								//Session::set('ruta_acceso', $data_array[0]['RUTA_ACCESO']);
								Session::set('tiempo', 		time());	
								Session::set('sitio',		$data_array[0]['SITIO']);
								Session::set('modulo_acceso',$modulos);
								if( $getVars['ajax']==0 ){
									/*
									if( $getVars['query'] )
										$this->redireccionar(base64_decode($getVars['query']));
									else
										*/
									$this->redireccionar('sitio_'.PAGINA_PRINCIPAL);
								}else{
									$mensaje = 'Acceso Concedido.';
									$status = 1;
								}
							}
						}else							
							$mensaje='Error: Password no Concuerda';					
					}else										
						$mensaje='Error: Usuario no Encontrado';							
				}else						
					$mensaje='Debe Ingresar un Usuario Valido';											
			}else				
				$mensaje='Debe Ingresar un Usuario';					
		}else			
			$mensaje='';
		if( $getVars['ajax']==1 ){			
			$salida = ob_get_contents();
			ob_end_clean();
			if($salida)
				$mensaje = 'Ha ocurrido un Error, Intente mas tarde.';			
			$Vista->print_json(array('status'=>$status,'msg'=>$mensaje));
		}
		
		$Vista -> Ver_Modelo(MODULO,$mensaje);		
	}
	
	private function cerrar()
    {
        Session::destroy();
        $this->redireccionar('login');
    }	
}

?>
