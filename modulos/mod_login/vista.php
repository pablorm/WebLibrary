<?php
class Vista extends MView {
	
	function Ver_Modelo($vista,$mensaje){		
		global $diccionario;			
		$html = $this->get_template($vista);
		$html = str_replace('{mensaje}',$mensaje,$html);
		if(defined('QUERY_STRING') )
			$html = str_replace('{QUERY}',base64_encode(QUERY_STRING),$html);
		else
			$html = str_replace('{QUERY}','',$html);
		$html = $this->render_dinamic_data($html, $diccionario['formulario']);
		print $html;	
	}
}
	
	