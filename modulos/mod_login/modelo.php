<?php
class Model extends ABModel{

		public function obtain_results_by_id($bindvars) {
			$sql = "SELECT ID, USUARIO, PASS, SITIO FROM U_LISTADO WHERE USUARIO = :LOGIN_USR";
			$this->connect();
			$this->prepare($sql);
			$this->sanitize($bindvars);
			$this->exec_w_commit();
			$res = $this->execFetchAll();
			return $res;
		}
	  
		public function listar_modulos($bindvars) {
			$sql = "SELECT NOMBRE, CARPETA, ID_PERMISO AS PERMISO
                    FROM U_PERMISOS
                    LEFT JOIN U_MODULOS ON U_MODULOS.P_KEY = U_PERMISOS.ID_MODULO
                    WHERE U_MODULOS.ESTADO='1' AND ID_USUARIO = :COD AND SITIO = '1' AND CARPETA != '#'
                    ORDER BY CARPETA ASC";
			return $this->query_fetch($sql,$bindvars);
		}
	  
	  
}
		



