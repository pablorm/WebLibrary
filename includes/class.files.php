<?php

class File{
	public $format;
	public $dest_folder;
	public $orig_folder;
	public $new_filename;
	public $extencion;
	
	public function valida_ext(){
		require(SERVER_ROOT .'/includes/formatos.php');
		if( $this->format ){
			$format = explode(",",$this->format);
			if( is_array($format) ){
				foreach( $format as $key=>$val ){
					if( array_key_exists($val, $formatos_registrados) ){
						if( in_array( $this->extencion, $formatos_registrados[$val] ) )
							return true;
					}else{
						if( $val == $this->extencion )
							return true;
					}
				}
			}
			return false;
		}else
			return true;
		
	}
	
	public function upload($input){	
		$filename 			= 	$_FILES[$input]['name'];
		$orig_filename 		= 	$_FILES[$input]['tmp_name'];
		$tamanio 			= 	$_FILES[$input]['size'];
		$this-> extencion	=	$this->get_extension($filename);
		$dest_filename 		= 	SERVER_ROOT.$this->dest_folder.'/'.$this->new_filename;
		//echo 'test';
		//echo $this->valida_ext();
		if( $this->valida_ext() ){
			if(move_uploaded_file($orig_filename, $dest_filename))
				return array('SUCCESS'=>'1','LAST_NAME'=>$filename,'NEW_NAME'=>$this->new_filename,'EXT'=>$this->extencion,'SIZE'=>$tamanio);
			else
				return array('SUCCESS'=>'0','ERROR'=>'No se pudo copiar.');
		}else
			return array('SUCCESS'=>'0','ERROR'=>'Archivo no Compatible.');
	}
	
	protected function download($name,$path,$type){
		
		if (is_file($path)) {
			$size = filesize($path);
			if (function_exists('mime_content_type')) {
				$type = mime_content_type($path);
			}elseif(function_exists('finfo_file')) {
				$info = finfo_open(FILEINFO_MIME);
				$type = finfo_file($info, $path);
				finfo_close($info);
			}
			if ($type == '') {
				$type = "application/force-download";
			}
			// Definir headers
			header("Content-Type: $type");
			header("Content-Disposition: attachment; filename=$name");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: " . $size);
			// Descargar archivo
			readfile($path);
			exit;
		}else{
			return false;
		}			
	}
	
	public function unZip($zipFile, $toFolder){
		
		$zip = new ZipArchive;
		if ($zip->open($zipFile) === TRUE) {
			$zip->extractTo($toFolder);
			$zip->close();
			return true;
		} else {
			return false;
		}	
	}
	
	public function get_extension($filename){
		$exp_filename	= 	explode(".",$filename);
		return strtolower(array_pop($exp_filename));	
	}


}








?>