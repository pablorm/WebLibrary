<?php
	$formatos_registrados = array(
		'imagenes' 		=>	array('png','gif','jpeg','jpg','bmp','tiff','psd','psp','svg','xcf','tif'),
		'documentos' 	=>	array('pdf','odt','ott','sxw','stw','ots','sxc','stc','sxi','sti','pot','odp','ods',
								'doc','docx','xls','xlt','ppt','pps','odg','otp','sxd','std','pptx','docx','xlsx','mpp','vsd','ppsx','xlsm'),
		'video' 		=>	array('avi','wmv','qt','mkv','ogv','flv','mpg','ram','m4v'),
		'audio' 		=>	array('mp3','ogg','oga','flac','wav','aif','aiff','aifc','cda','m3u','mid','mod','mp2','snd','voc','wma','m4a'),
		'comprimido' 	=>	array('gz','tar','ar','cbz','jar','tar.7z','tar.bz2','tar.gz','tar.lzma','tar.xz','zip','rar','bz','deb','rpm','7z','ace','cab','arj','msi','apk'),
		'otro' 			=>	array('rtf','ttf'),
		'binarios'		=>  array('exe','jav','java','msi','apk')
		);


?>