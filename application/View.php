<?php
class MView {

	function page_render( $html, $template, $_data, $marker='LISTADO', $format='E' ){
		if( $template )
			$html .= $this->get_template($template);
		
		if( count($_data)>1 )
			$html = $this->renderGrid($html, $_data, $marker, $format);
		else
			$html = $this->renderDetail($html, $_data, $format);
		return $html;
	}
	
	public function imprime_fotos_pdf($datos, $_html='',$header){
		$_img	=	array();
		$count 	= 	count($datos);
		$a=1;
		while( $a <= $count ){
			@$_img[] = array(
				'IMAGEN_1'			=>	$datos[$a-1]['COD'],
				'FECHA_1'			=>	$datos[$a-1]['FECHA'],
				'OBSERVACIONES_1'	=>	$datos[$a-1]['OBSERVACIONES'],
				'IMAGEN_2'			=>	$datos[$a]['COD'],
				'FECHA_2'			=>	$datos[$a]['FECHA'],
				'OBSERVACIONES_2'	=>	$datos[$a]['OBSERVACIONES']
			);
			@$this->compress_image('/folder/'.$datos[$a-1]['COD'], '/folder_temp/'.$datos[$a-1]['COD'], 40);
			@$this->compress_image('/folder/'.$datos[$a]['COD'], '/folder_temp/'.$datos[$a]['COD'], 40);
			++$a;
			++$a;
		}
		$b=1;
		$d=0;
		$contador = count($_img);
		while( $b <= $contador ){		
			$img = $header.'<div style="width:100%; height:100%; border:1px solid;">';
			$c=0;
			while( ($d < $contador) && ($c<=2) ){	
				$img .= '<div style="height:250; width:100%; border:0px solid;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="center" class="texto1">'.$_img[$d]['FECHA_1'].'</td>
								<td align="center" class="texto1">'.$_img[$d]['FECHA_2'].'</td></tr>
							<tr><td align="center"><img src="folder_temp/'.$_img[$d]['IMAGEN_1'].'" style="height:200px;" /></td>
								<td align="center">'.( $_img[$d]['IMAGEN_2'] ?'<img src="folder_temp/'.$_img[$d]['IMAGEN_2'].'" style="height:200px;"  />' : '').'</td></tr>
							<tr><td align="center" class="texto1">'.$_img[$d]['OBSERVACIONES_1'].'</td>
								<td align="center" class="texto1">'.$_img[$d]['OBSERVACIONES_2'].'</td></tr>
						</table></div>';
				$c++;
				$d++;
			}
			$img .= '</div>';
			$_html[] = $img;
			$b = $b + 3;
		}
		return $_html;
	}
	
	public function compress_image($src, $dest , $quality){
		$image = SERVER_ROOT.$src;
		if( is_file($image) ){
			$info = getimagesize($image);	
			if ($info['mime'] == 'image/jpeg'){
				$image = imagecreatefromjpeg($image);
			}elseif ($info['mime'] == 'image/gif'){
				$image = imagecreatefromgif($image);
			}elseif ($info['mime'] == 'image/png'){
				$image = imagecreatefrompng($image);
			}else{
				return false;
			}	  
			//compress and save file to jpg
			imagejpeg($image, SERVER_ROOT.$dest, $quality);	  
			//return destination file
			return true;
		}else
			return false;
	}
	public function creaXLS($nombre_archivo,$_label,$_data,$_titulos,$fila_inicio,$col_inicio){
		include_once(SERVER_ROOT .'/includes/class.excel.php');
		$columna	= 	$col_inicio;
		$b 			= 	$fila_inicio;
		$Excel 		= 	new CreateExcel( $nombre_archivo );
		/*************************************** TITULOS ******************************************/
		if( is_array($_titulos) ){
			foreach( $_titulos as $key4=>$val4){
				$Excel -> xlsWriteLabel( $val4['COL'], $val4['FILA'], $val4['DATO'] );
				if( $val4['FILA'] >= $b )
					$b = $val4['FILA'];
			}
		}
		$b += 2;
		/************************************* ENCABEZADOS ****************************************/
		if( is_array($_label) ){
			foreach( $_label as $key=>$val ){
				$Excel -> xlsWriteLabel($columna,$b,$val);
				++$columna;
			}
			$columna = $col_inicio;
			$b++;
		}
		/*************************************** DATOS *******************************************/
		foreach( $_data as $key2=>$val2 ){
			foreach( $val2 as $key3=>$val3 ){
				if( Controller::is_valid_digit($val3) )
					$Excel -> xlsWriteNumber($columna,$b,$val3);
				else
					$Excel -> xlsWriteLabel($columna,$b,$val3);
				++$columna;
			}
			$columna = $col_inicio;
			$b++;
		}				
		exit;
	}
	
	function print_json($_data,$base64=false,$opc=''){
		if( $base64 ){
			foreach($_data as $key=>$val){
				foreach($val as $k=>$v ){
					$_data[$key][$k] = base64_encode($this->regula_texto($v,$opc));
				}
			}
		}
		echo json_encode($_data);
		exit;
		return true;
	}
	
	function abstract_row($html,$key){
		$regex = "/<!--".$key."-->[\s\S]*?<!--".$key."-->/";
		$match = preg_match($regex, $html,$matches);
		return str_replace("<!--".$key."-->","",$matches[0]);
	}
	
	function regula_texto($texto,$opc){
		$opciones=str_split($opc);
		
		$opciones[0] = !isset($opciones[0]) ? null : $opciones[0];
		$opciones[1] = !isset($opciones[1]) ? null : $opciones[1];
		$opciones[2] = !isset($opciones[2]) ? null : $opciones[2];
		$opciones[3] = !isset($opciones[3]) ? null : $opciones[3];		
		
		switch($opciones[0]){			
			case 'E':
				$texto = utf8_encode($texto);
				break;
			case 'D':
				$texto = utf8_decode($texto);
		}
		switch($opciones[1]){
			case 'U':
				$texto = mb_strtoupper($texto,'UTF-8');
				break;
			case 'L':
				$texto = mb_strtolower($texto,'UTF-8');
		}		
		switch($opciones[2]){
			case 'W':
				$texto = ucwords($texto);
				break;
			case 'F':
				$texto = ucfirst($texto);
		}
		
		if($opciones[3]==1){
			$texto = trim($texto);	
		}
		return $texto;			
	}
	
	function alert($aviso,$regreso,$cod){
		$file = SERVER_ROOT . '/site_media/html/miscelaneo/alert.html';
		$template = file_get_contents($file);
		$template = str_replace('{aviso}',$aviso,$template);
		$template = str_replace('{regreso}',$regreso,$template);
		$template = str_replace('{cod}',$cod,$template);
		echo $template;
	}
	function window_close(){		
		echo "<script>window.close();</script>";
	}
	function renderCombo($html, $_data, $key, $selected=null, $reg='0000', $val1='P_KEY', $val2='NOMBRE') {		
		$regex 	= 	'/<!--'.$key.'-->[\s\S]*?<!--'.$key.'-->/';
		$match 	= 	preg_match($regex, $html,$matches);		
		$a	=0;
		$tabla = null;
		if( isset($matches[0]) ){
			$test	=	str_replace("<!--".$key."-->","",$matches[0]);
			while($a<count($_data)){
				$fila	= $test;
				$_sel 	= ( $selected == $_data[$a][$val1] )? 'selected' : '';
				$at 	= array(0=>array('SELECTED'=>$_sel,'CODIGO'=>$_data[$a][$val1],'DENOMINACION'=>$_data[$a][$val2]));
				$fila 	= $this->renderDetail($fila, $at, $reg);		
				$tabla .= $fila;
				$a++;
			}
			$html = str_replace($matches[0],$tabla,$html);
		}
		return $html;
	}
	function render_combo($html, $array_data, $key, $val1, $val2) {		
		$regex = '/<!--'.$key.'-->[\s\S]*?<!--'.$key.'-->/';
		$match = preg_match($regex, $html,$matches);		
		$a=0;
		$tabla = null;
		if( isset($matches[0]) ){
			$test=str_replace("<!--".$key."-->","",$matches[0]);
			while($a<count($array_data)){
				$fila=$test;
				$fila;
				$fila = str_replace('{codigo}', $array_data[$a][$val1], $fila);
				$fila = str_replace('{denominacion}', $array_data[$a][$val2], $fila);			
				$tabla .= $fila;
				$a++;
			}
			$html = str_replace($matches[0],$tabla,$html);
		}
		return $html;
	}
	
	function renderGrid($html, $array_data,$key, $reg='0000') {
		if( is_array($array_data)){
			$regex = '/<!--'.$key.'-->[\s\S]*?<!--'.$key.'-->/';
			$match = preg_match($regex, $html,$matches);
			
			$a=0;
			$tabla = null;
			if( isset($matches[0]) ){
				$test = str_replace("<!--".$key."-->","",$matches[0]);
				$b = count($array_data);
				while( $a<$b ){
					if ($a % 2) { $fondolinea="itemParTabla"; } else { $fondolinea="itemImparTabla"; }
					$fila=$test;
					$fila;
					$fila = str_replace('{fondolinea}', $fondolinea, $fila);
					foreach($array_data[$a] as $key=>$val){
						$fila = str_replace('{'.$key.'}', $this->regula_texto($val,$reg), $fila);		
					}
					
					$tabla .= $fila;
					$a++;
				}
				$html = str_replace($matches[0],$tabla,$html);
			}
			return $html;
		}else
			return false;
		
	}
	
	function render_listado($html, $array_data, $key, $val1) {		
		$regex = '/<!--'.$key.'-->[\s\S]*?<!--'.$key.'-->/';
		$match = preg_match($regex, $html,$matches);		
		$a=0;
		$tabla = null;
		$test=str_replace("<!--".$key."-->","",$matches[0]);
		while($a<count($array_data)){
			$fila=$test;
			$fila;
			//$fila = str_replace('{codigo}', $array_data[$a][$val1], $fila);
			$fila = str_replace('{denominacion}', $array_data[$a][$val1], $fila);			
			$tabla .= $fila;
			$a++;
		}
		$html = str_replace($matches[0],$tabla,$html);
		return $html;
	}
	
	function combo_dependiente($data,$val1,$val2){
		echo '<option value="0" selected="selected">Selecciona Uno...</option>';
		foreach ($data as $clave=>$valor) {
			echo "<option value=".$valor[$val1].">".$valor[$val2]."</option>";
		}
	}
	
	function renderDetail($html, $array_data, $reg='0000') {
		if(is_array($array_data[0])){
			foreach($array_data[0] as $key=>$val){
				$html = str_replace('{'.$key.'}', $this->regula_texto($val,$reg), $html);		
			}	
		}
		return $html;
	}
	
	function render_dinamic_data($html, $data) {
		if($data){
			foreach ($data as $clave=>$valor) {
				$html = str_replace('{'.$clave.'}', $valor, $html);
			}
		}
		return $html;
	}

	function get_template($vista) {
		$vista=strtolower($vista);
		
		// verifico si se llama a la pagina principal  o a una pagina complementearia
		if($vista==MODULO){ 
			$template='index'; 
		}else{
			$template=$vista;
		}
		
		// armo la ruta del template
		$file = SERVER_ROOT . '/site_media/html/'.MODULO.'/'.$template.'.html';
		
		// verifico que exista el archivo ( no necesario)
		if(!file_exists($file)){
			$file=SERVER_ROOT . '/site_media/html/error_404.html';
		}	
		
		// termino con la asignacion y regreso
		$template = file_get_contents($file);	
		return $template;
	}
	
	function mensaje_error($html,$data_array){
		$campos;
		if( isset($data_array[0]) ){
			if($data_array[0]=='ERROR'){
				array_shift($data_array);
				foreach($data_array as $key){
					$campos.=ucfirst(strtolower($key)).", ";
				}
				$html = str_replace('{mensaje}', 'Error encontrado en: '.$campos, $html);
			}
			$html = str_replace('{mensaje}', '', $html);
		}
		return $html;	
	}
}