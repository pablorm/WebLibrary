<?php
	/**
	  * MODO 0 = DESARROLLO ;; MODO 1 = PRODUCCION ;; MODO 2 = PRUEBAS (SE CONECTA A LA BD DE PRODUCCION PERO EN FORMA LOCAL)
	  * http://mdr-codelco.bureauveritas.cl/  --- http://localhost/bvmr_poo
	  */
	
	if(APLICATION_MODE==1){
		define('SITE_ROOT' , 'http://'.$_SERVER['SERVER_NAME'].'/portal/');
		define('SESSION_LIFE',60*40);
		define('CONTENEDOR_INFORMES','407');
		define('CONTENEDOR_INFORMES_GESTION','481');
	}elseif(APLICATION_MODE==0 || APLICATION_MODE==2){
		define('SITE_ROOT' , 'http://localhost/library/');
		define('SESSION_LIFE',60*60*24*7);
		define('CONTENEDOR_INFORMES','184');
		define('CONTENEDOR_INFORMES_GESTION','208');
	}
	define('NAME_APP','STING');
	//define('CONF_INC_FILE',"/includes/config.inc.php");
	define('PAGINA_PRINCIPAL','prim2');
	define('MODULO_PRINCIPAL','sitio_principal');
	define('MODULOS','/modulos');
	define('NOMBRE_EMPRESA','Data General');
	/*********************************************************************************************************
												PARENTS BIBLIOTECA
	*********************************************************************************************************/
	define('AC_FOTOS_PARENT','1');
	/*********************************************************************************************************
												RELATIVO A ARCHIVOS Y CARPETAS
	*********************************************************************************************************/
	define('MASTER_FOLDER','/folder');
	define('TEMP_FOLDER','/folder_temp');
	define('LIB_FOLDER','/biblioteca');	
	define('CURRENT_DIR','/'.date('Y/m/d'));
	if(!is_dir(SERVER_ROOT.MASTER_FOLDER.LIB_FOLDER.CURRENT_DIR.'/')){
		mkdir(SERVER_ROOT.MASTER_FOLDER.LIB_FOLDER.CURRENT_DIR.'/',0777,true);
	}
	
	/*********************************************************************************************************
												CONEXION ORACLE
	*********************************************************************************************************/
	if( APLICATION_MODE==1  || APLICATION_MODE==2 ){
		define('USUARIO','');
		define('CLAVE','');
		define('HOST','');
		define('SID','XE');
		define('PUERTO','1521');
		
	} elseif( APLICATION_MODE==0 ){
		define('USUARIO','LIB');
		define('CLAVE','LIB');
		define('HOST',get_cfg_var("mdr.cfg.DB_HOST"));
		define('SID','XE');
		define('PUERTO','1521');		
	}
	/*********************************************************************************************************
												CONTROL DE SITIOS
	*********************************************************************************************************
	$sitios = array(
		'monitoringrt' => '1',
		'integralchuqui' => '2',
		'gestion' => '3'
	);	
	/*********************************************************************************************************
												CONTROL DE PERMISOS
	*********************************************************************************************************/	
	define('PERMISO_LECTURA',4);	
	define('PERMISO_MODIFICAR',6);
	define('PERMISO_EJECUTAR',7);
	/*********************************************************************************************************
												SUBPAGINAS Y PERMISOS
	*********************************************************************************************************/
	$Permiso_Modificar = array( 'modificar','eliminar','nuevo','guardar' );
	$Permiso_Descargar = array( '' );

	/*********************************************************************************************************
												ARCHIVOS XML
	*********************************************************************************************************/
	if(APLICATION_MODE==1 || APLICATION_MODE==2){
		define('XML_LISTBOX',SERVER_ROOT.'/xml/listas_desplegables.xml');
	}elseif(APLICATION_MODE==0){
		define('XML_LISTBOX',SERVER_ROOT.'/xml/dev_listas_desplegables.xml');
	}
	
?>