<?php
	/***************************************************************************************************************
												OBTENGO PAGINA Y SUBPAGINA
	 ***************************************************************************************************************/
	$request = $_SERVER['QUERY_STRING'];	
	$getVars=array();
	$subpage='NULL';
		
	/**********************VARIBLES GET POST**************************/
	if($request){
		$_request = explode('#',$request);
		$request = $_request[0];
		
		// extraigo la pgina solicitada - la pagina es el primer elemento
		$parsed = explode('&' , $request);
		$temp_page = array_shift($parsed);
						
		// extraigo la subpagina - divido entre pagina y subpagina
		$temp_page = explode('_',$temp_page);
		$page = array_shift($temp_page);
		$subpage = array_pop($temp_page);
		
		// ordeno las variables recibidas
		$test=array();
		foreach($parsed as $key){			
			$pieces=explode("=",$key);
			$temp=$pieces[0];
			$test[$temp]=$pieces[1];			
		}		
		$getVars = fillpost('ALL','cadena_busqueda',$test);
		
	}else{
		$page='sitio';	
		$subpage=PAGINA_PRINCIPAL;
	}	
	//echo $page;
	// SE COMPRUEBA EL TIEMPO DE LA SESION Y SE RESETEA SI ES QUE NO EXPIRO
	$no_pass_page = array(	'biblioteca'=>array('shlink','shdownload','shlistfiles','shupload','shmkdir'),
							'venc'=>array('check','repvidrio'));
							//'sicm'=>array('expdata','impdata','newextid','testexpdata','testimpdata')
	
	if( Session::get('autenticado') ){
		if( (time()-Session::get('tiempo'))>SESSION_LIFE ){
			Session::set('autenticado', false);
			if($request) define('QUERY_STRING',$request);
			if( !array_key_exists($page,$no_pass_page) ){
				$page='login';
			}else{
				if( !in_array($subpage,$no_pass_page[$page]) ){
					$page='login';
				}
			}
		}else{
			Session::set('tiempo', time());	
		}
	}else{		
		if( !array_key_exists($page,$no_pass_page) ){
			if($request) define('QUERY_STRING',$request);
			$page='login';
		}else{
			if( !in_array($subpage,$no_pass_page[$page]) ){
				if($request) define('QUERY_STRING',$request);
				$page='login';
			}
		}
	}
	
	/*******************************************************************************************
	 *  						REDIRECCION AL CONTROLADOR									   *
	 *******************************************************************************************/
	$target = SERVER_ROOT . '/modulos/mod_'.$page.'/control.php';

	if ($target != 'error') {
		if (file_exists($target)) {
			
			include_once($target);
			$class = 'Controller';
			if (class_exists($class)) {
				$controller = new $class;
			} else {
				send404();//die('Clase no Existe!');
			}
			
		} else {
			send404();//die('Pagina No existe!');
		}
		
		/***   ACCESO A TODO EN MODO DE DESARROLLO; QUITAR DESPUES   ****
		// if(APLICATION_MODE!=0 ){
		 	if( Session::get('autenticado') ){
		 		// VALIDO MODULO EN SITIO	
				if(!$controller->checkSubpage($page)){
					$controller->redireccionar(MODULO_PRINCIPAL);
				}
		 		/*
				$controller->valida_modulo_sitio($page);
				// BUSCO EL ID DEL MODULO
					
		 		$codigo = $controller->get_ID_Module($page);				
				// VERIFICO PERMISOS 
				if ($codigo) {
		 			$subpage = $controller->comprobar_nivel($codigo, $subpage);
		 		}*
		 	}
		// }
		/*********************************************************************/
		
		//Session::close();
		$controller->main($getVars, $subpage);
		
	} else {
		send404();//die('Pagina no Encontrada');		
	}
	
	function send404(){
		header("HTTP/1.0 404 Not Found");
		echo "PHP continues.\n";
		die();		
	}

	function fillPost($keys,$exclude,$parsed){
		$array = array();
		foreach ($_POST as $key=>$val){
			if (is_array($keys)){
				if (in_array($key, $keys)) $array[$key] = $val;
			}elseif($keys==="ALL"){
				if (isset($exclude)){
					if(is_array($exclude)){
						if (!in_array($key,$exclude)) $array[$key] = $val;
					}else{
						if ($key!=$exclude) $array[$key] = $val;
					}
				}else{
					$array[$key] = $val;
				}
			}else return $_POST[$keys];
		}
		foreach ($parsed as $key=>$val){
			if (is_array($keys)){
				if (in_array($key, $keys)) $array[$key] = $val;
			}elseif($keys==="ALL"){
				if (isset($exclude)){
					if(is_array($exclude)){
						if (!in_array($key,$exclude)) $array[$key] = $val;
					}else{
						if ($key!=$exclude) $array[$key] = $val;
					}
				}else{
					$array[$key] = $val;
				}
			}else return $_GET[$keys];
		}
		return $array;
	}