<?php
class ABModel extends OracleImproved_Driver{
	private $id_cursor;
		public function query_fetch($sql,$bindvars){
			$this->connect();
			$this->prepare($sql);
			if( $this->sanitize($bindvars) ){
				if( $this->exec_w_commit() ){
					return $this->execFetchAll();
				}else
					return false;
			}
			return false;
		}
		
		public function execProcedure($sql,$bindvars, $Bcursor){
			$this->connect();
			$this->prepare("BEGIN ".$sql."; END;");
			/****/
			if($Bcursor){
				$this->id_cursor = $this->setNewCursor();
				$bindvars[] = array(
					0 => ':CURSOR',
					1 => $this->id_cursor,
					2 => -1,
					3 => OCI_B_CURSOR
				);
			}	
			//print_r($bindvars);			
			/****/			
			$bindvars = $this->sanitize2($bindvars);
			if( $this->ociExec()){
				return $bindvars;//;
				
			}
			
			/*if( $this->exec_w_commit() ){
				return oci_fetch_array($c);
				//return $this->execFetchAll();
				return $bindvars;
			}*/
			return null;
		}
		
		public function getCursor(){
			$this->ociExec($this->id_cursor);
			//return oci_fetch_array($this->id_cursor);
			oci_fetch_all($this->id_cursor, $res, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
			$this->stid = null; 
			return $res;
		}

		
		public function single_query($sql,$bindvars){
			$this->connect();
			$this->prepare($sql);
			if( !empty($bindvars) ){ 
				if( $this->sanitize($bindvars) ){
					//$this->sanitize($bindvars);
					return $this->exec_w_commit();
					return 'ok';
				}else{
					return false;
				}
			}
		}
		
		
		public function activa_nls_sort() {
			$sql = "ALTER SESSION SET NLS_COMP='LINGUISTIC'";
			$res = $this->single_query($sql,'');
		}
		public function activa_nls_comp() {
			$sql = "ALTER SESSION SET NLS_SORT='BINARY_AI'";
			$res = $this->single_query($sql,'');
		}
		
		public function last_id($secuencia) {
			$sql = "SELECT ".$secuencia.".CURRVAL FROM DUAL";
			$res = $this->query_fetch($sql,'');
			$this->disconnect();
			return $res;
		}
		
		/*****************************************************************************************************************
												NUEVA SECUENCIA
		******************************************************************************************************************/
		
		
		public function giveMeID($sqc) {
			$sql = "SELECT ".$sqc.".nextval as P_KEY FROM dual";
			$resp = $this->query_fetch($sql,'');
			return $resp[0]['P_KEY'];
		}
		
		/*****************************************************************************************************************
												ELIMINAR REGISTROS
		******************************************************************************************************************/
		public function recicla_elemento($tabla,$clave,$bindvars){
			$sql="UPDATE ".$tabla."  SET ESTADO = 5 WHERE ".$clave."=:COD";
			return $this->single_query($sql,$bindvars);			
		}
		public function eliminaElemento($tabla,$clave,$bindvars){
			$sql="DELETE FROM ".$tabla." WHERE ".$clave."=:COD";
			return $this->single_query($sql,$bindvars);			
		}
		
		
		
		/*****************************************************************************************************************
												  OTROS
		******************************************************************************************************************/
		public function getFileInfo($bindvars) {
			$sql = "SELECT P_KEY,PARENT,NOMBRE,EXT,TAMANIO,UBICACION,TO_CHAR(FECHA_SUBIDA,'DD/MM/YYYY') AS FECHA_SUBIDA,ESTADO
					FROM A_ARCHIVOS
					WHERE P_KEY = :COD";
			return $this->query_fetch($sql,$bindvars);
		}
		/*****************************************************************************************************************
												  DATOS COMBOBOX
		******************************************************************************************************************/
		public function setAC_LOG($bindvars) {
			$sql = "INSERT INTO AC_TRABAJOS_LOG
					VALUES (SQ_AC_TRABAJOS_LOG.NEXTVAL,:ID_TRABAJO,TO_DATE(:FECHA,'DD/MM/YYYY'),:HORA_INICIO,:HORA_TERMINO,:TIPO,:ID_USUARIO,'')";
			return $this->single_query($sql,$bindvars);
		}
		public function setCI_LOG($bindvars) {
			$sql = "INSERT INTO RI_TRABAJOS_LOG
					VALUES (SQ_RI_TRABAJOS_LOG.NEXTVAL,:ID_TRABAJO,TO_DATE(:FECHA,'DD/MM/YYYY'),:HORA_INICIO,:HORA_TERMINO,:TIPO,:ID_USUARIO,'')";
			return $this->single_query($sql,$bindvars);
		}
		public function lModulos() {
			$sql = "SELECT P_KEY, NOMBRE, CARPETA, SITIO FROM U_MODULOS ORDER BY NOMBRE ASC";
			return $this->query_fetch($sql,$bindvars);
		}
		public function listaParent($id_lista, $parent, $orden_por='P_KEY'){
			$parent = (($parent=='0' || $parent)?' AND PARENT='.$parent:'');
			$sql = "SELECT P_KEY, NOMBRE, NVL(PARENT,0) AS PARENT FROM L_LISTAS_PARENT WHERE ID_LISTA=".$id_lista." AND ESTADO='1' ".$parent." ORDER BY ".$orden_por;
			return $this->query_fetch($sql,null);
		}
		public function listaSimple($id_lista, $orden_por='P_KEY') {
			$sql = "SELECT P_KEY, NOMBRE, ESTADO FROM L_LISTAS_SIMPLES WHERE ID_LISTA=".$id_lista." AND ESTADO='1' ORDER BY ".$orden_por;
			return $this->query_fetch($sql,null);
		}
		public function carga_combo($tabla, $columnas='P_KEY, NOMBRE', $orden_por='P_KEY') {
			$sql = "SELECT ".$columnas." FROM ".$tabla."  WHERE ESTADO='1' ORDER BY ".$orden_por;
			return $this->query_fetch($sql,null);
		}
		
	
	
}