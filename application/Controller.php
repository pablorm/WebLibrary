<?php
abstract class Controllermass
{
	function data2b64($_data){
		if(is_array($_data)){
			foreach($_data as $key=>$val){
				if( is_array($val) ){
					$_data[$key] = $this->data2b64($val);
				}else{
					$_data[$key] = base64_encode($val);
				}
			}
		}
		return $_data;
	}
	
	function array_utf8($_data,$ende){
		if(is_array($_data)){
			foreach($_data as $key=>$val){
				$_data[$key] = $this->array_utf8($val,$ende);
			}				
		}else{				
			if($ende=='ENCODE'){
				$_data = utf8_encode($_data);
			}elseif($ende=='DECODE'){
				$_data = utf8_decode($_data);
			}
		}			
		return $_data;
	}
	public function CheckboxChecked($data2,$comparador, $id_check_inf,$_datos){
		foreach($data2 as $key=>$val){
			$_datos[0][$id_check_inf.$val['P_KEY']] = ( ($_datos[0][$comparador]==$val['P_KEY'])? "checkbox_si.png" : "checkbox_no.png");
		}
		return $_datos;
	}
	function showPDF($file,$filename,$filesize){
		if (file_exists($file)){
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="' . $filename . '"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . $filesize);
			header('Accept-Ranges: bytes');
			@readfile($file);
		}
	}
    function array_column($_data,$column){
		return array_map(function($element){return $element[$column]; }, $_data);
	}
	
	function nombremes($mes){
		setlocale(LC_TIME, 'spanish');  
		$nombre=strftime("%B",mktime(0, 0, 0, $mes, 1, 2000)); 
		return $nombre;
	}
	
	protected function XMLGetValues($lista,$_nodos){
		$dir_xml = XML_LISTBOX;
		$array 	 = array();
		$_data = array();
		if( is_file( $dir_xml ) && $lista ){
			$doc = simplexml_load_file($dir_xml);				
			foreach( $doc->$lista->row as $key=>$val ){
				$array = array();
				foreach($_nodos as $v){
					$array[$v] = (string)$val->{$v};
				}
				$_data[] = $array;
			}
		}
		return $_data;
	}
		
	public function replaceIDtoValueXML($_data,$lista, $id, $nombre,$xml_item_name='name'){
		// array con datos de BD, nombre de listado XML, ID del array a buscar, nombre final del valor a entregar
		foreach($_data as $key=>$val){
			if(isset($val[$id]))
				$_data[$key][$nombre] = $this->get_value_xml( $lista, $val[$id],$xml_item_name); 
		}
		return $_data;
	}
	protected function get_value_xml($lista,$cod,$item){
		$dir_xml = XML_LISTBOX;
		$array 	 = array();
		if( is_file( $dir_xml ) && $lista ){
			$doc = simplexml_load_file($dir_xml);
			foreach( $doc->$lista->row as $key=>$val ){					
				if( $val->id == $cod ){
					return (string)$val->$item;	
				}
			}
		}
	}
	protected function get_value_xmllist($lista,$cod){
		$dir_xml = XML_LISTBOX;
		$array 	 = array();
		if( is_file( $dir_xml ) && $lista ){
			$doc = simplexml_load_file($dir_xml);
			foreach( $doc->$lista->row as $key=>$val ){					
				if( $val->id == $cod ){
					return (string)$val->name;	
				}
			}
		}
	}
	protected function XML2Select($lista,$estado,$_items=array('id','name')){
		$dir_xml = XML_LISTBOX;
		$array 	 = array();
		if( is_file( $dir_xml ) && $lista ){
			$doc = simplexml_load_file($dir_xml);
			foreach( $doc->$lista->row as $key=>$val ){
				if( $estado || $estado=='0' ){
					if( $val->status==$estado  ){
						$array[] = array(	'P_KEY'=>(string)$val->$_items[0],
											'NOMBRE'=>(string)$val->$_items[1]	
										);
					}
				}else{
					$array[] = array(	'P_KEY'=>(string)$val->$_items[0],
										'NOMBRE'=>(string)$val->$_items[1]	
									);
				}
			}
		}else
			return false;
			
		return $array;
	}
	protected function XML2ListBox($lista,$estado){
		$dir_xml = XML_LISTBOX;
		$array 	 = array();
		if( is_file( $dir_xml ) && $lista ){
			$doc = simplexml_load_file($dir_xml);
			foreach( $doc->$lista->row as $key=>$val ){
				if( $estado || $estado=='0' ){
					if( $val->status==$estado  ){
						$array[] = array(	'P_KEY'=>(string)$val->id,
											'NOMBRE'=>(string)$val->name	
										);
					}
				}else{
					$array[] = array(	'P_KEY'=>(string)$val->id,
										'NOMBRE'=>(string)$val->name	
									);
				}
			}
		}else
			return false;
			
		return $array;
	}	
	protected function download_file($name, $path, $type)
    {
        
        if (is_file($path)) {
            $size = filesize($path);
            if (function_exists('mime_content_type')) {
                $type = mime_content_type($path);
            } elseif (function_exists('finfo_file')) {
                $info = finfo_open(FILEINFO_MIME);
                $type = finfo_file($info, $path);
                finfo_close($info);
            }
            if ($type == '') {
                $type = "application/force-download";
            }
			/*
			elseif($type == 'application/pdf'){

				header('Content-type: application/pdf');
				header('Content-Disposition: inline; filename="' . $name . '"');
				header('Content-Transfer-Encoding: binary');
				header('Content-Length: ' . $size);
				header('Accept-Ranges: bytes');

				@readfile($path);
				exit;
			}*/
            // Definir headers
            header("Content-Type: $type");
            header("Content-Disposition: attachment; filename=$name");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . $size);
            // Descargar archivo
            readfile($path);
            exit;
        } else {
            return false;
        }
    }    
    public function rm_include($includeVars,$rmv){
		$rmv = ':'.mb_strtoupper($rmv);
		$b=0;
		
		foreach( $includeVars as $key=>$val ){
			if( $val[0] == $rmv ){
				unset($includeVars[$b]);
			}
			++$b;
		}
		ksort($includeVars);
		return $includeVars;
	}		
	public function add_include($includeVars,$getVars,$const){
		$includeV1 = $this->include_vars($getVars,'',$const);
		$a=0;
		foreach( $includeVars as $key=>$val ){
			$b=0;
			foreach( $includeV1 as $clave=>$valor ){
				
				if( $val[0] == $valor[0] ){
					$includeVars[$a] = $valor;
					unset($includeV1[$clave]);
				}
			}
			++$a;
		}
		if( $includeV1 ){
			$new_array = array_merge($includeVars,$includeV1);
			ksort($new_array);
			return $new_array;
		}else{
			ksort($includeVars);
			return $includeVars;
		}
	}
    public function include_vars($getVars, $exclude, $form)
    {
        global $listado_inputs;
        
        $error_encontrado;
        $includeVars = array();
        $vars_error  = array('ERROR');
        // valido el tipo de formulario
        if ($form) {
            foreach ($getVars as $key => $val) {
				$error_encontrado=null;
				$zero = null;
				$largo = null;
                $tipo = null;
                $valido_nulo = null;
                // obtengo el array del campo y sus propiedades
				$listado_inputs[$form][strtoupper($key)] = !isset($listado_inputs[$form][strtoupper($key)]) ? 0 : $listado_inputs[$form][strtoupper($key)];
				
                if (is_array($listado_inputs[$form][strtoupper($key)])) {
					//$listado_inputs[$form][strtoupper($key)]['zero'] = !isset($listado_inputs[$form][strtoupper($key)]['zero']) ? 0 : $listado_inputs[$form][strtoupper($key)]['zero'];
					if( isset($listado_inputs[$form][strtoupper($key)]['largo']) )
						$largo       = $listado_inputs[$form][strtoupper($key)]['largo'];
					if( isset($listado_inputs[$form][strtoupper($key)]['tipo']) )
						$tipo        = $listado_inputs[$form][strtoupper($key)]['tipo'];
					if( isset($listado_inputs[$form][strtoupper($key)]['valido_nulo']) )	
						$valido_nulo = $listado_inputs[$form][strtoupper($key)]['valido_nulo'];
					if( isset($listado_inputs[$form][strtoupper($key)]['zero']) )	
						$zero        = $listado_inputs[$form][strtoupper($key)]['zero'];
                    
                    
                    if ($zero != 'si') {
						if ($val) {
                            // verifico que no exceda el largo
							if ($largo < strlen($val)) 
                                $error_encontrado = 1;                            
						} else {
							if ($valido_nulo == 'no')
                                $error_encontrado = 1;
							else {
                                if ($form == 'buscador')
                                    $exclude[] = $key;
                                else
                                    $val = '';                               
							}
						}
                    } else {
                        if ($val != '0') {                            
                            if ($val) {
                                // verifico que no exceda el largo
                                if ($largo < strlen($val)) 
                                    $error_encontrado = 1;                                
							} else {
								if ($valido_nulo == 'no') 
                                    $error_encontrado = 1;
                                else {
									if ($form == 'buscador') 
                                        $exclude[] = $key;
                                    else
                                        $val = '0';                                    
								}
							}
                        }
                    }
                    
                    
                    
                    if ($this->valida_tipo($tipo, $val) && !$error_encontrado) {
                        if (isset($exclude)) {
							if (is_array($exclude)) {
								if (!in_array($key, $exclude))
									$includeVars[] = array(
                                        ':' . strtoupper($key),
                                        $val,
                                        '-1'
                                    );
							} else {
								if ($key != $exclude)
									$includeVars[] = array(
										':' . strtoupper($key),
                                        $val,
                                        '-1'
                                    );
                            }
                        } else {
                          $includeVars[] = array(
                                ':' . strtoupper($key),
                                $val,
                                '-1'
                            );
                        }
                    } else {
                        $vars_error[] = $key;
                    }
                }
                unset($error_encontrado);                
            }
        }
        if (count($vars_error) >= 2) {
            return $vars_error;
        }
        return $includeVars;
    }
	
    protected function valida_tipo($tipo, $valor)
    {
        if ($tipo && $valor) {
            switch ($tipo) {
                case 'integer':
                    if (!$this->is_valid_digit($valor)) {
                        return false;
                    }
                    break;
                
                case 'alphanum':
                    if (!$this->is_valid_alnum($valor)) {
                        return false;
                    }
                    break;
                
                case 'chars':
                    if (!$this->is_valid_char($valor)) {
                        return false;
                    }
                    break;
					
                case 'email':
                    if (!$this->is_valid_email($valor)) {
                        return false;
                    }
                    break;
					
                case 'fecha':
                    if (!$this->is_valid_date($valor)) {
                        return false;
                    }
					break;
					
                case 'filename':
                    $valor = $this->is_valid_filename($valor);                    
                    break;
					
                case 'text':
                    $valor = $this->is_valid_text($valor);                    
                    break;
					
                case 'number':
                    $valor = $this->is_valid_number($valor);                    
                    break;
            }
        }
        return true;
    }
    
    
    protected function is_valid_date($val)
    {
        $partes = explode("/", $val);
        if ($this->is_valid_digit($partes[1]) && $this->is_valid_digit($partes[0]) && $this->is_valid_digit($partes[2])) {
            if (!checkdate($partes[1], $partes[0], $partes[2])) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
    
    public function is_valid_digit($val)
    {
        if (!ctype_digit($val)) {
            return false;
        }
        return true;
    }
	
	protected function is_valid_number($val)
    {
        if (!is_numeric($val)) {
            return false;
        }
        return true;
    }
    
    protected function is_valid_char($val)
    {
        if (!ctype_alpha($val)) {
            return false;
        }
        return true;
    }
    
    protected function is_valid_alnum($val)
    {
        if (!ctype_alnum($val)) {
            return false;
        }
        return true;
    }
    protected function is_valid_email($val)
    {
        if (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }
    
    protected function is_valid_filename($val)
    {
        $nombreArchivo = str_replace(array(
            '\\',
            '/',
            ':',
            '*',
            '?',
            '"',
            '<',
            '>',
            '|'
        ), "", $val);
		$nombreArchivo = str_replace(array('SCRIPT','OBJECT','APPLET','EMBED','FORM'),'',$val);
        return $this->clean_xss($nombreArchivo);
    }
	
	protected function clean_xss($val){
		$_exc= array(
			'SCRIPT','OBJECT','APPLET','EMBED','FORM','script','object','applet','embed','form',
			'-->','&','<','>',"\\",'&amp;','&lt;','&gt;','&quot;','&#x27;','&#x2F;');
		return str_replace($_exc,'',$val);
	}
    
    protected function is_valid_text($val)
    {
        $val = str_replace(array(
            '\\',
            '/',
            ':',
            '*',
            '?',
            '"',
            '<',
            '>',
            '|',
            '&',
            'ç'
        ), "", $val);
        return $this->clean_xss($val);
    }
    
    
    protected function paginar($includeVars, $iniciopagina, $finalpagina)
    {
        if (!$iniciopagina)
            $iniciopagina = 1;
        $finalpagina   = $iniciopagina + $finalpagina;
        $includeVars[] = array(
            ':INICIOPAGINA',
            $iniciopagina,
            '-1'
        );
        $includeVars[] = array(
            ':FINALPAGINA',
            $finalpagina,
            '-1'
        );
        return $includeVars;
    }
    
    
    public function redireccionar($ruta = false)
    {
        if ($ruta) {
            header('location: index.php?' . $ruta);
            exit;
        } else {
            header('location: index.php');
            //header('location: index.php?' . PAGINA_PRINCIPAL);
            exit;
        }
    }
	
	public function checkSubpage($subpage){
		$_data = Session::get('modulo_acceso');
		if(is_array($_data)){
			foreach($_data as $key=>$val){
				//echo $subpage;
				$c = strlen($subpage);
				//exit;
				if( substr($val['CARPETA'],0,$c) == $subpage ){
					Session::set('MAIN_MODULE_PERM',$val['PERMISO']);
					return true;
				}
				/*
				if( $val['CARPETA']==$subpage ){
					Session::set('MAIN_MODULE_PERM',$val['PERMISO']);
					return true;
				}*/
			}
		}
		return false;
	}
	
	protected function valida_sitio($subpage, $sitios)
    {
        $session_sitio = Session::get('sitio');
		$k_sitios = explode(',', $session_sitio);
        if (array_key_exists($subpage, $sitios)) {
            if (!in_array($sitios[$subpage], $k_sitios)) {
                return $this->cambia_subpage($sitios, $k_sitios[0]);
            }
        } else {
            return $this->cambia_subpage($sitios, $k_sitios[0]);
        }
        return $subpage;
    }
	
	protected function cambia_subpage($sitios, $sitio_buscado)
    {
        foreach ($sitios as $key => $val) {
            if ($sitio_buscado == $val) {
                $subpage = $key;
            }
        }
        return $subpage;
    }
    
    public function get_Permission($page)
    {
        if(APLICATION_MODE!=0 ){  /***   ACCESO A TODO EN MODO DE DESARROLLO; QUITAR DESPUES   ****/
			/*
			$permiso = PERMISO_LECTURA; //PERMISO LECTURA
			$codigo  = Controller::get_ID_Module($page);
			foreach (Session::get('modulo_acceso') as $key => $val) {
				if ($val['ID_MODULO'] == $codigo) {
					$permiso = $val['ID_PERMISO'];
				}
			}
			return $permiso;*/
			return 7;
		}else 
			return 7;
    }
    public function get_ID_Module($page)
    {
        $Model = new ABModel;
		$ID_Modulos = $Model->lModulos();
		//global $ID_Modulos;
        foreach ($ID_Modulos as $key => $val) {
            if ($val['CARPETA'] == $page) {
                return $val['P_KEY'];
            }
        }
        return 0;
    }
	
	public function get_Site_Module($page)
    {
        $Model = new ABModel;
		$ID_Modulos = $Model->lModulos();
		//print_r($ID_Modulos);
		//global $ID_Modulos;
        foreach ($ID_Modulos as $key => $val) {
            if ($val['CARPETA'] == $page) {				
                return $val['SITIO'];
            }
        }
        return false;
    }
    
    public function valida_modulo_sitio($page)
    {
		$sitio            = $this->get_Site_Module($page);
        $session_sitios   = Session::get('sitio');
        $session_sitios   = explode(',', $session_sitios);
        $session_sitios[] = '0';
        if (in_array($sitio, $session_sitios))
            return true;
        else
            $this->redireccionar('sitio_principal');
        
    }
	
    protected function comprobar_permisos($permiso, $subpage)
    {
        global $Permiso_Descargar;
		global $Permiso_Modificar;
		
		if( in_array($subpage,$Permiso_Modificar) ){
			if ( $permiso == PERMISO_LECTURA )
				$subpage = MODULO;
		}

		if( in_array($subpage,$Permiso_Descargar) ){
			if ( $permiso == PERMISO_LECTURA || $permiso == PERMISO_MODIFICAR )
				$subpage = MODULO;
		}
			
		return $subpage;
    }
	
	public function comprobar_nivel($codigo, $subpage)
    {
        if (is_array(Session::get('modulo_acceso'))) { 
			//print_r(Session::get('modulo_acceso'));
            foreach (Session::get('modulo_acceso') as $key => $val) {
                if ($val['ID_MODULO'] == $codigo) {
                    define('PERMISO_MODULO', $val['ID_PERMISO']);
                    return $this->comprobar_permisos($val['ID_PERMISO'], $subpage);
                }
            }
            $this->redireccionar('sitio_principal');
        }
    }
}

?>
