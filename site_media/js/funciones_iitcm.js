
function carga_filas(data){
	console.log('F1>>>>>>>>>>>:',data);
	
	var json = $.parseJSON(data);
	var id_li = json['opciones']['CONTENEDOR'];
	//console.log('Cont::',json['opciones']['contenedor']);
	
	$.each(json['filas'], function(idx, obj) {
		var datos = new Array();
		datos['P_KEY'] = obj.P_KEY;
		datos['NOMBRE'] = obj.NOMBRE;
		datos['TIPO_ELEMENTO'] = obj.TIPO_ELEMENTO;
		datos['TIPO_CONTENEDOR'] = json['opciones']['TIPO_CONTENEDOR'];
		datos['CONTENEDOR'] = json['opciones']['CONTENEDOR'];
		datos['A_ARCHIVOS'] = obj.A_ARCHIVOS;
		datos['ID_DIALOGO'] = json['opciones']['ID_DIALOGO'];
		
		agrega_fila(datos)	
	});
}

function agrega_fila(datos){
	
	//console.log('Filas>>:',datos)
	var accion_elimina 	= ''; 
	var elemento 		= '';			
	var tipo_elemento 	= datos['TIPO_ELEMENTO'];		
	var file 			= datos['A_ARCHIVOS'];
	var tipo_contenedor = datos['TIPO_CONTENEDOR'];
	var nombre 			= datos['NOMBRE'];
	if( nombre.length > 40 ) 
		nombre = nombre.substr(0,40)+'...';		
	var contenedor 		= datos['CONTENEDOR'];
	var id_dialogo 		= datos['ID_DIALOGO'];
	
	var datos 			= '"'+file+'","'+tipo_contenedor+'","'+file+'","'+elemento+'"';
	if( tipo_contenedor == 'li' ){	
		if( VerificaModoEdicion() )
			accion_elimina =" <a href='#' onclick='quitar_archivo("+datos+")'>Eliminar</a>"	;		
		var response = 	"<li id='f"+file+"' class='li_hover'><div class='div_content' style='float:left;width:100%;' >"+
						"<div class='div_content' style='float:left;width:35%;padding-left:2%;'>"+tipo_elemento+"</div>"+
						"<div class='div_content' style='float:left;width:53%;'>"+nombre+"</div>"+
						"<div class='div_content' style='float:right;width:10%;'>"+ accion_elimina +
						"</div></div></li>" 
		if ($('#' + contenedor + ' li').length == 0) {
			$('#' + contenedor).html(response).fadeIn("fast");
		} else {
			$('#' + contenedor).prepend(response);
		}
		$('#' + contenedor + ' li').eq(0).hide().show("slow");
		
	}else if( tipo_contenedor == 'div' ){
		
		var datos = '"'+file+'","'+tipo_contenedor+'","'+contenedor+'","'+id_dialogo+'"';
		if( VerificaModoEdicion() )
			accion_elimina =" <a href='#' onclick='quitar_archivo("+datos+")'>Eliminar</a>"	;							
		var respuesta = nombre + accion_elimina
		document.getElementById(contenedor).innerHTML = respuesta;
		$("#"+id_dialogo).prop('disabled', true)
	}
	
}




function quitar_archivo(file,tipo,id,elemento) {
	var permisos=obten_permisos();
	if(permisos){	
		if(permisos!='4'){
			if (confirm("¿Desea eliminar este archivo?")) {
				var direccion = "index.php?iitcm_fquit&file="+file
				console.log(direccion);
				ajax_get(direccion,3);
				if(tipo=='li'){
					document.getElementById("f"+file).style.display = "none";	
				}else if(tipo=='div'){
					document.getElementById(id).innerHTML = '';
					$("#"+elemento).prop('disabled', false)
				}
								
			}
			//arriba();
		}else{
			alert("No tiene permisos para acceder a este recurso");	
			//arriba();			
		}	

	}
}


function sendFile(files, url) {
	var data = new FormData();
	$.each(files, function (key, value) {
		data.append(key, value);
	});
	//console.log(data,data[0],data);
	$.ajax({
		type: 'post',
		url: url,
		data: data,
		success: carga_filas,
		xhrFields: {
			// add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
			onprogress: function (progress) {
				//console.log(progress);
				// calculate upload progress
				var percentage = Math.floor((progress.total / progress.totalSize) * 100);
				// log upload progress to console
				//console.log('progress', percentage);
				if (percentage === 100) {
					console.log('DONE!');
				}
			}
		},
		processData: false,
		contentType: false
	});
}






function abre_asistentes(cod) {
	if(cod){
		miPopup = window.open("index.php?iitcm_asistentes&cod="+cod,"miwin","width=550,height=330,scrollbars=yes");
		miPopup.focus();
	}else{
		alert("Error");
	}
}

function prepara_fechas(){
	var fecha_inicio = document.getElementById("fecha_inicio").value;
	var fecha_termino = document.getElementById("fecha_termino").value;
	var resultado = resta_fecha(fecha_inicio, fecha_termino);
	document.getElementById("resta_fecha").value = resultado	
}





function response(response) {
	
	var json = $.parseJSON(response);
	//console.log('>>>>>>',response);
	if(json[0]['tipo_respuesta'] == 'li'){
		
		var id_li 	= json[0]['repository'];
		var file 	= json[0]['id_archivo'];
		var tipo 	= 'li';
		var id 		= json[0]['id_respuesta'];
		var elemento = " ";
		var nombre 	= json[0]['name'];
		var datos 	= '"'+file+'","'+tipo+'","'+file+'","'+elemento+'"';
						
		var response = 	"<li id='f"+file+"' class='li_hover'><div class='div_content' style='float:left;width:100%;' >"+
						"<div class='div_content' style='float:left;width:35%;padding-left:2%;'>"+json[0]['type']+"</div>"+
						"<div class='div_content' style='float:left;width:53%;'>"+json[0]['name']+"</div>"+
						"<div class='div_content' style='float:right;width:10%;'><a href='#' onclick='quitar_archivo("+datos+")'>Eliminar</a>"+
						"</div></div></li>"  
		if ($('#' + id_li + ' li').length == 0) {
			$('#' + id_li).html(response).fadeIn("fast");
		} else {
			$('#' + id_li).prepend(response);
		}
		$('#' + id_li + ' li').eq(0).hide().show("slow");
		
	}else if(json[0]['tipo_respuesta'] == 'div'){
		var file = json[0]['id_archivo'];
		var tipo = 'div';
		var id = json[0]['id_respuesta'];
		var elemento = json[0]['id_elemento'];
		var nombre = json[0]['name'];
		var datos = '"'+file+'","'+tipo+'","'+id+'","'+elemento+'"'
		var response = nombre+"<a href='#' onclick='quitar_archivo("+datos+")'>Eliminar</a>"
		document.getElementById(json[0]['id_respuesta']).innerHTML = response;
		$("#"+json[0]['id_elemento']).prop('disabled', true)
		
		
	}
}