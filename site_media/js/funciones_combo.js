		/**  arrays :: 0=cod; 1=name; 2=parent **/
		
		function desactiva_combos(dc) {
            if (dc == 1) {
				$("#combo2").attr("disabled", true);
				document.getElementById('combo2').innerHTML = "";
            }			
            $("#combo3").attr("disabled", true);
            $("#combo4").attr("disabled", true);
			document.getElementById('combo3').innerHTML = "";
			document.getElementById('combo4').innerHTML = "";
        }
		
		function desactiva_combobox(dc) {
			$("#combo"+dc).attr("disabled", true);
			document.getElementById('combo'+dc).innerHTML = "";
		}
		
		
		
		
		function ajax_get_combos(url,cont){
			$.get("index.php?bitacoras_"+url,
			function (resultado) {
				a=0;
				var z = JSON.parse(resultado);
				if(cont==0){
					for(var i in z) {
						array_1[a] = [ z[i]['id'] , z[i]['nombre']];
						a++;
					}
					document.getElementById('sistema').innerHTML = "";	
					carga_datos_combo('sistema','NULL',array_1);					
				}
				
				if(cont==1){
					for(var i in z) {
						array_2[a] = [ z[i]['id'] , z[i]['nombre'] ,  z[i]['parent']];
						a++;
					}
				}
				
				if(cont==2){
					for(var i in z) {
						array_3[a] = [ z[i]['id'] , z[i]['nombre'] ,  z[i]['parent']];
						a++;
					}
				}
				
				if(cont==3){
					for(var i in z) {
						array_4[a] = [ z[i]['id'] , z[i]['nombre'] ,  z[i]['parent']];
						a++;
					}
				}									
			});				
		}
		
       
		
		function carga_datos_combo(a_combo,code,ar_selects){
			var select_insert="<option value='0' selected>Seleccione uno..</option>";
			if(code=='NULL'){
				for (var i = 0; i < ar_selects.length; i++){
					select_insert = select_insert+"<option value='"+ar_selects[i][0]+"'>"+ar_selects[i][1]+"</option>";				
				}				
			}else{
				for (var i = 0; i < ar_selects.length; i++){
					if(parseInt(ar_selects[i][2]) == parseInt(code)){
						select_insert = select_insert+"<option value='"+ar_selects[i][0]+"'>"+ar_selects[i][1]+"</option>";					
					}
				}
			}
			$("#"+a_combo).attr("disabled", false);
			$('#'+a_combo).append(select_insert);
			document.getElementById(a_combo).options[0].selected = true;
			delete select_insert;
			delete ar_selects;					
		}
		
		function combo_dependiente(a_combo,b_combo,ar_selects) {			
            var code = $("#"+a_combo).val();
            document.getElementById(b_combo).innerHTML = "";
            if (code != 0) {
				carga_datos_combo(b_combo,code,ar_selects);
            }
        }