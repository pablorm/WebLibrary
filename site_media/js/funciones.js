	var cursor;
	if (document.all) {
	// Está utilizando EXPLORER
	cursor='hand';
	} else {
	// Está utilizando MOZILLA/NETSCAPE
	cursor='pointer';
	}
	var miPopup;
	
	function arriba(){
		window.parent.$("body").animate({scrollTop:0}, 'fast');
	}
	

	function sendFile(files, url, funcion) {
		var data = new FormData();
		$.each(files, function (key, value) {
			data.append(key, value);
		});
		//console.log(data,data[0],data);
		$.ajax({
			type: 'post',
			url: url,
			data: data,
			success: funcion,
			xhrFields: {
				// add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
				onprogress: function (progress) {
					//console.log(progress);
					// calculate upload progress
					var percentage = Math.floor((progress.total / progress.totalSize) * 100);
					// log upload progress to console
					//console.log('progress', percentage);
					/*
					if (percentage === 100) {
						console.log('DONE!');
					}*/
				}
			},
			processData: false,
			contentType: false
		});
	}
	/******************************************************************************************************************************************************
															 	LIGHT BOX
	*******************************************************************************************************************************************************/
	function is_int(data){
		var intRegex = /^\d+$/;
		if(intRegex.test(data))
			return true;
		return false;			
	}
	
	
	function Lightbox_parent(modulo, submodulo, cod) {
		arriba();
		var href = "index.php?"+modulo+"_"+submodulo+'&cod='+cod;								
		window.parent.parent.Shadowbox.open({  
			content:    href,  
			player:     "iframe",  
			//title:    1005, 
			width: 		1002, 
			height:     700 
		});
	}
	

	
	function calcula_height(){
		var divHeight;
		var obj = document.getElementById('primary_table');
		
		if(obj.offsetHeight)          {divHeight=obj.offsetHeight;}
		else if(obj.style.pixelHeight){divHeight=obj.style.pixelHeight;}
		//alert(divHeight);
		if((parseInt(divHeight)+16) < 700){
			parent.parent.document.getElementById("sb-wrapper-inner").style.height=(parseInt(divHeight))+'px';  
		}
		//parent.document.getElementById("sb-wrapper").style.width='1005px';
	}
	
	
	function ShadowImage(direccion){
			var myImage = new Image();
			myImage.name = "image.jpg";
			myImage.onload = getWidthAndHeight;
			myImage.onerror = loadFailure;
			myImage.src = direccion;			
		}
		function getWidthAndHeight() {
			Shadow_box(this.src,this.width,this.height)
			return true;
		}
		function loadFailure() {
			alert("'" + this.name + "' No se pudo cargar la imagen.");
			return true;
	}
	

	
	function Shadow_box(direccion,lwid,lhei) {
		var href = direccion;								
		window.parent.Shadowbox.open({  
			content:    direccion,  
			player:     "iframe",  
			//title:    1005, 
			width: 		lwid, 
			height:     lhei  
		});
	}
	
	
	function VerElemento(cod,modulo,width,height){
		var div_id = "dialog7";
		var iframe_id = "dialog_7";
		$("body").append('<div id="'+div_id+'" style="display:none;"><iframe id="'+iframe_id+'" src="" frameborder="0" width="'+width+'" height="'+height+'"></iframe></div>');
		
		if( $('#'+div_id).dialog() )
			$('#'+div_id).dialog('destroy');
		$('#'+div_id).dialog({
			autoOpen: false,
			show: "fade",
			hide: "fade",
			modal: true,
			position: { my: "center", at: "top"},
			open: function(ev, ui){
				$('#'+iframe_id).attr('src','index.php?'+modulo+'_visualizar&cod='+cod);
				$(".ui-widget-overlay").css({
					opacity: 1.0,
					filter: "Alpha(Opacity=1)",
					backgroundColor: "black"
				});
				$(".ui-dialog-content").css({
					padding:0
				});
			},
			height: 'auto',
			width: 'auto',
			resizable: false,
			buttons: {
				Cerrar: function(){
					$( this ).dialog( "close" );
					$('#'+div_id).remove();
				}
			}
		});
		$(".ui-dialog-titlebar").hide();
		$( '#'+div_id ).dialog( "open" );
	}
	
	function VerElemento2(url,width,height){
		var div_id = "dialog7";
		var iframe_id = "dialog_7";
		$("body").append('<div id="'+div_id+'" style="display:none;"><iframe id="'+iframe_id+'" src="" frameborder="0" width="'+width+'" height="'+height+'"></iframe></div>');
		
		if( $('#'+div_id).dialog() )
			$('#'+div_id).dialog('destroy');
		$('#'+div_id).dialog({
			autoOpen: false,
			show: "fade",
			hide: "fade",
			modal: true,
			position: { my: "center", at: "top"},
			open: function(ev, ui){
				$('#'+iframe_id).attr('src',url);
				$(".ui-widget-overlay").css({
					opacity: 0.5,
					filter: "Alpha(Opacity=0.1)",
					backgroundColor: "black"
				});
				$(".ui-dialog-content").css({
					padding:0
				});
			},
			height: 'auto',
			width: 'auto',
			resizable: false,
			buttons: {
				Cerrar: function(){
					$( this ).dialog( "close" );
					$('#'+div_id).remove();
				}
			}
		});
		$(".ui-dialog-titlebar").hide();
		$( '#'+div_id ).dialog( "open" );
	}
	/******************************************************************************************************************************************************
															 			DIALOGOS Y WORKERS
	*******************************************************************************************************************************************************/
	
	
	function setDialog(nombre,btns,w,h) {
		if ($(nombre).dialog())
            $(nombre).dialog('destroy');
			
		$(nombre).dialog({
			autoOpen: false,
			height: h,
			width: w,
			modal: true,
			position: {
				my: "center",
				at: "top+50%"
			},
			resizable: false,
			buttons: btns
		});
		$(".ui-dialog-titlebar").hide();
	}
	function setElementValue(json,base64,key){
		var fKey = ((key)?key:'V');
		if( json ){
			$.each(json, function(name, value) {
				if( base64 && value)
					value = atob((value?value:''));
				else
					value = (value?value:'');
					
				name = name.toLowerCase();
				label = document.getElementById(fKey+name);
				
				if( label ){
					switch(label.type){
						case 'hidden': label.value = value; break;
						case 'select-one': seleccionar(fKey+name,value); break;
						case 'text': label.value = value; break;
						default: label.innerHTML = value;
					}
				}
			});
			return true;
		}	
	}
	/******************************************************************************************************************************************************
															 	AJAX
	*******************************************************************************************************************************************************/
	function eliminar_archivo(file,modulo,tipo,id) {
		var permisos=obten_permisos();
		if(permisos){	
			if(permisos!='4'){
				if (confirm("¿Desea eliminar este archivo?")) {
					var direccion = "index.php?"+modulo+"&type="+tipo+"&file="+file
					ajax_get(direccion,3);
					document.getElementById(id).style.display = "none";					
				}
				arriba();
			}else{
				alert("No tiene permisos para acceder a este recurso");	
				//arriba();			
			}	

		}
	}
	
	function eliminar_archivo_e(file,cod,id) {
		var permisos=obten_permisos();
		if(permisos){	
			if(permisos!='4'){
				if (confirm("¿Desea eliminar este archivo?")) {
					var direccion = "index.php?equipos_eliminararchivo&file="+file+"&cod="+cod
					ajax_get(direccion,3);
					document.getElementById(id).style.display = "none";					
				}
			}else{
				alert("No tiene permisos para acceder a este recurso");	
			}	
		}
	}
		
	function recicla_elemento(cod, modulo){
		var permisos=document.getElementById("permisos").value;
		if(permisos && cod && modulo){	
			if(permisos!='4'){
				if (confirm("Atencion va a proceder a la eliminacion de un registro. Desea continuar?")) {					
					var direccion = "index.php?" + modulo + "_guardar&accion=ELIMINAR&cod=" + cod;
					ajax_get(direccion,4);					
					arriba();
				}
			}else{
				alert("No tiene permisos para acceder a este recurso");	
			}
		}else{
			alert("No se pudo realizar la accion solicitada");	
		}	
	}
	
	function reciclaElemento(cod, modulo){
		var permisos=obten_permisos();
		if(permisos && cod && modulo){	
			if(permisos!='4'){
				if (confirm("Atencion va a proceder a la eliminacion de un registro. Desea continuar?")) {					
					var direccion = "index.php?" + modulo + "_guardar&accion=ELIMINAR&cod=" + cod;
					sendViaGet(direccion,revisaRespuesta);					
					//arriba();
				}
			}else{
				alert("No tiene permisos para acceder a este recurso");	
			}
		}else{
			alert("No se pudo realizar la accion solicitada");	
		}	
	}
	
	function ajax_get(direccion,alerta){
		$.ajax({
				url:   direccion,
				success:  function (response) {
					if(alerta==1){
						alert(response)
					}else if(alerta==3){
						if(response){
							alert(response);
						}
					}else if(alerta==4){
						alert(response);
						parent.inicio();						
					}else if(alerta==5){
						test=response
					}					
				}
		});
		
	}
	
	function realizaProceso(valor,chk_select){
		var parametros = valor;
		$.ajax({
				url:   'index.php?mantenciones_actualiza&cod='+valor+'&solucion='+chk_select,
				success:  function (response){
					if(response){
						alert(response);
						parent.location.reload();
					}else{
						alert("Error al realizar accion");
					}
				}
		});
	}
	
	function elimina_img(file,cod,id){
		$.ajax({
			url: 'index.php?inspecciones_eliminarimagen&file='+file+'&cod='+cod,
			success: function(datos_recibidos){
				alert(datos_recibidos);
			}
		});	
		document.getElementById(id).style.display = "none";
	}
	
	
	function elimina_sistema(modulo){
			var cod = document.getElementById("cod").value
			if(confirm("Desea eliminar este registro?")){
				url='index.php?equipos_eliminar&modulo='+modulo+'&cod='+cod
				$.ajax({
					url: url,
					success: function(datos_recibidos) {
						alert(datos_recibidos);
						window.opener.location.reload();
						window.close();
					}
				});					
			}	
		}
		
	function sendViaGet(direccion,frespuesta){
		if( direccion ){
			if( frespuesta ){
				$.ajax({
					url:   direccion,
					success: frespuesta
				});
			}else{
				var data='test';
				$.ajax({
					url:   direccion,
            		async: true
				})
				.done(function( html ) {
					data = html;
				});
				return data;
			}
		}		
	}
	
	function sendViaPost(direccion,data,frespuesta){
		if(direccion){
			$.ajax({
			  	type: "POST",
			  	url: direccion,
			  	data: data,
			  	success: frespuesta
			});
		}		
	}
	
	
			/*
			$.post( "index.php?mantenciones_exportar", $( "#form_busqueda" ).serialize())
			  .done(function( data ) {
				alert( "Data Loaded: " + data );
			  });
			  
	/******************************************************************************************************************************************************
															 	CONTROL DE FECHAS
	*******************************************************************************************************************************************************/	  
			  
		function fecha(cadena) {
			//Separador para la introduccion de las fechas  
			var separador = "/"
		
			//Separa por dia, mes y año  
			if (cadena.indexOf(separador) != -1) {
				var posi1 = 0
				var posi2 = cadena.indexOf(separador, posi1 + 1)
				var posi3 = cadena.indexOf(separador, posi2 + 1)
				this.dia = cadena.substring(posi1, posi2)
				this.mes = cadena.substring(posi2 + 1, posi3)
				this.anio = cadena.substring(posi3 + 1, cadena.length)
			} else {
				this.dia = 0
				this.mes = 0
				this.anio = 0
			}
		}
		
		function resta_fecha(f1, f2) {
		
			if (f1 && f2) {
				var fecha1 = new fecha(f1)
				var fecha2 = new fecha(f2)
		
				var miFecha1 = new Date(fecha1.anio, fecha1.mes, fecha1.dia)
				var miFecha2 = new Date(fecha2.anio, fecha2.mes, fecha2.dia)
		
				var diferencia = miFecha2.getTime() - miFecha1.getTime()
				var dias = Math.floor(diferencia / (1000 * 60 * 60 * 24))
				return dias;
			} else
				return false;
		
		}	  
		
	/******************************************************************************************************************************************************
															 	CONTROL DE FORMULARIOS
	*******************************************************************************************************************************************************/
	function startDownload (n) { 
		var frame = document.getElementById("download");
		var frame_p = parent.document.getElementById("download");
		if( frame ){
			frame.src=n;
		}else if( frame_p ){
			frame_p.src=n;
		}
		return false;	
	}
	function IsJsonString(str) {
		try {
			var data = JSON.parse(str);
		} catch (e) {
			var data = false;
		}
		return data;
	}


	function reset_form_element(e) {
		e.wrap('<form>').parent('form').trigger('reset');
		e.unwrap();
	}



	
	function cambiarDisplay(id) {
		if (!document.getElementById) return false;
		fila = document.getElementById(id);
		if (fila.style.display != "none") {
			fila.style.display = "none"; //ocultar fila 
		}else{
			fila.style.display = ""; //mostrar fila 
		}
	}
	
		
	function seleccionar(elemento,valor) {
		var combo = document.getElementById(elemento)
		//var combo = document.forms["formulario"].criticidad;
		var cantidad = combo.length;
		for (i = 0; i < cantidad; i++) {
			if (combo[i].value == valor) {
				combo[i].selected = true;
			}   
		}
	}
	
	function exportar_excel(form,modulo){
		var inputs = $("#"+form).serialize();
		var url = 'index.php?'+modulo+'_exportar&'+inputs;
		console.log(url);
		startDownload(url);
		//window.open(url);	
	}
			
	function setData(){
		var frame = document.getElementById('frame_rejilla');
		var chkaprobado = document.getElementById('chkaprobado').value;
		var checkboxes = frame.contentWindow.document.getElementsByName('aprobado');
		
		for (var i = 0; i < checkboxes.length; i++){
			
			if(chkaprobado!=1){
				checkboxes[i].checked = true;
				document.getElementById('chkaprobado').value=1;
			}else{
				checkboxes[i].checked = false;
				document.getElementById('chkaprobado').value=0;
			}
		}
	}
	
	
	function enviar_correo(cod,modulo) {
		if (confirm("Atencion va a proceder con el envio de correo de alerta. Desea continuar?")) {
				parent.location.href="index.php?listas_enviacorreo&cod="+cod;
		}
		
	}
	
	
	function ver_elemento(cod,modulo) {
		if(modulo && cod){
			parent.location.href="index.php?" + modulo + "_visualizar&cod=" + cod;
		}else{
			alert("Error");
		}
	}
	
	function obten_permisos(){
		permisos=document.getElementById("permisos");
		if(!permisos){
			permisos=window.parent.document.getElementById("permisos");
			if(!permisos){
				return 4;	
			}
		}
		return permisos.value;
	}
	function VerificaModoEdicion(){
		accion=document.getElementById("accion");
		if(!accion){
			accion=opener.document.getElementById("accion");
			if(!accion){
				return false;	
			}
		}
		return true;	
	}
	
	function editar_equipo(cod,tipo){
		var permisos=obten_permisos();
		if(permisos){
			if(permisos!='4'){
				if(cod && tipo){
					switch(tipo){
						case 'area':
							miPopup = window.open("index.php?equipos_editarea&cod="+cod,"miwin","width=1010,height=145,scrollbars=no");				
							break;
						case 'sector':
							miPopup = window.open("index.php?equipos_editsector&cod="+cod,"miwin","width=1010,height=185,scrollbars=no");
							break;
						case 'equipo':
							miPopup = window.open("index.php?equipos_editequipo&cod="+cod,"miwin","width=802,height=583,scrollbars=no");
							break;
						case 'componente':
							miPopup = window.open("index.php?equipos_editcomponente&cod="+cod,"miwin","width=1001,height=176,scrollbars=no");
							break;	
					}
				}
				miPopup.focus();
			}else{
				alert("No tiene permisos para acceder a este recurso");				
			}
		}		
	}
	
	function nuevo_equipo(tipo){
		var permisos=obten_permisos();
		if(permisos){
			if(permisos!='4'){
				if(tipo){
					switch(tipo){
						case 'area':
							miPopup = window.open("index.php?equipos_nuevoarea","miwin","width=1010,height=170,scrollbars=no");				
							break;
						case 'sector':
							miPopup = window.open("index.php?equipos_nuevosector","miwin","width=1010,height=218,scrollbars=no");
							break;
						case 'equipo':
							miPopup = window.open("index.php?equipos_nuevoequipo","miwin","width=1010,height=250,scrollbars=no");
							break;
						case 'componente':
							miPopup = window.open("index.php?equipos_nuevocomponente","miwin","width=1010,height=218,scrollbars=no");
							break;	
					}
				}
				miPopup.focus();
			}else{
				alert("No tiene permisos para acceder a este recurso");				
			}
		}		
	}
	
	function ver_equipo(cod,tipo) {
		if(cod && tipo){
			switch(tipo){
				case 'area':
					frame_detalle.location.href="index.php?equipos_area&cod="+cod;				
					break;
				case 'sector':
					frame_detalle.location.href="index.php?equipos_sector&cod="+cod;
					break;
				case 'equipo':
					frame_detalle.location.href="index.php?equipos_equipo&cod="+cod;
					break;
				case 'componente':
					frame_detalle.location.href="index.php?equipos_componente&cod="+cod;
					break;	
			}
		}	
	}
	
	function ver_equipo_iframe(cod,tipo) {
		if(cod && tipo){
			switch(tipo){
				case 'area':
					location.href="index.php?equipos_area&cod="+cod;				
					break;
				case 'sector':
					location.href="index.php?equipos_sector&cod="+cod;
					break;
				case 'equipo':
					location.href="index.php?equipos_equipo&cod="+cod;
					break;
				case 'componente':
					location.href="index.php?equipos_componente&cod="+cod;
					break;	
			}
		}	
	}
	
	function ver_fallas(cod) {
		if(cod){
			miPopup = window.open("index.php?inspecciones_fallas&cod="+cod,"miwin","width=550,height=380,scrollbars=yes");
			miPopup.focus();
		}else{
			alert("Error");
		}
	}
	
	function nuevo_registro(modulo) {
		var permisos=document.getElementById("permisos").value;
		if(permisos){
			if(permisos!='4'){
				location.href="index.php?" + modulo + "_nuevo";
			}else{
				alert("No tiene permisos para acceder a este recurso");	
			}
		}
	}
	
	function modificar_elemento(cod,modulo) {
		var permisos=document.getElementById("permisos").value;
		if(permisos){	
			if(permisos!='4'){
				window.open("index.php?" + modulo + "_modificar&cod=" + cod,  '_blank' );
			}else{
				alert("No tiene permisos para acceder a este recurso");	
			}
		}
	}
	
	function eliminar_elemento(cod,modulo) {
		var permisos=document.getElementById("permisos").value;
		if(permisos){	
			if(permisos!='4'){
				if (confirm("Atencion va a proceder a la eliminacion de un registro. Desea continuar?")) {
					parent.location.href="index.php?" + modulo + "_eliminar&cod=" + cod;
				}				
			}else{
				alert("No tiene permisos para acceder a este recurso");	
			}	
			arriba()
		}
	}
	

	
		
	function regresar_index(ruta,accion) {
		location.href=ruta+"&cadena_busqueda=<? echo $cadena_busqueda?>";
	}
	
	function cancelar(modulo) {
		location.href="index.php?" + modulo + "_index";
	}
	function m_cancelar(modulo) {
		window.close();
	}
	
	function inicio() {
		document.getElementById("form_busqueda").submit();
	}
	
	function limpiar() {
		document.getElementById("form_busqueda").reset();
		document.getElementById("form_busqueda").submit();
	}	
	
	function limpiar_form() {
		document.getElementById("formulario").reset();
	}
	
	function LimpiarForm() {
		document.getElementById("form_busqueda").reset();
		vBuscar()
	}

		
	
	
	
	
	function ValidaFormulario(form)
	{
		var inputs_array = new Array();
		var tipo;
		var cont=0;
		var sAux="";
		var exclude0 = document.forms[form]["exclude"];
		var form = document.getElementById(form);
		//var exclude0 = document.getElementById("exclude");		
		if(exclude0){
			exclude = exclude0.value.split(",")
		}
		if(form){
			for (i=0;i<form.elements.length;i++)
			{
				tipo=form.elements[i].type;
				if(!exclude){
					var exclude= new Array();
				}
				if ( !inArray(form.elements[i].name, exclude) ) {
					
					switch (tipo)
					{
						case 'text':
							if( form.elements[i].value == "" ){
								if(form.elements[i].getAttribute('tag'))
									inputs_array[cont] = form.elements[i].getAttribute('tag');
								else
									inputs_array[cont] = form.elements[i].name;
								cont++;
							}
							break;
							
						case 'textarea':
							if( form.elements[i].value == "" ){
								if(form.elements[i].getAttribute('tag'))
									inputs_array[cont] = form.elements[i].getAttribute('tag');
								else
									inputs_array[cont] = form.elements[i].name;
								cont++;
							}
							break;
						
						case 'select-one':
							if( form.elements[i].value == "0" ){
								if(form.elements[i].getAttribute('tag'))
									inputs_array[cont] = form.elements[i].getAttribute('tag');
								else
									inputs_array[cont] = form.elements[i].name;
								cont++;
							}
							break;
					}
				}
			}
		}else{
			return false;	
		}
		if(inputs_array.length > 0 ){
			alert("Se han encontrado "+inputs_array.length+" errores: \n "+inputs_array);
			return false
		}else{
			return true;
		}
	}
	
	
	function validar_form()
	{
		var inputs_array = new Array();
		var tipo;
		var cont=0;
		var sAux="";
		var form = document.getElementById("formulario");
		var exclude0 = document.getElementById("exclude");
		if(exclude0){
			exclude = exclude0.value.split(",")
		}
		for (i=0;i<form.elements.length;i++)
		{
			tipo=form.elements[i].type;
			if(!exclude){
				var exclude= new Array();
			}
			if ( !inArray(form.elements[i].name, exclude) ) {
				
				switch (tipo)
				{
					case 'text':
						if( form.elements[i].value == "" ){
							if(form.elements[i].getAttribute('tag'))
								inputs_array[cont] = form.elements[i].getAttribute('tag');
							else
								inputs_array[cont] = form.elements[i].name;
							cont++;
						}
						break;
						
					case 'textarea':
						if( form.elements[i].value == "" ){
							if(form.elements[i].getAttribute('tag'))
								inputs_array[cont] = form.elements[i].getAttribute('tag');
							else
								inputs_array[cont] = form.elements[i].name;
							cont++;
						}
						break;
					
					case 'select-one':
						if( form.elements[i].value == "0" ){
							if(form.elements[i].getAttribute('tag'))
								inputs_array[cont] = form.elements[i].getAttribute('tag');
							else
								inputs_array[cont] = form.elements[i].name;
							cont++;
						}
						break;
				}
			}
		}
		if(inputs_array.length > 0 ){
			alert("Se han encontrado "+inputs_array.length+" errores: \n "+inputs_array);
		}else{
			document.getElementById("formulario").submit();
		}
	}
	
	function inArray(needle, haystack) {
		var length = haystack.length;
		for(var i = 0; i < length; i++) {
			if(haystack[i] == needle) return true;
		}
		return false;
	}
	
	
	
	function buscar() {
		if (document.getElementById("iniciopagina").value=="") {
			document.getElementById("iniciopagina").value=1;
		} else {
			document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
		}
		document.getElementById("form_busqueda").submit();
	}
	/******************************************************************************************************************************************************
															 			PAGINACION
	*******************************************************************************************************************************************************/
	
	function paginar() {
		document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
		document.getElementById("form_busqueda").submit();
	}

	
	function actualiza_paginador(cantidad){
		var re_height=((parseInt(cantidad.value)*300)/10)
		document.getElementById("frame_rejilla").height=re_height;
		document.getElementById("form_busqueda").submit();
	}
	
	function inicio_paginar() {
		var cantidad_filas = parseInt(parent.document.getElementById("cantidad_filas").value);
		var numfilas=document.getElementById("numfilas").value;
		var indi=parent.document.getElementById("iniciopagina").value;
		var contador=1;
		var indice=0;
		if (indi>numfilas) { 
			indi=1; 
		}
		parent.document.form_busqueda.filas.value=numfilas;
		parent.document.form_busqueda.paginas.innerHTML="";		
		while (contador<=numfilas) {
			texto=contador + "-" + parseInt(contador+(cantidad_filas-1));
			if (indi==contador) {
				parent.document.form_busqueda.paginas.options[indice]=new Option (texto,contador);
				parent.document.form_busqueda.paginas.options[indice].selected=true;
			} else {
				parent.document.form_busqueda.paginas.options[indice]=new Option (texto,contador);
			}
			indice++;
			contador=contador+cantidad_filas;
		}
	}	
	
	function vBuscar(){
		if (document.getElementById("iniciopagina").value=="") {
			document.getElementById("iniciopagina").value=1;
		} else {
			document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
		}
		document.getElementById("form_grid").innerHTML = '';
		//console.log($("#form_busqueda").serialize());
		var url = "index.php?"+modulo()+"_grid&"+$("#form_busqueda").serialize()
		//$("#form_grid").load(url);
		$( "#form_grid" ).load( url, function() {
			InicioPaginar();
		});
	}
	function InicioPaginar() {
		var cantidad_filas = parseInt(document.getElementById("cantidad_filas").value);
		var numfilas=document.getElementById("numfilas").value;
		var indi=document.getElementById("iniciopagina").value;
		var contador=1;
		var indice=0;
		if (indi>numfilas) { 
			indi=1; 
		}
		document.form_busqueda.filas.value=numfilas;
		document.form_busqueda.paginas.innerHTML="";		
		while (contador<=numfilas) {
			texto=contador + "-" + parseInt(contador+(cantidad_filas-1));
			if (indi==contador) {
				document.form_busqueda.paginas.options[indice]=new Option (texto,contador);
				document.form_busqueda.paginas.options[indice].selected=true;
			} else {
				document.form_busqueda.paginas.options[indice]=new Option (texto,contador);
			}
			indice++;
			contador=contador+cantidad_filas;
		}
	}
	
	/******************************************************************************************************************************************************
															 			VALIDACIONES
	*******************************************************************************************************************************************************/
	function validar_usuario(){			
		var mensaje="";
		if (document.getElementById("password").value=="") mensaje+="  - Contraseña\n";
		if (document.getElementById("password_confirm").value=="") mensaje+="  - Repetir Contrasenia\n";
		if (document.getElementById("username").value=="") mensaje+="  - Usuario\n";

		if (document.getElementById("password").value!=document.getElementById("password_confirm").value) mensaje+="  - Las Contrasenias no son iguales\n";
		if (mensaje!="") {
			alert("Atencion, se han detectado las siguientes incorrecciones:\n\n"+mensaje);
		} else {
			document.getElementById("formulario").submit();
		}
	}
	
	
	
	function compruebaValidoTexto() {
	 if ((event.keyCode != 32) && (event.keyCode < 65) || (event.keyCode > 90) && (event.keyCode < 97) || (event.keyCode > 122))
	  event.returnValue = false;
	}
	
	function compruebaValidoNumeros() {
	 if ((event.keyCode < 48) || (event.keyCode > 57)) 
	  event.returnValue = false;
	}
		
	function compruebaValidoEmail(objeto){
		var patron=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; 
		
		if(objeto.value != ""){				
			if(!patron.test(objeto.value.toString())){ 
				//si era la cadena vacía es que no era válido. Lo aviso
				objeto.value='';
				alert ("Email Incorrecto")
				//coloco otra vez el foco
				objeto.focus()
			}			
		}
	}
	
	function compruebaValidoAlfaNum(objeto){
		var patron=/^[a-zA-Z0-9\n]*$/;   ///^[a-zA-Z0-9\n]*$/; 
		
		if(objeto.value != ""){				
			if(!patron.test(objeto.value.toString())){ 
				//si era la cadena vacía es que no era válido. Lo aviso
				objeto.value='';
				alert ("Debe escribir solo Caracteres Alfa-Numericos")
				//coloco otra vez el foco
				objeto.focus()
			}			
		}
	}