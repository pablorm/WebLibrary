/**
 * FORMULARIOS DE BUSQUEDAS
 */
function vBuscar(){
	if (document.getElementById("iniciopagina").value=="") {
		document.getElementById("iniciopagina").value=1;
	} else {
		document.getElementById("iniciopagina").value=document.getElementById("paginas").value;
	}
	document.getElementById("form_grid").innerHTML = '';
	
	var url = setURL("_grid&"+$("#form_busqueda").serialize());
	var b = $("#form_grid");		
	
	$.ajax({
		type: "GET",
		url: url,
		dataType: "html",
		cache: !0,
		beforeSend: function() {
			
			if ($.navAsAjax && $.intervalArr.length > 0 && b[0] == $("#content")[0] && enableJarvisWidgets)
				for (; $.intervalArr.length > 0;) clearInterval($.intervalArr.pop());
			pagefunction = null, 
			b.removeData().html(""), 
			b.html('<h1 class="ajax-loading-animation"><i class="fa fa-cog fa-spin"></i> Loading...</h1>');
		},
		success: function(a) {
			b.css({
				opacity: "0.0"
			}).html(a).delay(50).animate({
				opacity: "1.0"
			}, 300), a = null, b = null
			
			InicioPaginar();
		},
		error: function() {
			b.html('<h4 class="ajax-loading-error"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>')
		},
		async: !0
	})
}

function InicioPaginar() {
	var numfilas = 0;
	if(document.getElementById("numfilas"))
		var numfilas 	= parseInt(document.getElementById("numfilas").value);
	var cantidad_filas 	= parseInt(document.getElementById("cantidad_filas").value);		
	var indi 	= parseInt(document.getElementById("iniciopagina").value);
	var contador= 1;
	var indice	= 0;
	if( indi > numfilas ) { 
		indi	= 1; 
	}
	document.form_busqueda.filas.value	= numfilas;
	document.form_busqueda.paginas.innerHTML	= "";	
	while (contador<=numfilas) {
		texto	=	contador + "-" + parseInt(contador+(cantidad_filas-1));
		if( indi==contador ) {
			document.form_busqueda.paginas.options[indice]	= new Option (texto,contador);
			document.form_busqueda.paginas.options[indice].selected	= true;
		} else {
			document.form_busqueda.paginas.options[indice]	= new Option (texto,contador);
		}
		indice++;
		contador = contador+cantidad_filas;
	}
}
function LimpiarForm(form) {
	if(!form)
		form = "form_busqueda"
	document.getElementById(form).reset();
	vBuscar()
}


/**
 * FORMULARIOS DE INGRESO, MODIFICCION Y VISUALIZACION DE REGISTROS
 */
function NuevoRegistro(w,h) {
	setDialog("#nuevo", {},w,h);
	$("#nuevo").dialog("open");		
	/*******************************************************************************/
	$("#Naccion").val('NUEVO');					
}	
function ModificarRegistro(cod,w,h) {
	setDialog("#nuevo", {},w,h);
	$("#nuevo").dialog("open");	
	/*******************************************************************************/
	$("#Naccion").val('MODIFICAR');
	$("#Ncod").val(cod);
	sendViaGet(setURL("_detalle&target=N&cod=" + cod), cbInfo);
}
function VerRegistro(cod,w,h) {
	setDialog("#visualizar", {},w,h);
	$("#visualizar").dialog("open");	
	/*******************************************************************************/
	sendViaGet(setURL("_detalle&target=V&cod=" + cod), cbInfo);
}
function reciclaElemento(cod){
	var permisos=obten_permisos();
	if(permisos){	
		if(permisos!='4'){
			$.SmartMessageBox({
				title : "Atención",
				content : "Va a eliminar un registro. Desea continuar?",
				buttons : '[Si][No]'
			}, function(ButtonPressed) {
				if (ButtonPressed === "Si") {	
					var direccion = setURL("_guardar&accion=ELIMINAR&cod=" + cod)
					$.post( direccion )
					.done(function( data ) {
						console.log(data);	
						var json = IsJsonString(data);										
						if( json['success']=='1' ){							
							showAlert('success','Registro Eliminado correctamente.');
						}else
							showAlert('danger','No se pudo eliminar el registro.');//alert('Error al guardar');					
						vBuscar();					
					});
				}
			});
		}else{
			alert("No tiene permisos para acceder a este recurso");	
		}
	}else{
		alert("No se pudo realizar la accion solicitada");	
	}	
}
function enviaNuevo(){
	var url = "index.php?"+modulo()+"_guardar&accion=" + $("#Naccion").val();
	//console.log('URL:',url);
	if( ValidaFormulario("nuevo_form") ){
		$.post( url, $( "#nuevo_form" ).serialize())
		.done(function( data ) {
			console.log(data);	
			var json = IsJsonString(data);										
			if( json['success']=='1' ){
				
				showAlert('success','Registro Guardado correctamente.');
				cierraModal('nuevo');
			}else
				showAlert('danger','No se pudo guardar el registro.');//alert('Error al guardar');					
			vBuscar();					
		});
	}	
}
function ValidaFormulario(form)
{
	var inputs_array = new Array();
	var tipo;
	var cont=0;
	var sAux="";
	var exclude0 = document.forms[form]["exclude"];
	var form = document.getElementById(form);
	//var exclude0 = document.getElementById("exclude");		
	if(exclude0){
		exclude = exclude0.value.split(",")
	}
	if(form){
		for (i=0;i<form.elements.length;i++)
		{
			tipo=form.elements[i].type;
			if(!exclude){
				var exclude= new Array();
			}
			if ( !inArray(form.elements[i].name, exclude) ) {
				
				switch (tipo)
				{
					case 'text':
						if( form.elements[i].value == "" ){
							if(form.elements[i].getAttribute('tag'))
								inputs_array[cont] = form.elements[i].getAttribute('tag');
							else
								inputs_array[cont] = form.elements[i].name;
							cont++;
						}
						break;
						
					case 'textarea':
						if( form.elements[i].value == "" ){
							if(form.elements[i].getAttribute('tag'))
								inputs_array[cont] = form.elements[i].getAttribute('tag');
							else
								inputs_array[cont] = form.elements[i].name;
							cont++;
						}
						break;
					
					case 'select-one':
						if( form.elements[i].value == "0" ){
							if(form.elements[i].getAttribute('tag'))
								inputs_array[cont] = form.elements[i].getAttribute('tag');
							else
								inputs_array[cont] = form.elements[i].name;
							cont++;
						}
						break;
				}
			}
		}
	}else{
		return false;	
	}
	if(inputs_array.length > 0 ){
		//alert("Se han encontrado "+inputs_array.length+" errores: \n "+inputs_array);
		$.SmartMessageBox({
			title : "Rellene los siguientes campos:",
			content : inputs_array,
			buttons : '[Aceptar]'
		});
		return false
	}else{
		return true;
	}
}

/**
 * MANEJO DE DIALOGOS
 */
function cierraModal(id){
	$("#"+id).dialog("close");
	var input = document.getElementById(id).getElementsByTagName('form');
	var inputList = Array.prototype.slice.call(input);
	inputList.forEach(ShowResults);
	function ShowResults(value, index, ar) {
		value.reset();
	}
}
function setDialog(nombre,btns,w,h) {
	if ($(nombre).dialog())
		$(nombre).dialog('destroy');
		
	$(nombre).dialog({
		autoOpen: false,
		height: h,
		width: w,
		modal: true,
		position: {
			my: "center",
			at: "top+50%"
		},
		resizable: false,
		buttons: btns
	});
	$(".ui-dialog-titlebar").hide();
}
function Alert(msg){
	$.SmartMessageBox({
		title : "Alerta",
		content : msg,
		buttons : '[Aceptar]'
	});	
} 
 
/**
 * AJAX
 */
 function sendFile(files, url, funcion) {
	var data = new FormData();
	$.each(files, function (key, value) {
		data.append(key, value);
	});
	//console.log(data,data[0],data);
	$.ajax({
		type: 'post',
		url: url,
		data: data,
		success: funcion,
		xhrFields: {
			// add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
			onprogress: function (progress) {
				//console.log(progress);
				// calculate upload progress
				var percentage = Math.floor((progress.total / progress.totalSize) * 100);
				// log upload progress to console
				//console.log('progress', percentage);
				/*
				if (percentage === 100) {
					console.log('DONE!');
				}*/
			}
		},
		processData: false,
		contentType: false
	});
}
function sendViaGet(direccion,frespuesta){
	if( direccion ){
		if( frespuesta ){
			$.ajax({
				url:   direccion,
				success: frespuesta
			});
		}else{
			var data='test';
			$.ajax({
				url:   direccion,
				async: false
			})
			.done(function( html ) {
				data = html;
			});
			return data;
		}
	}		
}
function getJSON64(url,target,func){
	url = url+"&target="+target;
	var data='NULL';
	var b = $("#"+target);
	if( url ){			
		$.ajax({
			url:   url,
			beforeSend: function() {			
				if ($.navAsAjax && $.intervalArr.length > 0 && b[0] == $("#content")[0] && enableJarvisWidgets)
					for (; $.intervalArr.length > 0;) clearInterval($.intervalArr.pop());
				pagefunction = null, 
				b.removeData().html(""), 
				b.html('<h1 class="ajax-loading-animation"><i class="fa fa-cog fa-spin"></i> Loading...</h1>');
			}
		})
		.done(function( html ) {
			func(html)
		});
	}		
}

/**
 * JSON
 */
function cbInfo(data){
	console.log("DF:",data);
	var json = IsJsonString(data);
	if(json){
		if(json['data']){
			setElementValue(json['data'],true,json['target'])
		}else if(json['append']){
			//setElementValue(json['data'],true,json['target'])
			$("#"+json['target']).text("");
			$("#"+json['target']).append(atob(json['append']));
		}
	}
	$("div[title=\"t_loading\"]").hide();
}
function IsJsonString(str) {
	try {
		var data = JSON.parse(str);
	} catch (e) {
		var data = false;
	}
	return data;
}
function setElementValue(json,base64,key){
	var fKey = ((key)?key:'V');
	if( json ){
		$.each(json, function(name, value) {
			if( base64 && value)
				value = atob((value?value:''));
			else
				value = (value?value:'');
				
			name = name.toLowerCase();
			label = document.getElementById(fKey+name);
			
			if( label ){
				switch(label.type){
					case 'hidden': label.value = value; break;
					case 'select-one': seleccionar(fKey+name,value); break;
					case 'text': label.value = value; break;
					case 'time': label.value = value; break;
					default: label.innerHTML = value;
				}
			}
		});
		return true;
	}	
}
/**
 * VALIDACIONES
 */
function compruebaValidoTexto() {
	 if ((event.keyCode != 32) && (event.keyCode < 65) || (event.keyCode > 90) && (event.keyCode < 97) || (event.keyCode > 122))
	  event.returnValue = false;
	}
	
	function compruebaValidoNumeros() {
	 if ((event.keyCode < 48) || (event.keyCode > 57)) 
	  event.returnValue = false;
	}
		
	function compruebaValidoEmail(objeto){
		var patron=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; 
		
		if(objeto.value != ""){				
			if(!patron.test(objeto.value.toString())){ 
				//si era la cadena vacía es que no era válido. Lo aviso
				objeto.value='';
				Alert("Email Incorrecto")
				//coloco otra vez el foco
				objeto.focus()
			}			
		}
	}
	
	function compruebaValidoAlfaNum(objeto){
		var patron=/^[a-zA-Z0-9\n]*$/;   ///^[a-zA-Z0-9\n]*$/; 
		
		if(objeto.value != ""){				
			if(!patron.test(objeto.value.toString())){ 
				//si era la cadena vacía es que no era válido. Lo aviso
				objeto.value='';
				Alert("Debe escribir solo Caracteres Alfa-Numericos")
				//coloco otra vez el foco
				objeto.focus()
			}			
		}
	}
/**
 * OTROS
 */
function reset_form_element(e) {
	e.wrap('<form>').parent('form').trigger('reset');
	e.unwrap();
}
function arriba(){
	window.parent.$("body").animate({scrollTop:0}, 'fast');
}
function startDownload (n) { 
	var frame = document.getElementById("download");
	var frame_p = parent.document.getElementById("download");
	if( frame ){
		frame.src=n;
	}else if( frame_p ){
		frame_p.src=n;
	}
	return false;	
}
function inArray(needle, haystack) {
	var length = haystack.length;
	for(var i = 0; i < length; i++) {
		if(haystack[i] == needle) return true;
	}
	return false;
}
function seleccionar(elemento,valor) {
	var combo = document.getElementById(elemento)
	//var combo = document.forms["formulario"].criticidad;
	var cantidad = combo.length;
	for (i = 0; i < cantidad; i++) {
		if (combo[i].value == valor) {
			combo[i].selected = true;
		}   
	}
}
function is_int(data){
	var intRegex = /^\d+$/;
	if(intRegex.test(data))
		return true;
	return false;			
}
function obten_permisos(){
	permisos=document.getElementById("permisos");
	if(!permisos){
		permisos=window.parent.document.getElementById("permisos");
		if(!permisos){
			return 4;	
		}
	}
	return permisos.value;
}

	
	
	
	
	