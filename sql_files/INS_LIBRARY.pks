CREATE OR REPLACE PACKAGE LIB.INS_LIBRARY AS
/******************************************************************************
   NAME:       INS_LIBRARY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        08-06-2017      Pablo       1. Created this package.
******************************************************************************/

  PROCEDURE IGROUP(GNOMBRE IN VARCHAR2);
  PROCEDURE IPERMISO(ICOD IN NUMBER, IGRUPO IN NUMBER, IPERMISO IN NUMBER);  
  PROCEDURE SAVE_FILE(I_COD IN NUMBER, I_PARENT IN NUMBER, I_NOMBRE IN VARCHAR2, 
                    I_EXT IN VARCHAR2, I_TAMANIO IN NUMBER, 
                    I_UBICACION IN VARCHAR2, I_FECHA_S IN VARCHAR2, I_ESTADO IN NUMBER );  
  PROCEDURE SET_DIR_USER(I_CARPETA IN NUMBER, I_USUARIO IN NUMBER );  
  PROCEDURE SAVE_OTP(I_CODIGO IN VARCHAR2, I_HASH IN VARCHAR2, I_ARCHIVO IN NUMBER, I_TIEMPO IN NUMBER );
  PROCEDURE MAKE_DIR(I_NOMBRE IN VARCHAR2, I_PARENT IN NUMBER, I_LIB IN NUMBER );
  PROCEDURE SET_DIR_PERMS(I_COD IN NUMBER, I_CARPETA IN NUMBER);
  PROCEDURE SET_PERMS(I_GRUPO IN NUMBER, I_CARPETA IN NUMBER);
  PROCEDURE SET_LINK(I_CARPETA IN NUMBER, I_FECHA_C IN VARCHAR2, I_FECHA_E IN VARCHAR2, 
                    I_CLAVE IN VARCHAR2, I_PERMISOS IN NUMBER, I_HASH IN VARCHAR2);
  PROCEDURE SET_HISTORY(I_USUARIO IN NUMBER, I_ARCHIVO IN NUMBER, I_TIPO_C IN NUMBER, 
                    I_FECHA IN VARCHAR2, I_ACCION IN VARCHAR2);

END INS_LIBRARY;
/