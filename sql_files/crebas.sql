/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     08-06-2017 12:25:33                          */
/*==============================================================*/


alter table A_ARCHIVOS
   drop constraint FK_A_ARCHIV_REFERENCE_A_CARPET;

alter table A_GRUPOS_USUARIOS
   drop constraint FK_A_GRUPOS_REFERENCE_U_LISTAD;

alter table A_GRUPOS_USUARIOS
   drop constraint FK_A_GRUPOS_REFERENCE_A_GRUPOS;

alter table A_HISTORIAL
   drop constraint FK_A_HISTOR_REFERENCE_A_ARCHIV;

alter table A_HISTORIAL
   drop constraint FK_A_HISTOR_REFERENCE_U_LISTAD;

alter table A_LINKS
   drop constraint FK_A_LINKS_REFERENCE_A_CARPET;

alter table A_OTPFILES
   drop constraint FK_A_OTPFIL_REFERENCE_A_ARCHIV;

alter table A_PERMISOS
   drop constraint FK_A_PERMIS_REFERENCE_A_GRUPOS;

alter table A_PERMISOS
   drop constraint FK_A_PERMIS_REFERENCE_A_CARPET;

alter table U_PERMISOS
   drop constraint FK_U_PERMIS_REFERENCE_U_LISTAD;

alter table U_PERMISOS
   drop constraint FK_U_PERMIS_REFERENCE_U_MODULO;

drop table A_ARCHIVOS cascade constraints;

drop table A_CARPETAS cascade constraints;

drop table A_GRUPOS cascade constraints;

drop table A_GRUPOS_USUARIOS cascade constraints;

drop table A_HISTORIAL cascade constraints;

drop table A_LINKS cascade constraints;

drop table A_OTPFILES cascade constraints;

drop table A_PERMISOS cascade constraints;

drop table U_LISTADO cascade constraints;

drop table U_MODULOS cascade constraints;

drop table U_PERMISOS cascade constraints;

drop sequence SQ_A_ARCHIVOS;

drop sequence SQ_A_CARPETAS;

drop sequence SQ_A_GRUPOS;

drop sequence SQ_A_GRUPOS_USUARIOS;

drop sequence SQ_A_HISTORIAL;

drop sequence SQ_A_LINKS;

drop sequence SQ_A_OTPFILES;

drop sequence SQ_A_PERMISOS;

drop sequence SQ_U_LISTADO;

drop sequence SQ_U_MODULOS;

drop sequence SQ_U_PERMISOS;

create sequence SQ_A_ARCHIVOS
start with 100;

create sequence SQ_A_CARPETAS
start with 100;

create sequence SQ_A_GRUPOS
start with 100;

create sequence SQ_A_GRUPOS_USUARIOS
start with 100;

create sequence SQ_A_HISTORIAL
start with 100;

create sequence SQ_A_LINKS
start with 100;

create sequence SQ_A_OTPFILES
start with 100;

create sequence SQ_A_PERMISOS
start with 100;

create sequence SQ_U_LISTADO
start with 100;

create sequence SQ_U_MODULOS
start with 100;

create sequence SQ_U_PERMISOS
start with 100;

/*==============================================================*/
/* Table: A_ARCHIVOS                                            */
/*==============================================================*/
create table A_ARCHIVOS 
(
   P_KEY                NUMBER(6)            not null,
   PARENT               NUMBER(6)            not null,
   NOMBRE               VARCHAR2(257 BYTE)   not null,
   EXT                  VARCHAR2(45 BYTE)    not null,
   TAMANIO              NUMBER(15)           not null,
   UBICACION            VARCHAR2(250 BYTE)   not null,
   FECHA_SUBIDA         DATE                 not null,
   ESTADO               NUMBER(1)            not null,
   constraint A_ARCHIVOS_PK primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: A_CARPETAS                                            */
/*==============================================================*/
create table A_CARPETAS 
(
   P_KEY                NUMBER(6)            not null,
   NOMBRE               VARCHAR2(257 BYTE)   not null,
   PARENT               NUMBER(6)            not null,
   LIB                  NUMBER(1)            not null,
   ESTADO               NUMBER(1)            default 0 not null,
   constraint A_CARPETAS_PK primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: A_GRUPOS                                              */
/*==============================================================*/
create table A_GRUPOS 
(
   P_KEY                NUMBER(6)            not null,
   NOMBRE               VARCHAR2(45 BYTE)    not null,
   ESTADO               NUMBER(1)            not null,
   constraint PK_A_GRUPOS primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: A_GRUPOS_USUARIOS                                     */
/*==============================================================*/
create table A_GRUPOS_USUARIOS 
(
   P_KEY                NUMBER(6)            not null,
   ID_USUARIO           NUMBER(6)            not null,
   ID_GRUPO             NUMBER(6)            not null,
   PERMISO              NUMBER(1)            not null,
   constraint A_GRUPOS_USUARIOS_PK primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: A_HISTORIAL                                           */
/*==============================================================*/
create table A_HISTORIAL 
(
   P_KEY                NUMBER(6)            not null,
   ID_USUARIO           NUMBER(6),
   ID_ARCHIVO           NUMBER(6),
   TIPO_CONSULTA        NUMBER(1),
   FECHA                DATE                 not null,
   ACCION               VARCHAR2(45 BYTE),
   ESTADO               NUMBER(1)            not null,
   constraint PK_A_HISTORIAL primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: A_LINKS                                               */
/*==============================================================*/
create table A_LINKS 
(
   P_KEY                NUMBER(6)            not null,
   ID_CARPETA           NUMBER(6)            not null,
   FECHA_CREACION       DATE                 not null,
   FECHA_EXPIRACION     DATE,
   CLAVE                NUMBER(1),
   PERMISOS             NUMBER(1),
   HASH                 VARCHAR2(35 BYTE)    not null,
   ESTADO               NUMBER(1)            not null,
   constraint PK_A_LINKS primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: A_OTPFILES                                            */
/*==============================================================*/
create table A_OTPFILES 
(
   P_KEY                NUMBER(6)            not null,
   CODIGO               NUMBER(9)            not null,
   HASH                 VARCHAR2(35 BYTE)    not null,
   ID_ARCHIVO           NUMBER(6)            not null,
   TIEMPO               NUMBER(10)           not null,
   ESTADO               NUMBER(1)            not null,
   constraint A_OTPFILES_PK primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: A_PERMISOS                                            */
/*==============================================================*/
create table A_PERMISOS 
(
   P_KEY                NUMBER(6)            not null,
   ID_CARPETA           NUMBER(6)            not null,
   ID_GRUPO             NUMBER(6)            not null,
   constraint PK_A_PERMISOS primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: U_LISTADO                                             */
/*==============================================================*/
create table U_LISTADO 
(
   ID                   NUMBER(6)            not null,
   USUARIO              VARCHAR2(100 BYTE)   not null,
   PASS                 VARCHAR2(100 BYTE)   not null,
   FECHA_CREACION       DATE,
   SITIO                NUMBER(6),
   ESTADO               NUMBER(1)            not null,
   constraint U_LISTADO_PK primary key (ID)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: U_MODULOS                                             */
/*==============================================================*/
create table U_MODULOS 
(
   P_KEY                NUMBER(6)            not null,
   NOMBRE               VARCHAR2(90 BYTE)    not null,
   CARPETA              VARCHAR2(90 BYTE)    not null,
   SITIO                NUMBER(1)            not null,
   ESTADO               NUMBER(1)            not null,
   PARENT               NUMBER(6),
   ORDEN                NUMBER(6),
   ICON                 VARCHAR2(250 BYTE),
   constraint U_MODULOS_PK primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

/*==============================================================*/
/* Table: U_PERMISOS                                            */
/*==============================================================*/
create table U_PERMISOS 
(
   P_KEY                NUMBER(6)            not null,
   ID_USUARIO           NUMBER(6)            not null,
   ID_MODULO            NUMBER(6)            not null,
   ID_PERMISO           NUMBER(6)            not null,
   constraint U_PERMISOS_PK primary key (P_KEY)
)
TABLESPACE USERS
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

alter table A_ARCHIVOS
   add constraint FK_A_ARCHIV_REFERENCE_A_CARPET foreign key (PARENT)
      references A_CARPETAS (P_KEY);

alter table A_GRUPOS_USUARIOS
   add constraint FK_A_GRUPOS_REFERENCE_U_LISTAD foreign key (ID_USUARIO)
      references U_LISTADO (ID);

alter table A_GRUPOS_USUARIOS
   add constraint FK_A_GRUPOS_REFERENCE_A_GRUPOS foreign key (ID_GRUPO)
      references A_GRUPOS (P_KEY);

alter table A_HISTORIAL
   add constraint FK_A_HISTOR_REFERENCE_A_ARCHIV foreign key (ID_ARCHIVO)
      references A_ARCHIVOS (P_KEY);

alter table A_HISTORIAL
   add constraint FK_A_HISTOR_REFERENCE_U_LISTAD foreign key (ID_USUARIO)
      references U_LISTADO (ID);

alter table A_LINKS
   add constraint FK_A_LINKS_REFERENCE_A_CARPET foreign key (ID_CARPETA)
      references A_CARPETAS (P_KEY);

alter table A_OTPFILES
   add constraint FK_A_OTPFIL_REFERENCE_A_ARCHIV foreign key (ID_ARCHIVO)
      references A_ARCHIVOS (P_KEY);

alter table A_PERMISOS
   add constraint FK_A_PERMIS_REFERENCE_A_GRUPOS foreign key (ID_GRUPO)
      references A_GRUPOS (P_KEY);

alter table A_PERMISOS
   add constraint FK_A_PERMIS_REFERENCE_A_CARPET foreign key (ID_CARPETA)
      references A_CARPETAS (P_KEY);

alter table U_PERMISOS
   add constraint FK_U_PERMIS_REFERENCE_U_LISTAD foreign key (ID_USUARIO)
      references U_LISTADO (ID);

alter table U_PERMISOS
   add constraint FK_U_PERMIS_REFERENCE_U_MODULO foreign key (ID_MODULO)
      references U_MODULOS (P_KEY);

