CREATE OR REPLACE PACKAGE BODY LIB.DEL_LIBRARY AS
/******************************************************************************
   NAME:       DEL_LIBRARY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        09-06-2017      Pablo       1. Created this package body.
******************************************************************************/

    PROCEDURE PERMSCHILDRENS(I_GRUPO IN NUMBER, I_CARPETA IN NUMBER) IS
    BEGIN
        DELETE FROM A_PERMISOS WHERE A_PERMISOS.ID_GRUPO = I_GRUPO AND ID_CARPETA IN(
        SELECT P_KEY FROM A_CARPETAS
        START WITH P_KEY = I_CARPETA
        CONNECT BY PRIOR P_KEY = PARENT);
    END;
    
    PROCEDURE PERMSPARENTCHILDRENS(I_GRUPO IN NUMBER, I_CARPETA IN NUMBER) IS
    BEGIN
        DELETE FROM A_PERMISOS WHERE A_PERMISOS.ID_GRUPO = I_GRUPO AND ID_CARPETA IN(
        SELECT P_KEY FROM A_CARPETAS
        START WITH PARENT = I_CARPETA
        CONNECT BY PRIOR P_KEY = PARENT
        UNION
        SELECT P_KEY FROM A_CARPETAS
        START WITH P_KEY = I_CARPETA
        CONNECT BY PRIOR  PARENT = P_KEY);
    END;
    
    PROCEDURE DIRPERMS(I_COD IN NUMBER) IS
    BEGIN
        DELETE FROM A_PERMISOS WHERE ID_CARPETA = I_COD;
    END;
    
    PROCEDURE RMCARPETA(I_COD IN NUMBER) IS
    BEGIN
        DELETE FROM A_CARPETAS WHERE P_KEY = I_COD;
    END;

END DEL_LIBRARY;
/