CREATE OR REPLACE PACKAGE LIB.DEL_LIBRARY AS
/******************************************************************************
   NAME:       DEL_LIBRARY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        09-06-2017      Pablo       1. Created this package.
******************************************************************************/

  PROCEDURE PERMSCHILDRENS(I_GRUPO IN NUMBER, I_CARPETA IN NUMBER);
  PROCEDURE PERMSPARENTCHILDRENS(I_GRUPO IN NUMBER, I_CARPETA IN NUMBER);
  PROCEDURE DIRPERMS(I_COD IN NUMBER);
  PROCEDURE RMCARPETA(I_COD IN NUMBER);

END DEL_LIBRARY;
/