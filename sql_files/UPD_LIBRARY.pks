CREATE OR REPLACE PACKAGE LIB.UPD_LIBRARY AS
/******************************************************************************
   NAME:       UPD_LIBRARY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        08-06-2017      Pablo       1. Created this package.
******************************************************************************/

  PROCEDURE UGROUP(GNOMBRE IN VARCHAR2, GCOD IN NUMBER);
  PROCEDURE UPARENTFILE(I_PARENT IN NUMBER, I_COD IN NUMBER);
  PROCEDURE UFILE(I_ESTADO IN NUMBER, I_COD IN NUMBER);
  PROCEDURE URECYCLEFILE(I_ESTADO IN NUMBER, I_COD IN NUMBER);
  PROCEDURE UDIRSTATUS(I_ESTADO IN NUMBER, I_COD IN NUMBER);
  PROCEDURE UDIRNAME(I_NOMBRE IN VARCHAR2, I_CARPETA IN NUMBER);

END UPD_LIBRARY;
/