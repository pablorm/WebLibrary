CREATE OR REPLACE PACKAGE BODY LIB.UPD_LIBRARY AS
/******************************************************************************
   NAME:       UPD_LIBRARY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        08-06-2017      Pablo       1. Created this package body.
******************************************************************************/

    PROCEDURE UGROUP(GNOMBRE IN VARCHAR2, GCOD IN NUMBER) IS
    BEGIN
        UPDATE A_GRUPOS SET NOMBRE=GNOMBRE WHERE P_KEY=GCOD;
    END;
  
    PROCEDURE UPARENTFILE(I_PARENT IN NUMBER, I_COD IN NUMBER) IS
    BEGIN
        UPDATE A_ARCHIVOS SET PARENT = I_PARENT WHERE P_KEY=I_COD;
    END;
    
    PROCEDURE UFILE(I_ESTADO IN NUMBER, I_COD IN NUMBER) IS
    BEGIN
        UPDATE A_ARCHIVOS  SET ESTADO = I_ESTADO WHERE P_KEY = I_COD;
    END;
  
    PROCEDURE URECYCLEFILE(I_ESTADO IN NUMBER, I_COD IN NUMBER) IS
    BEGIN
        UPDATE A_ARCHIVOS  SET ESTADO = I_ESTADO WHERE P_KEY = I_COD AND ESTADO='1' ;
    END;
  
    PROCEDURE UDIRSTATUS(I_ESTADO IN NUMBER, I_COD IN NUMBER) IS
    BEGIN
        UPDATE A_CARPETAS  SET ESTADO = I_ESTADO WHERE P_KEY = I_COD;
    END;
  
    PROCEDURE UDIRNAME(I_NOMBRE IN VARCHAR2, I_CARPETA IN NUMBER) IS
    BEGIN
        UPDATE A_CARPETAS  SET NOMBRE = I_NOMBRE WHERE P_KEY = I_CARPETA;
    END;

END UPD_LIBRARY;
/